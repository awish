#!/bin/sh

SDL_FLAGS=`pkg-config sdl --cflags`
SDL_LIBS=`pkg-config sdl --libs`

SDM_FLAGS=`pkg-config SDL_mixer --cflags`
SDM_LIBS=`pkg-config SDL_mixer --libs`


linklist=""

# $1: infile
compile() {
  ptmp=`basename "$1" .c`
  pdst="_bld/${ptmp}.o"
  echo "CC    ${ptmp}.o"
  gcc -pipe -O2 -Wall -std=gnu99 -Isrc ${SDL_FLAGS} ${SDM_FLAGS} -s -c -o "${pdst}" "$1"
  linklist="${linklist} ${pdst}"
}


# $1: dest
link() {
  echo "LINK  $1"
  gcc -s ${SDL_LIBS} ${SDM_LIBS} -o "$1" ${linklist}
  chmod 755 "$1"
}


odir=`pwd`
mydir=`dirname "$0"`
cd "$mydir"

rm -rf _bld 2>/dev/null
mkdir _bld

#
compile src/vm.c
saved_linklist="${linklist}"
compile src/awasm.c
link awasm
linklist="${saved_linklist}"
#
compile src/librnc/librnc.c
compile src/libwdx/libwdx.c
#
compile src/resfile.c
compile src/video.c
compile src/gameglobals.c
compile src/mainloop.c
compile src/title.c
compile src/game.c
compile src/polymod.c
compile src/awish.c
link awish

mkdir -p data/code
./awasm asm/awlogic.awa data/code/awish.vmd

rm -rf _bld 2>/dev/null
cd "$odir"

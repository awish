/* Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "librnc/librnc.h"


static void show_help (const char *pname) {
  fprintf(stderr, "usage: %s <files> or %s -o <infile> <outfile>\n", pname, pname);
  exit(1);
}


static int main_pack (const char *pname, const char *iname, const char *oname) {
  FILE *ifp, *ofp;
  int ulen, ulen2, plen;
  void *packed = NULL, *unpacked = NULL, *unpacked2 = NULL;
  int leeway;
  //
  ifp = fopen(iname, "rb");
  if (ifp == NULL) {
    perror(iname);
    return 1;
  }
  //
  if (fseek(ifp, 0, SEEK_END) != 0) {
    perror(pname);
    return 1;
  }
  ulen = ftell(ifp);
  rewind(ifp);
  //
  unpacked = malloc(ulen);
  if (!unpacked) {
    perror(pname);
    return 1;
  }
  if (fread(unpacked, ulen, 1, ifp) != 1) {
    perror(pname);
    return 1;
  }
  fclose(ifp);
  //
  fprintf(stderr, "%s -> %s  ", iname, oname); fflush(stderr);
  packed = rnc_pack(unpacked, ulen, &plen);
  fprintf(stderr, "DONE\n");
  if (!packed) {
    fprintf(stderr, "Error in compression\n");
    return 1;
  }
  //
  unpacked2 = malloc(ulen);
  if (!unpacked2) {
    perror(pname);
    return 1;
  }
  //
  ulen2 = rnc_unpack(packed, unpacked2, &leeway);
  if (ulen2 < 0) {
    fprintf(stderr, "Test unpack: %s\n", rnc_error (ulen2));
    return 1;
  }
  if (ulen2 != ulen) {
    fprintf(stderr, "Test unpack: lengths do not match\n");
    return 1;
  }
  if (memcmp (unpacked, unpacked2, ulen)) {
    fprintf(stderr, "Test unpack: files do not match\n");
    return 1;
  }
  //
  if (leeway > 255) {
    fprintf(stderr, "Unable to handle leeway > 255\n");
    return 1;
  }
  ((unsigned char *)packed)[16] = leeway;
  //
  ofp = fopen(oname, "wb");
  if (!ofp) {
    perror(oname);
    return 1;
  }
  fwrite(packed, 1, plen, ofp);
  fclose(ofp);
  //
  if (unpacked2 != NULL) free(unpacked2);
  if (packed != NULL) free(packed);
  if (unpacked != NULL) free(unpacked);
  //
  return 0;
}


int main (int argc, char *argv[]) {
  int mode = 0;
  //
  if (argc == 1) show_help(argv[0]);
  //
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-o") == 0) {
      if (mode != 0) {
        fprintf(stderr, "FATAL: too many '-o'!\n");
        exit(1);
      }
      mode = i;
    }
  }
  //
  if (mode > 0 && argc != 4) show_help(argv[0]);
  //
  switch (mode) {
    case 0:
      for (int i = 1; i < argc; ++i) if (main_pack(argv[0], argv[i], argv[i])) return 1;
      return 0;
    case 1: // -o if of
      return main_pack(argv[0], argv[2], argv[3]);
    case 2: // if -o of
      return main_pack(argv[0], argv[1], argv[3]);
    case 3: // if of -o
      return main_pack(argv[0], argv[1], argv[2]);
    default:
      fprintf(stderr, "Internal fault.\n");
      break;
  }
  return 1;
}

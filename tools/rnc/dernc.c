/* Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "librnc/librnc.h"


static void show_help (const char *pname) {
  fprintf(stderr, "usage: %s <files> or %s -o <infile> <outfile>\n", pname, pname);
  exit(1);
}


static int copy_file (const char *iname, const char *oname) {
  FILE *fi, *fo;
  static char buf[8192];
  //
  fi = fopen(iname, "rb");
  if (fi == NULL) {
    fprintf(stderr, "FATAL: can't open input file: '%s'\n", iname);
    return 1;
  }
  //
  fo = fopen(oname, "wb");
  if (fo == NULL) {
    fclose(fi);
    fprintf(stderr, "FATAL: can't create output file: '%s'\n", oname);
    return 1;
  }
  //
  for (;;) {
    size_t rd;
    //
    rd = fread(buf, 1, sizeof(buf), fi);
    if (rd == 0) {
      if (ferror(fi)) {
        fclose(fi);
        fclose(fo);
        unlink(oname);
        fprintf(stderr, "FATAL: error reading input file: '%s'\n", iname);
        return 1;
      }
      break;
    }
    if (fwrite(buf, rd, 1, fo) != 1) {
      fclose(fi);
      fclose(fo);
      unlink(oname);
      fprintf(stderr, "FATAL: error writing output file: '%s'\n", oname);
      return 1;
    }
  }
  //
  fclose(fi);
  fclose(fo);
  //
  return 0;
}


static int main_unpack (const char *pname, const char *iname, const char *oname) {
  FILE *ifp = NULL, *ofp = NULL;
  int plen, ulen;
  void *packed, *unpacked;
  char buffer[4];
  //
  ifp = fopen(iname, "rb");
  if (!ifp) {
    perror(iname);
    return 1;
  }
  //
  fseek(ifp, 0L, SEEK_END);
  plen = ftell(ifp);
  rewind(ifp);
  if (plen < 4) {
    // can't be an RNC file
    if (strcmp(iname, oname) != 0) return copy_file(iname, oname);
    return 0;
  }
  //
  if (fread(buffer, 4, 1, ifp) != 1) goto error;
  if (strncmp(buffer, "RNC", 3)) {
    fclose(ifp);
    if (strcmp(iname, oname)) return copy_file(iname, oname);
    return 0;
  }
  //
  rewind(ifp);
  packed = calloc(plen+8, 1);
  if (packed == NULL) {
    perror(pname);
    return 1;
  }
  if (fread(packed, plen, 1, ifp) != 1) goto error;
  fclose(ifp);
  ifp = NULL;
  //
  ulen = rnc_ulen(packed);
  if (ulen < 0) {
    free(packed);
    if (ulen == -1) return 0; // file wasn't RNC to start with
    fprintf(stderr, "%s\n", rnc_error(ulen));
    return 1;
  }
  //
  unpacked = malloc(ulen);
  if (unpacked == NULL) {
    perror(pname);
    return 1;
  }
  //
  ulen = rnc_unpack(packed, unpacked, NULL);
  if (ulen < 0) {
    fprintf(stderr, "%s\n", rnc_error(ulen));
    return 1;
  }
  //
  ofp = fopen(oname, "wb");
  if (!ofp) {
    perror(oname);
    return 1;
  }
  //
  if (fwrite(unpacked, ulen, 1, ofp) != 1) {
    fclose(ofp);
    unlink(oname);
    perror(oname);
    return 1;
  }
  fclose(ofp);
  //
  free(unpacked);
  free(packed);
  //
  return 0;
error:
  if (ofp != NULL) {
    fclose(ofp);
    unlink(oname);
  }
  if (ifp != NULL) fclose(ifp);
  //
  perror(iname);
  return 1;
}


int main (int argc, char *argv[]) {
  int mode = 0;
  //
  if (argc == 1) show_help(argv[0]);
  //
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-o") == 0) {
      if (mode != 0) {
        fprintf(stderr, "FATAL: too many '-o'!\n");
        exit(1);
      }
      mode = i;
    }
  }
  //
  if (mode && argc != 4) show_help(argv[0]);
  //
  switch (mode) {
    case 0:
      for (int i = 1; i < argc; ++i) if (main_unpack (*argv, argv[i], argv[i])) return 1;
      return 0;
    case 1: // -o if of
      return main_unpack(*argv, argv[2], argv[3]);
    case 2: // if -o of
      return main_unpack(*argv, argv[1], argv[3]);
    case 3: // if of -o
      return main_unpack(*argv, argv[1], argv[2]);
    default:
      fprintf(stderr, "Internal fault.\n");
      break;
  }
  return 1;
}

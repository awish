/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "SDL.h"
#include "SDL_endian.h"

#ifdef WIN32
# include <windows.h>
#endif

#include "awishcommon.h"
#include "resfile.h"
#include "video.h"
#include "mainloop.h"
#include "vm.h"
#include "gameglobals.h"
#include "game.h"
#include "title.h"
#include "polymod.h"


int goobers = 0;
int datfirst = 0;


////////////////////////////////////////////////////////////////////////////////
static SDL_Surface *loadCFScreen (const char *fname, SDL_Surface *old, int w, int h, int chained) {
  SDL_Surface *new;
  static Uint8 pal[256][3];
  static Uint32 opal[256];
  static Uint8 scr[64000];
  int rsz;
  uint8_t *diskdata = tryDiskFile(fname, &rsz);
  //
  if (diskdata == NULL) return old;
  if (rsz < w*h+(chained >= 0 ? 768 : 0)) { free(diskdata); return old; }
  //
  new = createSurface(320, 200);
  if (!new) { free(diskdata); return old; }
  SDL_SetColorKey(new, 0, 0); // clear colorkey info (it's the fullscreen image after all!)
  //
  for (int dy = 0; dy < 200; ++dy) {
    for (int dx = 0; dx < 320; ++dx) {
      putPixel2x(new, dx, dy, 0);
    }
  }
  //
  memcpy(scr, diskdata, w*h);
  if (chained >= 0) memcpy(pal, diskdata+w*h, 768);
  free(diskdata);
  //
  if (chained >= 0) {
    memcpy(opal, palette, 256*4);
    for (int f = 0; f < 256; ++f) {
      pal[f][0] <<= 2;
      if (pal[f][0] != 0) pal[f][0] |= 3;
      pal[f][1] <<= 2;
      if (pal[f][1] != 0) pal[f][1] |= 3;
      pal[f][2] <<= 2;
      if (pal[f][2] != 0) pal[f][2] |= 3;
      palette[f] = SDL_MapRGB(screen->format, pal[f][0], pal[f][1], pal[f][2]);
    }
  }
  //
  if (chained > 0) {
    for (int dy = 0; dy < h; ++dy) {
      for (int dx = 0; dx < w/4; ++dx) {
        for (int c = 0; c < 4; ++c) {
          putPixel2x(new, dx*4+c, dy, scr[dy*(w/4)+(w*h)/4*c+dx]);
        }
      }
    }
  } else {
    for (int dy = 0; dy < h; ++dy) {
      for (int dx = 0; dx < w; ++dx) {
        putPixel2x(new, dx, dy, scr[dy*w+dx]);
      }
    }
  }
  //
  memcpy(palette, opal, 256*4);
  SDL_FreeSurface(old);
  return new;
}


////////////////////////////////////////////////////////////////////////////////
static int loadFont (void) {
  int sz;
  uint8_t *buf = loadResFile(&resfile, 92, &sz);
  //
  if (buf == NULL) return -1;
  if (sz != 256*8) { free(buf); return -1; }
  memcpy(font, buf, 256*8);
  free(buf);
  return 0;
}


static int loadPalette (void) {
  int sz;
  uint8_t *buf = loadResFile(&resfile, 83, &sz);
  //
  if (buf == NULL) return -1;
  if (sz != 768) { free(buf); return -1; }
  for (int f = 0; f < 768; ++f) {
    buf[f] <<= 2;
    if (buf[f] != 0) buf[f] |= 3;
  }
  for (int f = 0; f < 256; ++f) palette[f] = SDL_MapRGB(screen->format, buf[f*3+0], buf[f*3+1], buf[f*3+2]);
  free(buf);
  return 0;
}


static SDL_Surface *loadImage (ResFile *resfile, int idx) {
  int sz;
  static Uint8 *img;
  SDL_Surface *res;
  //
  img = loadResFile(resfile, idx, &sz);
  if (img == NULL) return NULL;
  if (sz < 320*200) { free(img); return NULL; }
  res = createSurface(320, 200);
  if (res == NULL) { free(img); return NULL; }
  SDL_SetColorKey(res, 0, 0); // clear colorkey info (it's the fullscreen image after all!)
  for (int f = 0; f < 320*200; ++f) putPixel2x(res, f%320, f/320, img[f]);
  free(img);
  return res;
}


// return # of sprites loaded or <0 on error
static int loadSpriteBank (SpriteBank *bank, ResFile *resfile, int idx) {
  Uint8 *buf;
  int sz, pos;
  //
  if ((buf = loadResFile(resfile, idx, &sz)) == NULL) return -1;
  if (sz < 3) { free(buf); return -1; }
  pos = 0;
  for (int f = bank->count-1; f >= 0; --f) {
    SDL_FreeSurface(bank->spr[f][1]);
    SDL_FreeSurface(bank->spr[f][0]);
  }
  bank->count = 0;
  while (pos+4 <= sz) {
    int h = ((Uint32)buf[pos+0])+256*((Uint32)buf[pos+1]);
    int w = ((Uint32)buf[pos+2])+256*((Uint32)buf[pos+3]);
    //
    pos += 4;
    if (h > 0 && w > 0) {
      SDL_Surface *s0 = createSurface(w, h);
      SDL_Surface *s1 = createSurface(w, h);
      //
      if (!s0 || !s1) { free(buf); return -1; }
      //
      for (int y = 0; y < h; ++y) {
        for (int x = 0; x < w; ++x) {
          if (pos < sz) {
            putPixel2x(s0, x, y, buf[pos]);
            putPixel2x(s1, w-x-1, y, buf[pos]);
            ++pos;
          }
        }
      }
      bank->spr[bank->count][0] = s0;
      bank->spr[bank->count][1] = s1;
      ++(bank->count);
    }
  }
  free(buf);
  return bank->count;
}


/*
static void freeSpriteBank (SpriteBank *bank) {
  for (int f = bank->count-1; f >= 0; --f) {
    SDL_FreeSurface(bank->spr[f][1]);
    SDL_FreeSurface(bank->spr[f][0]);
  }
}
*/


////////////////////////////////////////////////////////////////////////////////
static void quitCleanupRes (void) {
  vmFreeLabels();
  //if (!disableSound) unloadAllSounds();
  deinitResFile(&sndfile);
  deinitResFile(&resfile);
  //for (int f = 0; f < sizeof(banks)/sizeof(SpriteBank); ++f) freeSpriteBank(&banks[f]);
}


////////////////////////////////////////////////////////////////////////////////
#include "sincostab.c"
static int awishRST (int tid, int opcode, int argc, int argv[], int *argp[]) {
  vmGVars[GVAR_RST_RESULT] = 0;
  //
  if (argv[0] == CONST_FRST_ML_TITLE) {
    setMainLoopTitle();
  } else if (argv[0] == CONST_FRST_ML_GAME) {
    setMainLoopGame();
  } else if (argv[0] == CONST_FRST_GET_MAX_THREADS) {
    vmGVars[GVAR_RST_RESULT] = VM_MAX_THREADS;
  } else if (argv[0] == CONST_FRST_GET_MAX_THREAD_ID) {
    vmGVars[GVAR_RST_RESULT] = vmLastThread();
  } else if (argv[0] == CONST_FRST_GET_RAND) {
    int nmin = 0, nmax = 32767;
    //
    switch (argc) {
      case 2: nmax = argv[1]; break;
      case 3: nmin = argv[1]; nmax = argv[2]; break;
    }
    if (nmin >= nmax) {
      vmGVars[GVAR_RST_RESULT] = nmin;
    } else {
      vmGVars[GVAR_RST_RESULT] = nmin+(randUInt32()%(nmax-nmin+1));
    }
  } else if (argv[0] == CONST_FRST_GET_SEED_H) {
    vmGVars[GVAR_RST_RESULT] = getSeedH();
  } else if (argv[0] == CONST_FRST_GET_SEED_L) {
    vmGVars[GVAR_RST_RESULT] = getSeedL();
  } else if (argv[0] == CONST_FRST_SET_SEED_H) {
    if (argc >= 2) setSeedH(argv[1]);
  } else if (argv[0] == CONST_FRST_SET_SEED_L) {
    if (argc >= 2) setSeedL(argv[1]);
  } else if (argv[0] == CONST_FRST_DEBUG_PRINT_STR) {
    int pos = 0, len = 0;
    //
    switch (argc) {
      case 1:
        pos = vmPop(tid);
        goto dolen;
      case 2:
        pos = argv[1];
dolen:  if (pos >= 0) {
          while (pos < vmCodeSize && vmCode[pos+len]) ++len;
        }
        break;
      case 3:
        pos = argv[1];
        len = argv[2];
        break;
    }
    if (goobers) {
      if (pos >= -255 && pos <= -1) {
        fputc(-pos, stderr);
      } else {
        for (; len > 0; --len, ++pos) if (pos >= 0 && pos < vmCodeSize) fputc(vmCode[pos], stderr);
      }
    }
  } else if (argv[0] == CONST_FRST_DEBUG_PRINT_NUM) {
    switch (argc) {
      case 1: argv[1] = vmPop(tid); // fallthru
      case 2: if (goobers) fprintf(stderr, "%d", argv[1]); break;
      case 3: if (goobers) fprintf(stderr, "%d,%d", argv[1], argv[2]); break;
    }
  } else if (argv[0] == CONST_FRST_PLAY_SOUND) {
    // arg1: sound index; arg2 (if present) channel; rst_result: -1 or channel
    if (argc == 1) argv[1] = vmPop(tid);
    if (argc < 2) argv[2] = -1;
    vmGVars[GVAR_RST_RESULT] = playSound(argv[1], argv[2]);
  } else if (argv[0] == CONST_FRST_STOP_CHANNEL) {
    // arg1: channel
    if (argc == 1) argv[1] = vmPop(tid);
    vmGVars[GVAR_RST_RESULT] = stopChannel(argv[1]);
  } else if (argv[0] == CONST_FRST_IS_CHANNEL_PLAYING) {
    // arg1: channel; rst_result: bool
    if (argc == 1) argv[1] = vmPop(tid);
    vmGVars[GVAR_RST_RESULT] = isChannelPlaying(argv[1]);
  } else if (argv[0] == CONST_FRST_LOAD_SOUND) {
    // arg1: sound index; rst_result: bool
    if (argc == 1) argv[1] = vmPop(tid);
    vmGVars[GVAR_RST_RESULT] = loadSound(argv[1]) ? 0 : 1;
  } else if (argv[0] == CONST_FRST_UNLOAD_SOUND) {
    // arg1: sound index; rst_result: bool
    if (argc == 1) argv[1] = vmPop(tid);
    vmGVars[GVAR_RST_RESULT] = unloadSound(argv[1]) ? 0 : 1;
  } else if (argv[0] == CONST_FRST_IS_SOUND_LOADED) {
    // arg1: sound index; rst_result: bool
    if (argc == 1) argv[1] = vmPop(tid);
    vmGVars[GVAR_RST_RESULT] = isSoundLoaded(argv[1]) ? 0 : 1;
  } else if (argv[0] == CONST_FRST_LOAD_SPRITES) {
    // arg1: bank, arg2: filename (0-terminated)
    int fnpos;
    //
    switch (argc) {
      case 1:
        argv[2] = vmPop(tid);
        argv[1] = vmPop(tid);
        break;
      case 2:
        argv[2] = vmPop(tid);
        break;
    }
    //
    fnpos = argv[2];
    vmGVars[GVAR_RST_RESULT] = -1;
    if (fnpos >= 0 && fnpos < vmCodeSize-1 && argv[1] >= 0 && argv[1] <= 256) {
      int end;
      //
      for (end = fnpos; end < vmCodeSize && vmCode[end]; ++end) ;
      if (end < vmCodeSize) {
        //fprintf(stderr, "!!! %d: [%s]\n", argv[1], (const char *)(vmCode+fnpos));
        vmGVars[GVAR_RST_RESULT] = loadSpriteBankFromFile(&banks[argv[1]], (const char *)(vmCode+fnpos));
      }
    }
  } else if (argv[0] == CONST_FRST_ADD_LEVEL_SPRITE || argv[0] == CONST_FRST_ADD_SPRITE) {
    // x, y, layer, bank, num, dir (can take 1st 2 args from stack too)
    int inlevel = (argv[0] == CONST_FRST_ADD_LEVEL_SPRITE);
    int x = 0, y = 0, layer, bank, num, dir;
    //
    dir = vmPop(tid);
    num = vmPop(tid);
    bank = vmPop(tid);
    layer = vmPop(tid);
    switch (argc) {
      case 1:
        y = vmPop(tid);
        x = vmPop(tid);
        break;
      case 2:
        y = vmPop(tid);
        x = argv[1];
        break;
      case 3:
        y = argv[2];
        x = argv[1];
        break;
    }
    //fprintf(stderr, "x=%d, y=%d, layer=%d, bank=%d, num=%d, dir=%d, inlevel=%d\n", x, y, layer, bank, num, dir, inlevel);
    addSpriteToLayer(x, y, layer, bank, num, dir, inlevel);
  } else if (argv[0] == CONST_FRST_START_POLY) {
    int angle = vmPop(tid);
    int scale = vmPop(tid);
    int ofsx = 0, ofsy = 0;
    //
    switch (argc) {
      case 1:
        ofsy = vmPop(tid);
        ofsx = vmPop(tid);
        break;
      case 2:
        ofsy = vmPop(tid);
        ofsx = argv[1];
        break;
      case 3:
        ofsy = argv[2];
        ofsx = argv[1];
        break;
    }
    pmStart(ofsx, ofsy, scale, angle);
  } else if (argv[0] == CONST_FRST_ADD_POLY_POINT) {
    switch (argc) {
      case 1:
        argv[2] = vmPop(tid);
        argv[1] = vmPop(tid);
        break;
      case 2:
        argv[2] = vmPop(tid);
        break;
    }
    pmAddPoint(argv[1], argv[2]);
  } else if (argv[0] == CONST_FRST_END_POLY) {
    switch (argc) {
      case 1:
        argv[2] = vmPop(tid);
        argv[1] = vmPop(tid);
        break;
      case 2:
        argv[2] = vmPop(tid);
        break;
    }
    pmDone(argv[1], argv[2]);
  } else if (argv[0] == CONST_FRST_DRAW_TEXT || argv[0] == CONST_FRST_TEXT_WIDTH) {
    // arg1: addr; arg2: len, x, y, scale, angle, color
    // arg1: addr; arg2: len, scale, angle
    int color = (argv[0] == CONST_FRST_DRAW_TEXT) ? vmPop(tid) : 0;
    int angle = vmPop(tid);
    int scale = vmPop(tid);
    int y = (argv[0] == CONST_FRST_DRAW_TEXT) ? vmPop(tid) : 0;
    int x = (argv[0] == CONST_FRST_DRAW_TEXT) ? vmPop(tid) : 0;
    int addr = 0, len = -1;
    const char *str = (const char *)vmCode;
    //
    switch (argc) {
      case 1:
        addr = vmPop(tid);
        break;
      case 2:
        addr = argv[1];
        break;
      case 3:
        addr = argv[1];
        len = argv[2];
        break;
    }
    if (len != 0 && addr >= 0 && addr < vmCodeSize) {
      str += addr;
      if (len < 0) {
        for (len = 0; addr+len < vmCodeSize && str[len]; ++len) ;
      } else {
        if (addr+len > vmCodeSize) len = vmCodeSize-addr;
      }
      if (str != NULL && len > 0) {
        if (argv[0] == CONST_FRST_TEXT_WIDTH) {
          char *s = calloc(len+1, 1);
          //
          if (s == NULL) fatal("out of memory");
          if (len > 0) memcpy(s, str, len);
          vmGVars[GVAR_RST_RESULT] = polymodStr(NULL, s, 0, 0, scale, angle, 0, 255);
          free(s);
        } else {
          textAdd(str, len, x, y, scale, angle, color, 255);
        }
      }
    }
  } else if (argv[0] == CONST_FRST_COS || argv[0] == CONST_FRST_SIN) {
    if (argc < 2) argv[1] = vmPop(tid);
    argv[1] %= 360; if (argv[1] < 0) argv[1] += 360;
    if (argv[0] == CONST_FRST_COS) argv[1] = (argv[1]+90)%360;
    vmGVars[GVAR_RST_RESULT] = sintab[argv[1]];
  } else {
    if (gameRSTCB) return gameRSTCB(tid, opcode, argc, argv, argp);
    fatal("invalid RST: %d", argv[0]);
  }
  return 0; // continue
}


////////////////////////////////////////////////////////////////////////////////
static int checkLevelCode (const char *t) {
  int ctrd;
  //
  if (!t || !t[0]) return -1;
  ctrd = vmNewThread(0);
  for (int level = 0; level < vmGVars[GVAR_MAX_LEVEL]; ++level) {
    //fprintf(stderr, "%d/%d\n", level+1, vmGVars[GVAR_MAX_LEVEL]);
    vmPush(ctrd, level);
    if (vmExecuteBSR(ctrd, CODE_ENTRY_GET_LEVEL_CODE, 0) == 0) {
      int pos = vmGVars[GVAR_LEVEL_CODE_OFS], len = vmGVars[GVAR_LEVEL_CODE_LEN], ok = 1;
      //
      if (strlen(t) == len && pos >= 0 && len > 0 && pos+len <= vmCodeSize) {
        //fwrite(vmCode+pos, len, 1, stderr);
        //fprintf(stderr, " [%s]\n", t);
        for (int f = 0; f < len; ++f) {
          //fprintf(stderr, "%c %c\n", tolower(t[f]), tolower(vmCode[pos+f]));
          if (tolower(t[f]) != tolower(vmCode[pos+f])) { ok = 0; break; }
        }
        if (ok) {
          if (goobers) fprintf(stderr, "found code for level #%02d\n", level+1);
          return level;
        }
      }
    } else {
      if (goobers) fprintf(stderr, "sorry!\n");
      break;
    }
  }
  vmKillThread(ctrd);
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
#define PUSH_BACK(_c)  (*ress)[dpos++] = (_c)
#define DECODE_TUPLE(tuple,bytes) \
  for (tmp = bytes; tmp > 0; tmp--, tuple = (tuple & 0x00ffffff)<<8) \
    PUSH_BACK((char)((tuple >> 24)&0xff))

// returns ress length
static int ascii85Decode (char **ress, const char *srcs/*, int start, int length*/) {
  static uint32_t pow85[5] = { 85*85*85*85UL, 85*85*85UL, 85*85UL, 85UL, 1UL };
  const uint8_t *data = (const uint8_t *)srcs;
  int len = strlen(srcs);
  uint32_t tuple = 0;
  int count = 0, c = 0;
  int dpos = 0;
  int start = 0, length = len;
  int tmp;
  //
  if (start < 0) start = 0; else { len -= start; data += start; }
  if (length < 0 || len < length) length = len;
  /*
  if (length > 0) {
    int xlen = 4*((length+4)/5);
    kstringReserve(ress, xlen);
  }
  */
  //
  *ress = (char *)calloc(1, len+1);
  for (int f = length; f > 0; --f, ++data) {
    c = *data;
    if (c <= ' ') continue; // skip blanks
    switch (c) {
      case 'z': // zero tuple
      if (count != 0) {
        //xdlog("%s: z inside ascii85 5-tuple\n", file);
        free(*ress);
        *ress = NULL;
        return -1;
      }
      PUSH_BACK('\0');
      PUSH_BACK('\0');
      PUSH_BACK('\0');
      PUSH_BACK('\0');
      break;
    case '~': // '~>': end of sequence
      if (f < 1 || data[1] != '>') { free(*ress); return -2; } // error
      if (count > 0) { f = -1; break; }
    default:
      if (c < '!' || c > 'u') {
        //xdlog("%s: bad character in ascii85 region: %#o\n", file, c);
        free(*ress);
        return -3;
      }
      tuple += ((uint8_t)(c-'!'))*pow85[count++];
      if (count == 5) {
        DECODE_TUPLE(tuple, 4);
        count = 0;
        tuple = 0;
      }
      break;
    }
  }
  // write last (possibly incomplete) tuple
  if (count-- > 0) {
    tuple += pow85[count];
    DECODE_TUPLE(tuple, count);
  }
  return dpos;
}

#undef PUSH_BACK
#undef DECODE_TUPLE


static void decodeBA (char *str, int len) {
  char pch = 42;
  //
  for (int f = 0; f < len; ++f, ++str) {
    char ch = *str;
    //
    ch = (ch-f-1)^pch;
    *str = ch;
    pch = ch;
  }
}


static void printEC (const char *txt) {
  char *dest;
  int len;
  //
  if ((len = ascii85Decode(&dest, txt)) >= 0) {
    decodeBA(dest, len);
    fprintf(stderr, "%s\n", dest);
    free(dest);
  }
}


static int isStr85Equ (const char *txt, const char *str) {
  char *dest;
  int len, res = 0;
  //
  if ((len = ascii85Decode(&dest, txt)) >= 0) {
    res = (strcmp(dest+1, str) == 0); // +1 to ignore '/'
    free(dest);
  }
  return res;
}


static int checkEGG (const char *str) {
  if (isStr85Equ("06:]JASq", str) || isStr85Equ("0/i", str)) {
    printEC(
      "H8lZV&6)1>+AZ>m)Cf8;A1/cP+CnS)0OJ`X.QVcHA4^cc5r3=m1c%0D3&c263d?EV6@4&>"
      "3DYQo;c-FcO+UJ;MOJ$TAYO@/FI]+B?C.L$>%:oPAmh:4Au)>AAU/H;ZakL2I!*!%J;(AK"
      "NIR#5TXgZ6c'F1%^kml.JW5W8e;ql0V3fQUNfKpng6ppMf&ip-VOX@=jKl;#q\"DJ-_>jG"
      "8#L;nm]!q;7c+hR6p;tVY#J8P$aTTK%c-OT?)<00,+q*8f&ff9a/+sbU,:`<H*[fk0o]7k"
      "^l6nRkngc6Tl2Ngs!!P2I%KHG=7n*an'bsgn>!*8s7TLTC+^\\\"W+<=9^%Ol$1A1eR*Be"
      "gqjEag:M0OnrC4FBY5@QZ&'HYYZ#EHs8t4$5]!22QoJ3`;-&=\\DteO$d6FBqT0E@:iu?N"
      "a5ePUf^_uEEcjTDKfMpX/9]DFL8N-Ee;*8C5'WgbGortZuh1\\N0;/rJB6'(MSmYiS\"6+"
      "<NK)KDV3e+Ad[@).W:%.dd'0h=!QUhghQaNNotIZGrpHr-YfEuUpsKW<^@qlZcdTDA!=?W"
      "Yd+-^`'G8Or)<0-T&CT.i+:mJp(+/M/nLaVb#5$p2jR2<rl7\"XlngcN`mf,[4oK5JLr\\"
      "m=X'(ue;'*1ik&/@T4*=j5t=<&/e/Q+2=((h`>>uN(#>&#i>2/ajK+=eib1coVe3'D)*75"
      "m_h;28^M6p6*D854Jj<C^,Q8Wd\"O<)&L/=C$lUAQNN<=eTD:A6kn-=EItXSss.tAS&!;F"
      "EsgpJTHIYNNnh'`kmX^[`*ELOHGcWbfPOT`J]A8P`=)AS;rYlR$\"-.RG440lK5:Dg?G'2"
      "['dE=nEm1:k,,Se_=%-6Z*L^J[)EC"
    );
    return 1;
  }
  if (isStr85Equ("04Jj?B)", str)) {
    printEC(
      "IPaSa(`c:T,o9Bq3\\)IY++?+!-S9%P0/OkjE&f$l.OmK'Ai2;ZHn[<,6od7^8;)po:HaP"
      "m<'+&DRS:/1L7)IA7?WI$8WKTUB2tXg>Zb$.?\"@AIAu;)6B;2_PB5M?oBPDC.F)606Z$V"
      "=ONd6/5P*LoWKTLQ,d@&;+Ru,\\ESY*rg!l1XrhpJ:\"WKWdOg?l;=RHE:uU9C?aotBqj]"
      "=k8cZ`rp\"ZO=GjkfD#o]Z\\=6^]+Gf&-UFthT*hN"
    );
    return 1;
  }
  if (isStr85Equ("04o69A7Tr", str)) {
    printEC(
      "Ag7d[&R#Ma9GVV5,S(D;De<T_+W).?,%4n+3cK=%4+0VN@6d\")E].np7l?8gF#cWF7SS_m"
      "4@V\\nQ;h!WPD2h#@\\RY&G\\LKL=eTP<V-]U)BN^b.DffHkTPnFcCN4B;]8FCqI!p1@H*_"
      "jHJ<%g']RG*MLqCrbP*XbNL=4D1R[;I(c*<FuesbWmSCF1jTW+rplg;9[S[7eDVl6YsjT"
    );
    return 1;
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
#ifdef _WIN32
# include "cmdline.c"
#endif


////////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  int csz;
  int disableSound = 0;
  //
#ifdef _WIN32
  cmdLineParse();
  argc = k8argc;
  argv = k8argv;
#endif
  //
  memset(banks, 0, sizeof(banks));
  //
  polymodInitialize();
  //
  for (int f = 1; f < argc; ++f) {
    int eaten = 1;
    //
    if (strcmp(argv[f], "-goobers") == 0) goobers = 1;
    else if (strcmp(argv[f], "--goobers") == 0) goobers = 1;
    else if (strcmp(argv[f], "-dat") == 0) datfirst = 1;
    else if (strcmp(argv[f], "--dat") == 0) datfirst = 1;
    else if (strcmp(argv[f], "-trace") == 0) vmDebugTrace = 1;
    else if (strcmp(argv[f], "--trace") == 0) vmDebugTrace = 1;
    else if (strcmp(argv[f], "-nosound") == 0) disableSound = 1;
    else if (strcmp(argv[f], "--nosound") == 0) disableSound = 1;
    else if (strcmp(argv[f], "-fs") == 0) optFullscreen = 1;
    else if (strcmp(argv[f], "--fs") == 0) optFullscreen = 1;
    else if (strcmp(argv[f], "-tracelog") == 0 || strcmp(argv[f], "--tracelog") == 0) {
      eaten = 1;
      vmDebugTrace = 1;
      vmDebugOutput = fopen("ztrace.log", "w");
    } else {
      if (argv[f][0] == '-' && checkEGG(argv[f]+1)) exit(1);
      eaten = 0;
    }
    if (eaten > 0) {
      for (int c = f+eaten; c < argc; ++c) argv[c-1] = argv[c];
      argv[argc -= eaten] = NULL;
      f -= eaten;
    }
  }
  //
  createHomeDir();
  //
  initResFile(&resfile, "RESOURCE.DAT");
  initResFile(&sndfile, "RESOURCE.SND");
  atexit(quitCleanupRes);
  //
  if (loadFont() != 0) {
    fprintf(stderr, "FATAL: can't load font!\n");
    exit(1);
  }
  //
  sdlInit();
  initVideo();
  //
  clearSpriteLayers();
  //
  if (loadPalette() != 0) {
    fprintf(stderr, "FATAL: can't load palette!\n");
    exit(1);
  }
  //
  {
    SurfaceLock lock;
    //
    lockSurface(&lock, screen);
    drawString(screen, "loading...", 2, 200-10, 1);
    unlockSurface(&lock);
    SDL_Flip(screen);
  }
  //
  for (int f = 0; f < 8; ++f) {
    if ((backs[(f+1)%8] = loadImage(&resfile, f)) == NULL) {
      fprintf(stderr, "FATAL: can't load image #%d!\n", f);
      exit(1);
    }
  }
  if ((backs[8] = loadImage(&resfile, 8)) == NULL) {
    fprintf(stderr, "FATAL: can't load image #%d!\n", 8);
    exit(1);
  }
  //
  backs[0] = loadCFScreen("CFTITLE.DAT", backs[0], 320, 200, 1);
  backs[0] = loadCFScreen("cftitle.dat", backs[0], 320, 200, 1);
  //
  memset(banks, 0, sizeof(banks));
  for (int f = 84; f <= 90; ++f) {
    if (loadSpriteBank(&banks[f], &resfile, f) <= 0) {
      fprintf(stderr, "FATAL: can't load sprite bank #%d!\n", f);
      exit(1);
    }
  }
  if (loadSpriteBankFromFile(&banks[256], "sprites/cursors.spr") <= 0) {
    fprintf(stderr, "FATAL: can't load 'cursors' sprite bank!\n");
    exit(1);
  }
  //
  if (vmInitialize() != 0) fatal("can't init VM");
  //
  memset(vmGVars, 0, sizeof(vmGVars));
  vmRSTCB = awishRST;
  //
  vmAddLabel("flag_skip_title", LB_GVAR, 120, 1); // 1: public
  vmGVars[120] = 0;
  //
  vmCodeSize = 0;
  if ((csz = loadCodeFile(&resfile, vmCodeSize, 93, 0)) < 1) {
    fprintf(stderr, "FATAL: can't load VM code!\n");
    exit(1);
  }
  vmCodeSize += csz;
  initLabels();
  //
  if (goobers) {
    int rsz = 0;
    uint8_t *buf;
    //
    if ((buf = loadDiskFileEx("goobers.vmd", &rsz)) != NULL) {
      if (buf != NULL) {
        csz = vmLoadCodeFileFromDump(buf, rsz, vmCodeSize, vmMaxGVar, vmMaxTVar, &vmMaxGVar, &vmMaxTVar);
        free(buf);
        if (csz > 0) vmCodeSize += csz;
      }
    }
  }
  //
  //vmGVars[GVAR_KEY_QUIT] = 0;
  //vmGVars[GVAR_KEY_START] = 0;
  vmGVars[GVAR_POLYFIX_BASE] = POLYFIX_BASE;
  vmGVars[GVAR_GOOBERS] = goobers;
  vmGVars[GVAR_SOUND_DISABLED] = disableSound;
  //
  vmSetPC(0, CODE_ENTRY_TITLE);
  if (vmExecuteBSR(0, CODE_ENTRY_MAIN_INIT, 0) != 0) fatal("can't initialize game");
  //if (goobers) fprintf(stderr, "MAX LEVEL: %d\n", vmGVars[GVAR_MAX_LEVEL]);
  //
  {
    VMLabelInfo *l = vmFindLabel("entry_goobers_init");
    //
    if (l != NULL && l->type == LB_CODE) {
      if (vmExecuteBSR(0, l->value, 0) != 0) fatal("can't initialize game");
    }
  }
  //
  if (argc > 1) {
    // check level code
    for (int f = 1; f < argc; ++f) {
      int lvl = checkLevelCode(argv[f]);
      //
      if (lvl >= 0) {
        vmGVars[GVAR_START_LEVEL] = lvl;
        break;
      }
    }
  }
  //
  mainLoop();
  //
  return 0;
}


#ifdef WIN32
int CALLBACK WinMain (HINSTANCE hInstance, HINSTANCE unused__, LPSTR lpszCmdLine, int nCmdShow) {
  char *shit[] = { (char *)"shit", NULL };
  return SDL_main(1, shit);
}
#endif

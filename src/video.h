/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _VIDEO_H_
#define _VIDEO_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "SDL.h"

#include "awishcommon.h"


////////////////////////////////////////////////////////////////////////////////
extern int optFullscreen;
extern SDL_Surface *screen;
extern SDL_Surface *screenReal;
extern SDL_Surface *backbuf; // 'background' buffer
extern Uint32 palette[256]; // converted
extern Uint8 font[256][8];


////////////////////////////////////////////////////////////////////////////////
extern void sdlInit (void);
extern void initVideo (void);
extern void switchFullScreen (void);


////////////////////////////////////////////////////////////////////////////////
// create 24bpp surface with color key
extern SDL_Surface *createSurface (int w, int h);


////////////////////////////////////////////////////////////////////////////////
// NO LOCKING!
static inline void putPixelC32 (SDL_Surface *s, int x, int y, Uint32 col) {
  if (s && x >= s->clip_rect.x && y >= s->clip_rect.y && x < s->clip_rect.x+s->clip_rect.w && y < s->clip_rect.y+s->clip_rect.h) {
    *(Uint32 *)(((Uint8 *)s->pixels)+(y*s->pitch)+(x*s->format->BytesPerPixel)) = col;
  }
}


#define _INTERNAL_VIDEO_ABLEND_MACRO_(n) \
  SDL_GetRGB(bits[n], s->format, &r, &g, &b); \
  r = (((pr-r)*alpha)>>8)+r; \
  g = (((pg-g)*alpha)>>8)+g; \
  b = (((pb-b)*alpha)>>8)+b; \
  bits[n] = SDL_MapRGB(s->format, r, g, b); \


static inline void putPixel2xA32 (SDL_Surface *s, int x, int y, Uint32 col, Uint8 alpha) {
  if (s && x >= s->clip_rect.x && y >= s->clip_rect.y && x < s->clip_rect.x+s->clip_rect.w && y < s->clip_rect.y+s->clip_rect.h) {
    Uint8 bpp = s->format->BytesPerPixel;
    Uint32 *bits = (Uint32 *)(((Uint8 *)s->pixels)+(y*s->pitch)+(x*bpp));
    unsigned char r, pr, g, pg, b, pb;
    //
    SDL_GetRGB(col, s->format, &pr, &pg, &pb);
    _INTERNAL_VIDEO_ABLEND_MACRO_(0)
  }
}


static inline void putPixel2x (SDL_Surface *s, int x, int y, Uint8 col) {
  putPixelC32(s, x, y, palette[col]);
}


static inline void putPixelA2x (SDL_Surface *s, int x, int y, Uint8 clr, Uint8 alpha) {
  if (alpha != 0) {
    if (alpha != 255) {
      putPixel2xA32(s, x, y, palette[clr], alpha);
    } else {
      putPixel2x(s, x, y, clr);
    }
  }
}


/*
static inline void hlineC32 (SDL_Surface *s, int x, int y, int len, Uint32 col) {
  if (s && y >= s->clip_rect.y && x < s->clip_rect.x+s->clip_rect.w && y < s->clip_rect.y+s->clip_rect.h) {
    if (x < s->clip_rect.x) {
      len -= s->clip_rect.x-x;
      x = s->clip_rect.x;
    }
    if (x+len > s->clip_rect.x+s->clip_rect.w) len = s->clip_rect.x+s->clip_rect.w-x;
    if (len > 0) {
      Uint8 bpp = s->format->BytesPerPixel;
      Uint32 *bits = (Uint32 *)(((Uint8 *)s->pixels)+(y*s->pitch)+(x*bpp));
      //
      for (; len > 0; --len) *bits++ = col;
    }
  }
}


static inline void hline2x (SDL_Surface *s, int x, int y, int len, Uint8 col) {
  x *= 2;
  y *= 2;
  len *= 2;
  hlineC32(s, x, y, len, palette[col]);
  hlineC32(s, x, y+1, len, palette[col]);
}
*/


////////////////////////////////////////////////////////////////////////////////
extern void blitSurfaceEx (SDL_Surface *dests, int destx, int desty, SDL_Surface *srcs, int srcx, int srcy, int srcw, int srch);


static inline void blitSurface (SDL_Surface *dests, int destx, int desty, SDL_Surface *srcs) {
  blitSurfaceEx(dests, destx, desty, srcs, 0, 0, -1, -1);
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  SDL_Surface *s;
  int needUnlock;
} SurfaceLock;


extern void lockSurface (SurfaceLock *lock, SDL_Surface *s);
extern void unlockSurface (SurfaceLock *lock);


////////////////////////////////////////////////////////////////////////////////
// surface must be locked!
extern void drawChar (SDL_Surface *s, char ch, int x, int y, Uint8 clr);
extern void drawString (SDL_Surface *s, const char *str, int x, int y, Uint8 clr);


#endif

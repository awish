/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "video.h"

#ifndef AWISH_NO_SOUND
# include "SDL_mixer.h"
#endif


////////////////////////////////////////////////////////////////////////////////
int optFullscreen = 0;
SDL_Surface *screen = NULL;
SDL_Surface *screenReal = NULL;
SDL_Surface *backbuf = NULL;
Uint32 palette[256]; // converted
Uint8 font[256][8];


////////////////////////////////////////////////////////////////////////////////
static void quitCleanupCB (void) {
  //if (TTF_WasInit()) TTF_Quit();
#ifndef AWISH_NO_SOUND
  Mix_CloseAudio();
#endif
  SDL_Quit();
}


////////////////////////////////////////////////////////////////////////////////
void sdlInit (void) {
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER
#ifndef AWISH_NO_SOUND
       | SDL_INIT_AUDIO
#endif
      ) < 0) {
    fprintf(stderr, "FATAL: can't set videomode!\n");
    exit(1);
  }
  SDL_WM_SetCaption("Awish", "Awish");
  SDL_EnableKeyRepeat(200, 25);
  //SDL_EnableUNICODE(1); // just4fun
  SDL_EnableUNICODE(0); // just4fun
#ifndef AWISH_NO_SOUND
# if SDL_MIXER_PATCHLEVEL >= 12
  Mix_Init(0);
# endif
  if (Mix_OpenAudio(22050, AUDIO_U8, 0, 512)) {
    SDL_Quit();
    fprintf(stderr, "FATAL: unable to open audio!\n");
    exit(1);
  }
#endif
  atexit(quitCleanupCB);
}


void initVideo (void) {
  if (optFullscreen) {
    screenReal = SDL_SetVideoMode(640, 480, 32, SDL_SWSURFACE | SDL_FULLSCREEN);
    screen = SDL_CreateRGBSurface(SDL_SWSURFACE, 320, 200, screenReal->format->BitsPerPixel, screenReal->format->Rmask, screenReal->format->Gmask, screenReal->format->Bmask, 0);
  } else {
    screenReal = SDL_SetVideoMode(640, 400, 32, SDL_SWSURFACE/*|SDL_DOUBLEBUF*/);
    screen = SDL_CreateRGBSurface(SDL_SWSURFACE, 320, 200, screenReal->format->BitsPerPixel, screenReal->format->Rmask, screenReal->format->Gmask, screenReal->format->Bmask, 0);
  }
  if (screen == NULL) {
    fprintf(stderr, "FATAL: can't set videomode!\n");
    exit(1);
  }
  //
  if (screen->format->BitsPerPixel != 32 || (screenReal != NULL && screenReal->format->BitsPerPixel != 32)) {
    fprintf(stderr, "FATAL: videomode initialization error!\n");
    SDL_Quit();
    exit(1);
  }
  //
  backbuf = createSurface(320, 200);
  if (backbuf == NULL) {
    fprintf(stderr, "FATAL: can't create backbuffer!\n");
    SDL_Quit();
    exit(1);
  }
  SDL_SetColorKey(backbuf, 0, 0);
  //fprintf(stderr, "BPP: %u\n", screen->format->BytesPerPixel);
  SDL_ShowCursor(0);
}


void switchFullScreen (void) {
  if (optFullscreen) {
    screenReal = SDL_SetVideoMode(640, 400, 32, SDL_SWSURFACE/*|SDL_DOUBLEBUF*/);
  } else {
    screenReal = SDL_SetVideoMode(640, 480, 32, SDL_SWSURFACE | SDL_FULLSCREEN);
  }
  optFullscreen = !optFullscreen;
  if (screenReal == NULL || screenReal->format->BitsPerPixel != 32) {
    fprintf(stderr, "FATAL: can't switch videomode!\n");
    exit(1);
  }
}


////////////////////////////////////////////////////////////////////////////////
// create 24bpp surface with color key
SDL_Surface *createSurface (int w, int h) {
  //Uint32 rmask, gmask, bmask/*, amask*/;
  SDL_Surface *s;
  //
  if (w < 1 || h < 1) return NULL;
/*
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    //amask = 0x000000ff;
#else
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    //amask = 0xff000000;
#endif
  s = SDL_CreateRGBSurface(SDL_SRCCOLORKEY, w, h, 32, rmask, gmask, bmask, amask);
*/
  s = SDL_CreateRGBSurface(SDL_SRCCOLORKEY, w, h, screen->format->BitsPerPixel, screen->format->Rmask, screen->format->Gmask, screen->format->Bmask, screen->format->Amask);
  if (s != NULL) {
    SDL_SetColorKey(s, SDL_SRCCOLORKEY, palette[255]);
  }
  return s;
}


void blitSurfaceEx (SDL_Surface *dests, int destx, int desty, SDL_Surface *srcs, int srcx, int srcy, int srcw, int srch) {
  SDL_Rect src, dst;
  //
  if (srcw < 0) srcw = srcs->w;
  if (srch < 0) srch = srcs->h;
  src.x = srcx;
  src.y = srcy;
  src.w = srcw;
  src.h = srch;
  dst.x = destx;
  dst.y = desty;
  SDL_BlitSurface(srcs, &src, dests, &dst);
}


////////////////////////////////////////////////////////////////////////////////
void lockSurface (SurfaceLock *lock, SDL_Surface *s) {
  if (lock) {
    lock->s = s;
    lock->needUnlock = 0;
    if (s != NULL && SDL_MUSTLOCK(s)) {
      lock->needUnlock = (SDL_LockSurface(s) == 0);
    }
  }
}


void unlockSurface (SurfaceLock *lock) {
  if (lock && lock->s && lock->needUnlock) {
    SDL_UnlockSurface(lock->s);
    lock->needUnlock = 0;
  }
}


////////////////////////////////////////////////////////////////////////////////
// surface must be locked!
void drawChar (SDL_Surface *s, char ch, int x, int y, Uint8 clr) {
  unsigned char c = (unsigned char)(ch);
  //
  for (int dy = 0; dy < 8; ++dy) {
    Uint8 b = font[c][dy];
    //
    for (int dx = 0; dx < 8; ++dx) {
      if (b&0x80) putPixel2x(s, x+dx, y+dy, clr);
      b = (b&0x7f)<<1;
    }
  }
}


void drawString (SDL_Surface *s, const char *str, int x, int y, Uint8 clr) {
  for (; *str; ++str) {
    drawChar(s, *str, x, y, clr);
    x += 8;
  }
}

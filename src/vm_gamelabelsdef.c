int CODE_ENTRY_GET_LEVEL_CODE;
int GVAR_TITLE_PHASE;
int TVAR_AFRAME;
int TVAR_ALAST_FRAME;
int TVAR_AFIRST_FRAME;
int TVAR_ANIMATED;
int TVAR_POS_TY;
int TVAR_POS_TX;
int TVAR_POS_Y;
int TVAR_POS_X;
int TVAR_SPR_ITEM;
int TVAR_SPR_DIR;
int TVAR_SPR_NUM;
int TVAR_SPR_BANK;
int TVAR_ITEM_ID;
int TVAR_IS_MENU_THREAD;
int GVAR_IN_MENU;
int GVAR_SOUND_DISABLED;
int GVAR_POLYFIX_BASE;
int GVAR_MOUSE_CURSOR;
int GVAR_MOUSE_HIDDEN;
int GVAR_MOUSE_BUTTONS;
int GVAR_MOUSE_Y;
int GVAR_MOUSE_X;
int GVAR_PROF_ITEM;
int GVAR_SCR_Y;
int GVAR_SCR_X;
int GVAR_VIS_HEIGHT;
int GVAR_VIS_WIDTH;
int GVAR_MAP_HEIGHT;
int GVAR_MAP_WIDTH;
int GVAR_LEVEL_BACKPIC;
int GVAR_LEVEL_CODE_LEN;
int GVAR_LEVEL_CODE_OFS;
int GVAR_CUR_LEVEL;
int GVAR_GAME_STATE;
int GVAR_START_LEVEL;
int GVAR_MAX_LEVEL;
int GVAR_KEY_NLEV_CHEAT;
int GVAR_KEY_PLEV_CHEAT;
int GVAR_KEY_WALK_CHEAT;
int GVAR_KEY_FALL_CHEAT;
int GVAR_KEY_MINIMAP;
int GVAR_KEY_USE;
int GVAR_KEY_TAKE;
int GVAR_KEY_DOWN;
int GVAR_KEY_UP;
int GVAR_KEY_RIGHT;
int GVAR_KEY_LEFT;
int GVAR_KEY_ESCAPE;
int GVAR_KEY_RESTART;
int GVAR_KEY_START;
int GVAR_KEY_QUIT;
int GVAR_GAME_WANT_KEY;
int GVAR_GOOBERS;
int GVAR_RST_RESULT;
int CONST_BANK_IM_PROF;
int CONST_BANK_MAP_ITEMS;
int CONST_BANK_ITEMS;
int CONST_BANK_MAP_TILES;
int CONST_BANK_BG_TILES;
int CONST_BANK_FG_TILES;
int CONST_BANK_PROF;
int CONST_GAME_STATE_DEAD;
int CONST_GAME_STATE_COMPLETE;
int CONST_GAME_STATE_STARTED;
int CONST_GAME_STATE_PLAYING;
int CONST_FRST_GET_GOOBERS_ONLOAD;
int CONST_FRST_GET_GOOBERS_INITER;
int CONST_FRST_DEBUG_ABORT;
int CONST_FRST_DEBUG_PRINT_NUM;
int CONST_FRST_DEBUG_PRINT_STR;
int CONST_FRST_SIN;
int CONST_FRST_COS;
int CONST_FRST_SET_SEED_L;
int CONST_FRST_SET_SEED_H;
int CONST_FRST_GET_SEED_L;
int CONST_FRST_GET_SEED_H;
int CONST_FRST_GET_RAND;
int CONST_FRST_GET_MAX_THREAD_ID;
int CONST_FRST_GET_MAX_THREADS;
int CONST_FRST_TEXT_WIDTH;
int CONST_FRST_DRAW_TEXT;
int CONST_FRST_END_POLY;
int CONST_FRST_ADD_POLY_POINT;
int CONST_FRST_START_POLY;
int CONST_FRST_IS_SOUND_LOADED;
int CONST_FRST_UNLOAD_SOUND;
int CONST_FRST_LOAD_SOUND;
int CONST_FRST_IS_CHANNEL_PLAYING;
int CONST_FRST_STOP_CHANNEL;
int CONST_FRST_PLAY_SOUND;
int CONST_FRST_ADD_SPRITE;
int CONST_FRST_ADD_LEVEL_SPRITE;
int CONST_FRST_LOAD_SPRITES;
int CONST_FRST_SET_ITEM_NAME;
int CONST_FRST_SET_LEVEL_CODE;
int CONST_FRST_SET_LEVEL_NAME;
int CONST_FRST_SET_LEVEL_SIZE;
int CONST_FRST_GET_LEVEL_FILE_WORD;
int CONST_FRST_GET_LEVEL_FILE_BYTE;
int CONST_FRST_GET_LEVEL_INITER;
int CONST_FRST_GET_LEVEL_LOADER;
int CONST_FRST_GET_LEVEL_DATA_SIZE;
int CONST_FRST_OPEN_LEVEL_FILE;
int CONST_FRST_MINIMAP;
int CONST_FRST_ML_GAME;
int CONST_FRST_ML_TITLE;
int CODE_ENTRY_GAME_RESTART_LEVEL;
int CODE_ENTRY_GAME_INITIALIZE;
int CODE_ENTRY_TITLE;
int CODE_ENTRY_MAIN_INIT;

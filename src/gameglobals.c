/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef AWISH_NO_SOUND
# include "SDL_mixer.h"
#endif

#include "video.h"
#include "polymod.h"

#include "gameglobals.h"


////////////////////////////////////////////////////////////////////////////////
ResFile resfile;
ResFile sndfile;

SDL_Surface *backs[9];
SpriteBank banks[257];

//int disableSound = 0;

extern int goobers;

/*
int mouseX = 0;
int mouseY = 0;
int mouseButtons = 0;
int mouseVisible = 1;
*/


#define SOUND_DISABLED  (vmGVars[GVAR_SOUND_DISABLED]!=0)


////////////////////////////////////////////////////////////////////////////////
__attribute__((__noreturn__)) __attribute__((format(printf, 1, 2))) void fatal (const char *fmt, ...) {
  va_list ap;
  //
  fprintf(stderr, "AWISH FATAL: ");
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fprintf(stderr, "\n");
  exit(1);
}


////////////////////////////////////////////////////////////////////////////////
// These values are not magical, just the default values Marsaglia used.
// Any pair of unsigned integers should be fine.
#define DEFAULT_M_W  (521288629LL)
#define DEFAULT_M_Z  (362436069LL)

static Uint32 m_w = DEFAULT_M_W;
static Uint32 m_z = DEFAULT_M_Z;


AWISH_PURE Uint32 getSeedH (void) {
  return m_z;
}


AWISH_PURE Uint32 getSeedL (void) {
  return m_w;
}


AWISH_PURE Uint32 getSeed (void) {
  return m_w;
}


/* from system time:  long x = dt.ToFileTime(); SetSeed((uint)(x >> 16), (uint)(x % 4294967296)); */
void setSeedH (Uint32 u) {
  m_z = u ? u : DEFAULT_M_Z;
}


void setSeedL (Uint32 u) {
  m_w = u ? u : DEFAULT_M_W;
}


void setSeedHL (Uint32 h, Uint32 l) {
  setSeedH(h);
  setSeedL(l);
}


void setSeed (Uint32 u) {
  setSeedHL(0, u);
}


Uint32 randUInt32 (void) {
  Uint32 res;
  //
  m_z = 36969*(m_z&65535)+(m_z>>16);
  m_w = 18000*(m_w&65535)+(m_w>>16);
  res = (m_z<<16)+m_w;
  if (m_w == 0) m_w = DEFAULT_M_Z;
  if (m_z == 0) m_z = DEFAULT_M_Z;
  return res;
}


// the magic number below is 1/(2^32 + 2)
// uniform: (randUInt32()+1.0)*2.328306435454494e-10;
// the result is strictly between 0 and 1.


////////////////////////////////////////////////////////////////////////////////
// return vm code size
int loadCodeFile (ResFile *resfile, int pc, int idx, int asmodule) {
  int rsz = 0, res = 0;
  uint8_t *buf = loadResFile(resfile, idx, &rsz);
  //
  if (buf == NULL) return -1;
  //
  res = vmLoadCodeFileFromDump(buf, rsz, pc, vmMaxGVar, vmMaxTVar, asmodule?NULL:&vmMaxGVar, asmodule?NULL:&vmMaxTVar);
  free(buf);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
#include "vm_gamelabelsdef.c"
void initLabels (void) {
#include "vm_gamelabelsinit.c"
}


////////////////////////////////////////////////////////////////////////////////
#ifndef AWISH_NO_SOUND
typedef struct SoundInfo {
  struct SoundInfo *next;
  int idx;
  Mix_Chunk *data;
} SoundInfo;

static SoundInfo *sndlist = NULL;


static AWISH_PURE SoundInfo *findSound (int idx) {
  for (SoundInfo *snd = sndlist; snd != NULL; snd = snd->next) if (snd->idx == idx) return snd;
  return NULL;
}


int loadSound (int idx) {
  SoundInfo *snd = findSound(idx);
  //
  if (snd == NULL) {
    Mix_Chunk *data = loadSoundFile(&sndfile, idx);
    //
    if (data != NULL) {
      snd = malloc(sizeof(SoundInfo));
      if (snd != NULL) {
        snd->next = sndlist;
        snd->idx = idx;
        snd->data = data;
        sndlist = snd;
      } else {
        Mix_FreeChunk(data);
      }
    }
  }
  return (snd != NULL) ? 0 : -1;
}


int unloadSound (int idx) {
  SoundInfo *snd, *p;
  //
  for (p = NULL, snd = sndlist; snd != NULL; p = snd, snd = snd->next) {
    if (snd->idx == idx) {
      Mix_FreeChunk(snd->data);
      if (p != NULL) p->next = snd->next; else sndlist = snd->next;
      free(snd);
      return 0;
    }
  }
  //
  return -1;
}


AWISH_PURE int isSoundLoaded (int idx) {
  return findSound(idx) ? 1 : 0;
}


int playSound (int idx, int chan) {
  if (!SOUND_DISABLED) {
    SoundInfo *snd = findSound(idx);
    //
    if (snd == 0) {
      loadSound(idx);
      snd = findSound(idx);
      if (snd == NULL) return -1;
    }
    //
    return Mix_PlayChannel(chan, snd->data, 0);
  }
  return -1;
}


int stopChannel (int chan) {
  if (!SOUND_DISABLED) Mix_HaltChannel(chan);
  return 0;
}


int isChannelPlaying (int chan) {
  return Mix_Playing(chan);
}


void unloadAllSounds (void) {
  Mix_HaltChannel(-1); // stop all sounds
  while (sndlist != NULL) {
    SoundInfo *c = sndlist;
    //
    sndlist = c->next;
    Mix_FreeChunk(c->data);
    free(c);
  }
}

#else

int loadSound (int idx) {
  return -1;
}


int unloadSound (int idx) {
  return -1;
}


int isSoundLoaded (int idx) {
  return 0;
}


int playSound (int idx, int chan) {
  return -1;
}


int stopChannel (int chan) {
  return 0;
}


int isChannelPlaying (int chan) {
  return 0;
}


void unloadAllSounds (void) {
}
#endif


////////////////////////////////////////////////////////////////////////////////
// return # of sprites loaded or <0 on error
int loadSpriteBankFromFile (SpriteBank *bank, const char *fname) {
  Uint8 *buf;
  int sz, pos;
  //
  if ((buf = loadDiskFileEx(fname, &sz)) == NULL) return -1;
  if (sz < 3) { free(buf); return -1; }
  pos = 0;
  for (int f = bank->count-1; f >= 0; --f) {
    SDL_FreeSurface(bank->spr[f][1]);
    SDL_FreeSurface(bank->spr[f][0]);
  }
  bank->count = 0;
  while (pos+4 <= sz) {
    int h = ((Uint32)buf[pos+0])+256*((Uint32)buf[pos+1]);
    int w = ((Uint32)buf[pos+2])+256*((Uint32)buf[pos+3]);
    //
    pos += 4;
    if (h > 0 && w > 0) {
      SDL_Surface *s0 = createSurface(w, h);
      SDL_Surface *s1 = createSurface(w, h);
      //
      if (!s0 || !s1) { free(buf); return -1; }
      //
      for (int y = 0; y < h; ++y) {
        for (int x = 0; x < w; ++x) {
          if (pos < sz) {
            putPixel2x(s0, x, y, buf[pos]);
            putPixel2x(s1, w-x-1, y, buf[pos]);
            ++pos;
          }
        }
      }
      bank->spr[bank->count][0] = s0;
      bank->spr[bank->count][1] = s1;
      ++(bank->count);
    }
  }
  free(buf);
  return bank->count;
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  int x, y;
  int bank;
  int num;
  int dir;
  int inlevel;
} SpriteInfo;


typedef struct {
  int count;
  int size; // # of slots
  SpriteInfo *list;
} SpriteLayer;


#define MAX_LAYER  (63)
static SpriteLayer spLayers[MAX_LAYER+1];
static int spLayersInitialized = 0;


void clearSpriteLayers (void) {
  if (!spLayersInitialized) {
    for (int f = 0; f <= MAX_LAYER; ++f) {
      spLayers[f].size = 0;
      spLayers[f].list = NULL;
    }
    spLayersInitialized = 1;
  }
  for (int f = 0; f <= MAX_LAYER; ++f) spLayers[f].count = 0;
}


void addSpriteToLayer (int x, int y, int layer, int bank, int num, int dir, int inlevel) {
  //fprintf(stderr, "*x=%d, y=%d, layer=%d, bank=%d, num=%d, dir=%d, inlevel=%d\n", x, y, layer, bank, num, dir, inlevel);
  if (layer >= 0 && layer <= MAX_LAYER &&
      bank >= 0 && bank <= 256 &&
      num >= 0 && num < 256) {
    //
    if (spLayers[layer].count+1 >= spLayers[layer].size) {
      int newsz = spLayers[layer].size+64;
      SpriteInfo *n = realloc(spLayers[layer].list, newsz);
      //
      if (n == NULL) fatal("out of memory");
      spLayers[layer].list = n;
      spLayers[layer].size = newsz;
    }
    //
    SpriteInfo *si = spLayers[layer].list+(spLayers[layer].count++);
    //
    si->x = x;
    si->y = y;
    si->bank = bank;
    si->num = num;
    si->dir = dir;
    si->inlevel = inlevel;
  }
}


void levelDrawSpriteLayers (SDL_Surface *frame, int visx, int visy, int visw, int vish) {
  SDL_Rect rc;
  //
  rc.x = visx;
  rc.y = visy;
  rc.w = visw;
  rc.h = vish;
  SDL_SetClipRect(frame, &rc);
  for (int f = MAX_LAYER; f >= 0; --f) {
    SpriteInfo *list = spLayers[f].list;
    //
    if (list == NULL && spLayers[f].count != 0) {
      fatal("WTF? f=%d; count=%d", f, spLayers[f].count);
    }
    for (int c = spLayers[f].count; c > 0; --c, ++list) {
      if (list->inlevel &&
          list->dir >= 0 && list->dir <= 1 &&
          list->bank >= 0 && list->bank <= 256 &&
          list->num >= 0 && list->num < banks[list->bank].count &&
          banks[list->bank].spr[list->num][list->dir]) {
        blitSurface(frame, visx+list->x, visy+list->y, banks[list->bank].spr[list->num][list->dir]);
      }
    }
  }
  //
  SDL_SetClipRect(frame, NULL);
  for (int f = MAX_LAYER; f >= 0; --f) {
    SpriteInfo *list = spLayers[f].list;
    //
    if (list == NULL) continue;
    for (int c = spLayers[f].count; c > 0; --c, ++list) {
      //fprintf(stderr, ":x=%d, y=%d, layer=%d, bank=%d, num=%d, dir=%d, inlevel=%d\n", list->x, list->y, f, list->bank, list->num, list->dir, list->inlevel);
      if (!list->inlevel &&
          list->dir >= 0 && list->dir <= 1 &&
          list->bank >= 0 && list->bank <= 256 &&
          list->num >= 0 && list->num < banks[list->bank].count &&
          banks[list->bank].spr[list->num][list->dir]) {
        blitSurface(frame, list->x, list->y, banks[list->bank].spr[list->num][list->dir]);
      }
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
typedef struct Poly {
  struct Poly *next;
  int ofsx;
  int ofsy;
  int scale;
  int angle;
  int color;
  int alpha;
  int size;
  int used;
  int *points;
} Poly;


static Poly *plist = NULL, *ptail = NULL;


void pmClear (void) {
  while (plist != NULL) {
    Poly *c = plist;
    //
    plist = c->next;
    if (c->points != NULL) free(c->points);
    free(c);
  }
  ptail = plist = NULL;
}


void pmStart (int ofsx, int ofsy, int scale, int angle) {
  Poly *p = calloc(1, sizeof(Poly));
  //
  if (p == NULL) fatal("out of memory");
  p->ofsx = ofsx;
  p->ofsy = ofsy;
  p->scale = scale;
  p->angle = angle;
  p->points = NULL;
  p->used = p->size = 0;
  p->alpha = 0;
  p->next = NULL;
  if (ptail != NULL) ptail->next = p; else plist = p;
  ptail = p;
}


void pmAddPoint (int x, int y) {
  if (ptail != NULL) {
    if (ptail->used+2 > ptail->size) {
      int newsz = ptail->size+128;
      int *np = realloc(ptail->points, newsz*sizeof(int));
      //
      if (np == NULL) fatal("out of memory");
      ptail->points = np;
      ptail->size = newsz;
    }
    ptail->points[ptail->used++] = x;
    ptail->points[ptail->used++] = y;
  }
}


void pmDone (int color, int alpha) {
  if (ptail != NULL && ptail->used > 0) {
    if (color < 0) color = 0; else if (color > 255) color = 255;
    if (alpha < 0) alpha = 0; else if (alpha > 255) alpha = 255;
    ptail->color = color;
    ptail->alpha = alpha;
  }
}


void pmDraw (SDL_Surface *frame) {
  for (Poly *p = plist; p != NULL; p = p->next) {
    if (p->used > 0 && p->alpha > 0) {
      polymodStart(p->ofsx, p->ofsy, p->scale, p->angle);
      for (int f = 0; f < p->used; f += 2) polymodAddPoint(p->points[f], p->points[f+1]);
      polymodEnd();
      polymodFill(frame, p->color, p->alpha);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
typedef struct Text {
  struct Text *next;
  char *str;
  int len;
  int x;
  int y;
  int scale;
  int angle;
  int color;
  int alpha;
} Text;


static Text *tlist = NULL, *ttail = NULL;


void textClear (void) {
  while (tlist != NULL) {
    Text *c = tlist;
    //
    tlist = c->next;
    if (c->str) free(c->str);
    free(c);
  }
  ttail = NULL;
}


void textAdd (const char *str, int len, int x, int y, int scale, int angle, int color, int alpha) {
  Text *p;
  //
  if (str == NULL) return;
  if (len < 0) len = strlen(str);
  if ((p = calloc(1, sizeof(Text))) == NULL) fatal("out of memory");
  if ((p->str = calloc(len+1, 1)) == NULL) { free(p); fatal("out of memory"); }
  if (color < 0) color = 0; else if (color > 255) color = 255;
  if (alpha < 0) alpha = 0; else if (alpha > 255) alpha = 255;
  p->x = x;
  p->y = y;
  p->scale = scale;
  p->angle = angle;
  p->color = color;
  p->alpha = alpha;
  if (len > 0) memcpy(p->str, str, len);
  p->next = NULL;
  if (ttail != NULL) ttail->next = p; else tlist = p;
  ttail = p;
}


void textDraw (SDL_Surface *frame) {
  for (Text *t = tlist; t != NULL; t = t->next) {
    polymodStr(frame, t->str, t->x, t->y, t->scale, t->angle, t->color, t->alpha);
  }
}

/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RESFILE_H_
#define _RESFILE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "SDL.h"
#ifndef AWISH_NO_SOUND
# include "SDL_mixer.h"
#endif

#include "awishcommon.h"


typedef struct {
  FILE *fl;
  int count;
  Uint32 *offsets;
  Uint32 *sizes;
} ResFile;


extern const char *getHomeDir (void);
extern const char *getHomeDirFile (const char *fname);

extern void createHomeDir (void);

extern int initResFile (ResFile *resfile, const char *rfname);
extern void deinitResFile (ResFile *resfile);
extern uint8_t *loadResFile (ResFile *resfile, int idx, int *rsz);

extern uint8_t *tryDiskFile (const char *fname, int *rsz);
extern uint8_t *loadDiskFileEx (const char *diskname, int *rsz);

#ifndef AWISH_NO_SOUND
extern Mix_Chunk *loadDiskSoundFile (const char *fname);
extern Mix_Chunk *loadSoundFile (ResFile *resfile, int idx);
#endif


#endif

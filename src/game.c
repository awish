/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "game.h"

#include "resfile.h"
#include "video.h"
#include "mainloop.h"
#include "vm.h"
#include "gameglobals.h"
#include "title.h"
#include "polymod.h"


#define DEMO_COMPRESSION  1


extern int goobers;



////////////////////////////////////////////////////////////////////////////////
VMRSTCB gameRSTCB = NULL;


////////////////////////////////////////////////////////////////////////////////
#define VIS_WIDTH   (15)
#define VIS_HEIGHT  (17)
#define VIS_X       (10)
#define VIS_Y       (20)


////////////////////////////////////////////////////////////////////////////////
static int redrawLevel;
static int oldScrX, oldScrY;
static int mapWidth;
static int mapHeight;
static int curLevel;
static char levelname[257];
static char levelcode[257];
static char itemname[257];
static int lnamex;
static Uint8 *levelData = NULL;
static int levelDataSize = 0;
static Uint8 *bmap = NULL, *fmap = NULL;
static Uint8 *xflags = NULL;
static int inMinimap;

static int doSave = 0;

static int demorecmode = 0; // 0: none; 1: recording; -1: playing
static int demoSize = 0;
static int demoPos = 0;
static Uint8 *demoData = NULL;
static Uint8 demoPrevKeyState = 0;
static int demoKeyStateRepeats = 0;

static char message[256];
//static Uint32 msgEndTime = 0;

static Uint32 demo_m_w = 0;
static Uint32 demo_m_z = 0;


////////////////////////////////////////////////////////////////////////////////
static int gamekeys[12];
static int fkeys[10];


static inline int isGameStopped (void) {
  return vmGVars[GVAR_GAME_STATE] != CONST_GAME_STATE_PLAYING;
}


static void setGameKeysGVars (void) {
  if (!vmGVars[GVAR_IN_MENU]) {
    if (vmGVars[GVAR_GAME_WANT_KEY] || isGameStopped()) {
      // update 'action' gvars only when prof is idle
      vmGVars[GVAR_KEY_LEFT]  = gamekeys[0] ? 1 : 0;
      vmGVars[GVAR_KEY_RIGHT] = gamekeys[1] ? 1 : 0;
      vmGVars[GVAR_KEY_UP]    = gamekeys[2] ? 1 : 0;
      vmGVars[GVAR_KEY_DOWN]  = gamekeys[3] ? 1 : 0;
      vmGVars[GVAR_KEY_TAKE]  = gamekeys[4] ? 1 : 0;
      vmGVars[GVAR_KEY_USE]   = gamekeys[5] ? 1 : 0;
    }
    //
    //vmGVars[GVAR_KEY_MINIMAP] = gamekeys[6] ? 1 : 0;
    //vmGVars[GVAR_KEY_RESTART] = gamekeys[7] ? 1 : 0;
    //
    vmGVars[GVAR_KEY_FALL_CHEAT] = gamekeys[8] ? 1 : 0;
    vmGVars[GVAR_KEY_WALK_CHEAT] = gamekeys[9] ? 1 : 0;
    vmGVars[GVAR_KEY_PLEV_CHEAT] = gamekeys[10] ? 1 : 0;
    vmGVars[GVAR_KEY_NLEV_CHEAT] = gamekeys[11] ? 1 : 0;
  }
}


////////////////////////////////////////////////////////////////////////////////
#define SECRET  (42)
//#define SECRET  (0)


static int readBuf (FILE *fl, void *buf, int len) {
  unsigned char *c = (unsigned char *)buf;
  //
  while (len-- > 0) {
    unsigned char b;
    //
    if (fread(&b, 1, 1, fl) != 1) return -1;
    b ^= SECRET;
    *c++ = b;
  }
  return 0;
}


static int writeBuf (FILE *fl, const void *buf, int len) {
  const unsigned char *c = (const unsigned char *)buf;
  //
  while (len-- > 0) {
    unsigned char b = *c++;
    //
    b ^= SECRET;
    if (fwrite(&b, 1, 1, fl) != 1) return -1;
  }
  return 0;
}


static int readDW (FILE *fl, int *v) {
  int t[4];
  //
  for (int f = 0; f < 4; ++f) {
    unsigned char b;
    //
    if (fread(&b, 1, 1, fl) != 1) return -1;
    t[f] = b^SECRET;
  }
  //
  if (v) {
    *v = 0;
    for (int f = 0; f < 4; ++f) *v |= t[f]<<(8*f);
  }
  return 0;
}


static int writeDW (FILE *fl, int v) {
  for (int f = 0; f < 4; ++f) {
    unsigned char b;
    //
    b = v&0xff;
    b ^= SECRET;
    if (fwrite(&b, 1, 1, fl) != 1) return -1;
    v >>= 8;
  }
  return 0;
}


static int readUDW (FILE *fl, Uint32 *v) {
  int t[4];
  //
  for (int f = 0; f < 4; ++f) {
    unsigned char b;
    //
    if (fread(&b, 1, 1, fl) != 1) return -1;
    t[f] = b^SECRET;
  }
  //
  if (v) {
    *v = 0;
    for (int f = 0; f < 4; ++f) *v |= t[f]<<(8*f);
  }
  return 0;
}


static int writeUDW (FILE *fl, Uint32 v) {
  for (int f = 0; f < 4; ++f) {
    unsigned char b;
    //
    b = v&0xff;
    b ^= SECRET;
    if (fwrite(&b, 1, 1, fl) != 1) return -1;
    v >>= 8;
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static const char *SAVE_GAME_SIGNATURE = "ASG1";


static int saveGame (void) {
  FILE *fl = fopen(getHomeDirFile("awish.sav"), "wb");
  //
  if (fl != NULL) {
    demo_m_w = getSeedL();
    demo_m_z = getSeedH();
    if (fwrite(SAVE_GAME_SIGNATURE, 4, 1, fl) != 1) goto error;
    if (vmSaveState(fl) != 0) goto error;
    if (writeDW(fl, mapWidth) != 0) goto error;
    if (writeDW(fl, mapHeight) != 0) goto error;
    if (writeUDW(fl, demo_m_w) != 0) goto error;
    if (writeUDW(fl, demo_m_z) != 0) goto error;
    if (writeDW(fl, inMinimap) != 0) goto error;
    if (writeDW(fl, curLevel) != 0) goto error;
    if (writeBuf(fl, levelname, sizeof(levelname)) != 0) goto error;
    if (writeDW(fl, lnamex) != 0) goto error;
    if (writeBuf(fl, levelcode, sizeof(levelcode)) != 0) goto error;
    if (writeBuf(fl, itemname, sizeof(itemname)) != 0) goto error;
    if (writeBuf(fl, bmap, mapWidth*mapHeight) != 0) goto error;
    if (writeBuf(fl, fmap, mapWidth*mapHeight) != 0) goto error;
    if (writeBuf(fl, xflags, mapWidth) != 0) goto error;
    fclose(fl);
    return 0;
  }
error:
  fclose(fl);
  unlink("awish.sav");
  if (goobers) fprintf(stderr, "error saving game\n");
  return -1;
}


static int loadGame (void) {
  FILE *fl = fopen(getHomeDirFile("awish.sav"), "rb");
  //
  if (fl != NULL) {
    char sign[4];
    //
    if (fread(sign, 4, 1, fl) != 1) goto error;
    if (memcmp(sign, SAVE_GAME_SIGNATURE, 4) != 0) goto error;
    if (vmLoadState(fl) != 0) goto error;
    if (readDW(fl, &mapWidth) != 0) goto error;
    if (readDW(fl, &mapHeight) != 0) goto error;
    if (readUDW(fl, &demo_m_w) != 0) goto error;
    if (readUDW(fl, &demo_m_z) != 0) goto error;
    if (readDW(fl, &inMinimap) != 0) goto error;
    if (readDW(fl, &curLevel) != 0) goto error;
    if (readBuf(fl, levelname, sizeof(levelname)) != 0) goto error;
    if (readDW(fl, &lnamex) != 0) goto error;
    if (readBuf(fl, levelcode, sizeof(levelcode)) != 0) goto error;
    if (readBuf(fl, itemname, sizeof(itemname)) != 0) goto error;
    if (bmap != NULL) free(bmap); bmap = NULL;
    if (fmap != NULL) free(fmap); fmap = NULL;
    if (xflags != NULL) free(xflags); xflags = NULL;
    fmap = calloc(mapWidth*mapHeight, 1); if (fmap == NULL) fatal("out of memory in loadGame");
    bmap = calloc(mapWidth*mapHeight, 1); if (bmap == NULL) fatal("out of memory in loadGame");
    xflags = calloc(mapWidth, 1); if (xflags == NULL) fatal("out of memory in loadGame");
    if (readBuf(fl, bmap, mapWidth*mapHeight) != 0) goto error;
    if (readBuf(fl, fmap, mapWidth*mapHeight) != 0) goto error;
    if (readBuf(fl, xflags, mapWidth) != 0) goto error;
    fclose(fl);
    setSeedL(demo_m_w);
    setSeedH(demo_m_z);
    redrawLevel = 1;
    oldScrX = oldScrY = -1;
    return 0;
  }
error:
  fclose(fl);
  if (goobers) fprintf(stderr, "error loading saved game\n");
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
static void demoClear (void) {
  //demorecmode = 0;
  demoSize = 0;
  demoPos = 0;
  demoPrevKeyState = 0;
  demoKeyStateRepeats = 0;
  if (demoData != NULL) free(demoData);
  demoData = NULL;
}


static void demoAddByte (Uint8 b) {
  if (demoPos+1 > demoSize && demoSize > 1024*1024*16) fatal("out of memory in demo recording!");
  if (demoPos+1 > demoSize) {
    int newsz = demoSize+1024*32;
    Uint8 *nn = realloc(demoData, newsz);
    //
    if (!nn) fatal("out of memory in demo recording!");
    demoData = nn;
    demoSize = newsz;
  }
  demoData[demoPos++] = b;
}


static Uint8 demoGetKeyState (void) {
  Uint8 res = 0;
  //
  for (int f = 0; f <= 5; ++f) res |= (gamekeys[f] ? (1<<f) : 0);
  return res;
}


static void demoSetKeyState (Uint8 kstate) {
  for (int f = 0; f <= 5; ++f) gamekeys[f] = ((kstate&(1<<f)) ? 1 : 0);
}


static void demoAddFrameData (void) {
  Uint8 kstate = demoGetKeyState();
  //
#if DEMO_COMPRESSION
  if (demoKeyStateRepeats == 256) {
    demoAddByte(demoKeyStateRepeats-1);
    demoAddByte(demoPrevKeyState);
    demoKeyStateRepeats = 0;
  }
  //
  if (kstate != demoPrevKeyState) {
    if (demoKeyStateRepeats > 0) {
      demoAddByte(demoKeyStateRepeats-1);
      demoAddByte(demoPrevKeyState);
    }
    demoPrevKeyState = kstate;
    demoKeyStateRepeats = 1;
  } else {
    ++demoKeyStateRepeats;
  }
#else
  demoAddByte(kstate);
#endif
}


static void demoGetFrameData (void) {
#if DEMO_COMPRESSION
  while (demoKeyStateRepeats < 1) {
    if (demoPos+2 > demoSize) {
      demoClear();
      demorecmode = 0;
      if (goobers) fprintf(stderr, "demo complete\n");
      strcpy(message, "demo stopped");
      return;
    }
    demoKeyStateRepeats = demoData[demoPos++];
    ++demoKeyStateRepeats;
    demoPrevKeyState = demoData[demoPos++];
  }
  --demoKeyStateRepeats;
  //
  demoSetKeyState(demoPrevKeyState);
#else
  if (demoPos+1 > demoSize) {
    demoClear();
    demorecmode = 0;
    if (goobers) fprintf(stderr, "demo complete\n");
    strcpy(message, "demo stopped");
    return;
  }
  demoSetKeyState(demoData[demoPos++]);
#endif
}


static int demoSave (void) {
  FILE *fl;
  char buf[128];
  char sign[5];
  //
  if (demoKeyStateRepeats > 0) {
    demoAddByte(demoKeyStateRepeats-1);
    demoAddByte(demoPrevKeyState);
  }
  //
  sprintf(buf, "awish%02d.dmo", curLevel+1);
  fl = fopen(getHomeDirFile(buf), "wb");
  if (fl == NULL) {
    if (goobers) fprintf(stderr, "can't create demo file '%s'\n", buf);
    return -1;
  }
  strcpy(sign, "AWD1");
  if (fwrite(sign, 4, 1, fl) != 1) goto error;
  if (writeDW(fl, curLevel) != 0) goto error;
  if (writeUDW(fl, demo_m_w) != 0) goto error;
  if (writeUDW(fl, demo_m_z) != 0) goto error;
  if (writeDW(fl, demoPos) != 0) goto error;
  if (writeBuf(fl, demoData, demoPos) != 0) goto error;
  fclose(fl);
  if (goobers) fprintf(stderr, "demo saved to file '%s'\n", buf);
  return 0;
error:
  fclose(fl);
  unlink(buf);
  if (goobers) fprintf(stderr, "can't write demo file '%s'\n", buf);
  return -1;
}


static int demoLoad (void) {
  FILE *fl;
  char buf[128];
  int size, level;
  char sign[4];
  //
  demoClear();
  sprintf(buf, "awish%02d.dmo", curLevel+1);
  fl = fopen(getHomeDirFile(buf), "rb");
  if (fl == NULL) {
    if (goobers) fprintf(stderr, "can't open demo file '%s'\n", buf);
    return -1;
  }
  //
  if (fread(sign, 4, 1, fl) != 1) goto error;
  if (memcmp(sign, "AWD1", 4) != 0) goto error;
  if (readDW(fl, &level) != 0) goto error;
  if (readUDW(fl, &demo_m_w) != 0) goto error;
  if (readUDW(fl, &demo_m_z) != 0) goto error;
  if (readDW(fl, &size) != 0) goto error;
  if (size < 1 || size > 1024*1024*16) goto error;
  demoData = malloc(size);
  if (demoData == NULL) goto error;
  if (readBuf(fl, demoData, size) != 0) goto error;
  demoSize = size;
  demoPos = 0;
  fclose(fl);
  setSeedL(demo_m_w);
  setSeedH(demo_m_z);
  if (goobers) fprintf(stderr, "loaded demo file '%s'\n", buf);
  //
  return 0;
error:
  fclose(fl);
  if (goobers) fprintf(stderr, "can't load demo file '%s'\n", buf);
  demoClear();
  return -1;
}


static void demoCB (void) {
  //if (vmGVars[GVAR_GAME_STATE] == CONST_GAME_STATE_STARTED) unloadMapScript();
  static int fcnt = 0;
  //
  if (vmGVars[GVAR_SCR_X] != oldScrX || vmGVars[GVAR_SCR_Y] != oldScrY) {
    oldScrX = vmGVars[GVAR_SCR_X];
    oldScrY = vmGVars[GVAR_SCR_Y];
    redrawLevel = 1;
  }
  //
  ++fcnt;
  switch (demorecmode) {
    case -666: // prepare to load demo
      if (demoLoad() != 0) {
        demorecmode = 0;
        break;
      }
      demorecmode = -2;
      // fallthru
    case 666: // prepare to save demo
      vmSetPC(0, CODE_ENTRY_GAME_RESTART_LEVEL);
      vmGVars[GVAR_CUR_LEVEL] = curLevel;
      vmGVars[GVAR_GAME_STATE] = -1; // invalid state
      if (demorecmode > 0) {
        demoClear();
        demorecmode = 2;
      }
      break;
    case -1: // demo replaying
    case 1: // demo saving
      if (isGameStopped()) {
        // the level is over or prof is dead
        if (demorecmode > 0) demoSave();
        demoClear();
        demorecmode = 0;
        memset(gamekeys, 0, sizeof(gamekeys));
      } else {
        if (vmGVars[GVAR_GAME_WANT_KEY]) {
          ((demorecmode < 0) ? demoGetFrameData : demoAddFrameData)();
          //fprintf(stderr, "fcnt: %d; data: %u\n", fcnt, demoGetKeyState());
        }
      }
      break;
    case -2: // waiting for 'game started' trigger
    case 2: // waiting for 'game started' trigger
      if (vmGVars[GVAR_GAME_STATE] == CONST_GAME_STATE_STARTED) {
        if (goobers) fprintf(stderr, "demo %s started...\n", demorecmode<0?"replaying":"recording");
        if (demorecmode < 0) {
          // replay
          setSeedL(demo_m_w);
          setSeedH(demo_m_z);
        } else {
          // record
          demo_m_w = getSeedL();
          demo_m_z = getSeedH();
        }
        demorecmode = (demorecmode < 0) ? -1 : 1;
        fcnt = 0;
      } else if (vmGVars[GVAR_GAME_STATE] >= 0) {
        demoClear();
        demorecmode = 0;
      }
      break;
  }
  setGameKeysGVars();
  vmGVars[GVAR_GAME_WANT_KEY] = 0;
}


////////////////////////////////////////////////////////////////////////////////
static void activateMinimap (void) {
  inMinimap = 1;
  vmPaused = 1;
  doSave = 0;
  redrawLevel = 1;
  //gamekeys[6] = 1;
}


static void deactivateMinimap (void) {
  inMinimap = 0;
  vmPaused = 0;
  doSave = 0;
  redrawLevel = 1;
  //gamekeys[6] = 0;
}


////////////////////////////////////////////////////////////////////////////////
static void frmGameKey (SDL_KeyboardEvent *key) {
  if (key->type == SDL_KEYDOWN) {
    switch (key->keysym.sym) {
      case SDLK_ESCAPE:
      case SDLK_e:
        if (vmPaused && inMinimap) deactivateMinimap();
        else if (demorecmode == 0) vmGVars[GVAR_KEY_ESCAPE] = 1;
        break;
      case SDLK_SPACE:
      case SDLK_RETURN:
      case SDLK_KP_ENTER:
        //fprintf(stderr, "VMP: %d; im: %d\n", vmPaused, inMinimap);
        if (vmPaused && inMinimap) deactivateMinimap();
        break;
      case SDLK_F12:
      case SDLK_q:
        vmGVars[GVAR_KEY_QUIT] = 1;
        break;
      case SDLK_p:
        if (!vmPaused) {
          activateMinimap();
        } else {
          if (inMinimap) deactivateMinimap();
        }
        break;
      case SDLK_r:
        if (!vmPaused) {
          if (demorecmode != 0) { demoClear(); demorecmode = 0; }
          if ((key->keysym.mod&(KMOD_CTRL)) != 0) {
            //gamekeys[7] = 1;
            vmGVars[GVAR_KEY_RESTART] = 1;
          }
        }
        break;
      case SDLK_s:
        if (!vmPaused && !demorecmode) {
          if (/*goobers &&*/ (key->keysym.mod&(KMOD_CTRL)) != 0) { doSave = 1; vmPaused = 1; }
        }
        break;
      case SDLK_l:
        if (!vmPaused && !demorecmode) {
          if (/*goobers &&*/ (key->keysym.mod&(KMOD_CTRL)) != 0) { doSave = -1; vmPaused = 1; }
        }
        break;
      case SDLK_d:
        if (!vmPaused) {
          if ((key->keysym.mod&(KMOD_CTRL)) != 0) {
            if (!demorecmode) {
              demorecmode = 666; // 'start saving'
            } else {
              demoSave();
              demoClear();
              demorecmode = 0;
            }
          }
        }
        break;
      case SDLK_m:
        if (!vmPaused) {
          if ((key->keysym.mod&(KMOD_CTRL)) != 0) {
            if (!demorecmode) {
              demorecmode = -666; // 'start playing'
            } else {
              demoClear();
              demorecmode = 0;
              demoSetKeyState(0);
            }
          }
        }
        break;
      default: ;
    }
  }
  //
  if (demorecmode == 0 || demorecmode == 1) {
    if (!vmGVars[GVAR_IN_MENU]) {
      switch (key->keysym.sym) {
        case SDLK_LEFT: case SDLK_KP4: gamekeys[0] = (key->type == SDL_KEYDOWN); break;
        case SDLK_RIGHT: case SDLK_KP6: gamekeys[1] = (key->type == SDL_KEYDOWN); break;
        case SDLK_UP: case SDLK_KP8: gamekeys[2] = (key->type == SDL_KEYDOWN); break;
        case SDLK_DOWN: case SDLK_KP2: gamekeys[3] = (key->type == SDL_KEYDOWN); break;
        case SDLK_SPACE: case SDLK_KP0: gamekeys[4] = (key->type == SDL_KEYDOWN); break;
        case SDLK_RETURN: case SDLK_KP_PERIOD: case SDLK_u: case SDLK_KP_ENTER: gamekeys[5] = (key->type == SDL_KEYDOWN); break;
        default: ;
      }
    } else {
      //gamekeys[0] = gamekeys[1] = gamekeys[2] = gamekeys[3] = gamekeys[4] = gamekeys[5] = 0;
      memset(gamekeys, 0, sizeof(gamekeys));
      if (key->type == SDL_KEYDOWN) {
        switch (key->keysym.sym) {
          case SDLK_ESCAPE: case SDLK_e: vmGVars[GVAR_KEY_ESCAPE] = 1; break;
          case SDLK_F12: case SDLK_q: vmGVars[GVAR_KEY_QUIT] = 1; break;
          case SDLK_SPACE: case SDLK_RETURN: case SDLK_KP_ENTER: vmGVars[GVAR_KEY_START] = 1; break;
          case SDLK_LEFT: case SDLK_KP4: vmGVars[GVAR_KEY_LEFT] = 1; break;
          case SDLK_RIGHT: case SDLK_KP6: vmGVars[GVAR_KEY_RIGHT] = 1; break;
          case SDLK_UP: case SDLK_KP8: vmGVars[GVAR_KEY_UP] = 1; break;
          case SDLK_DOWN: case SDLK_KP2: vmGVars[GVAR_KEY_DOWN] = 1; break;
          default: ;
        }
      }
    }
    //
    if (goobers && !demorecmode && !vmGVars[GVAR_IN_MENU]) {
      switch (key->keysym.sym) {
        case SDLK_f: gamekeys[8] = (key->type == SDL_KEYDOWN); break;
        case SDLK_w: gamekeys[9] = (key->type == SDL_KEYDOWN); break;
        case SDLK_KP_MINUS: gamekeys[10] = (key->type == SDL_KEYDOWN); break;
        case SDLK_KP_PLUS: gamekeys[11] = (key->type == SDL_KEYDOWN); break;
        //
        case SDLK_F1: fkeys[1] = (key->type == SDL_KEYDOWN); break;
        case SDLK_F2: fkeys[2] = (key->type == SDL_KEYDOWN); break;
        case SDLK_F3: fkeys[3] = (key->type == SDL_KEYDOWN); break;
        case SDLK_F4: fkeys[4] = (key->type == SDL_KEYDOWN); break;
        case SDLK_F5: fkeys[5] = (key->type == SDL_KEYDOWN); break;
        case SDLK_F6: fkeys[6] = (key->type == SDL_KEYDOWN); break;
        case SDLK_F7: fkeys[7] = (key->type == SDL_KEYDOWN); break;
        case SDLK_F8: fkeys[8] = (key->type == SDL_KEYDOWN); break;
        case SDLK_F9: fkeys[9] = (key->type == SDL_KEYDOWN); break;
        default: ;
      }
    }
  }
}


static void frmMouse (int x, int y, int buttons) {
  vmGVars[GVAR_MOUSE_X] = x;
  vmGVars[GVAR_MOUSE_Y] = y;
  vmGVars[GVAR_MOUSE_BUTTONS] = buttons;
}


////////////////////////////////////////////////////////////////////////////////
static inline Uint8 fgTile (int x, int y) {
  return (x >= 0 && y >= 0 && x < mapWidth && y < mapHeight) ? fmap[y*mapWidth+x] : 0;
}


static inline Uint8 bgTile (int x, int y) {
  if (y == -666) return (x >= 0 && x < mapWidth) ? xflags[x] : 0;
  return (x >= 0 && y >= 0 && x < mapWidth && y < mapHeight) ? bmap[y*mapWidth+x] : 0;
}


static inline void setFGTile (int x, int y, Uint8 tile) {
  if (x >= 0 && y >= 0 && x < mapWidth && y < mapHeight) {
    if (fmap[y*mapWidth+x] != tile) {
      redrawLevel = 1;
      fmap[y*mapWidth+x] = tile;
    }
  }
}


static inline void setBGTile (int x, int y, Uint8 tile) {
  if (y == -666 && x >= 0 && x < mapWidth) xflags[x] = tile;
  if (x >= 0 && y >= 0 && x < mapWidth && y < mapHeight) {
    if (bmap[y*mapWidth+x] != tile) {
      redrawLevel = 1;
      bmap[y*mapWidth+x] = tile;
    }
  }
}


static int mapGet (int tid, int fg, int x, int y) {
  if (fmap != NULL) return fg ? fgTile(x, y) : bgTile(x, y);
  return 0;
}


static void mapSet (int tid, int fg, int x, int y, int tile) {
  if (fmap != NULL) {
    if (fg) setFGTile(x, y, tile); else setBGTile(x, y, tile);
  }
}


////////////////////////////////////////////////////////////////////////////////
// cliprect should be set
static void levelDrawMap (SDL_Surface *frame) {
  SDL_Rect rc;
  //
  rc.x = VIS_X;
  rc.y = VIS_Y;
  rc.w = VIS_WIDTH*20;
  rc.h = VIS_HEIGHT*10;
  //
  if (!inMinimap) {
    int levelback = vmGVars[GVAR_LEVEL_BACKPIC];
    int scrX = vmGVars[GVAR_SCR_X], scrY = vmGVars[GVAR_SCR_Y];
    //
    if (levelback < 1 || levelback > 7) levelback = 1;
    blitSurface(frame, 0, 0, backs[levelback]);
    //
    SDL_SetClipRect(frame, &rc);
    for (int dy = 0; dy < VIS_HEIGHT; ++dy) {
      for (int dx = 0; dx < VIS_WIDTH; ++dx) {
        int x = scrX+dx, y = scrY+dy;
        Uint8 b = bgTile(x, y), f = fgTile(x, y);
        //
        if (b) blitSurface(frame, VIS_X+dx*20, VIS_Y+dy*10, banks[CONST_BANK_BG_TILES].spr[b-1][0]);
        if (f) blitSurface(frame, VIS_X+dx*20, VIS_Y+dy*10, banks[CONST_BANK_FG_TILES].spr[f-1][0]);
      }
    }
  } else {
    SDL_SetClipRect(frame, NULL);
    blitSurface(frame, 0, 0, backs[8]);
    for (int dy = 0; dy < mapHeight; ++dy) {
      for (int dx = 1; dx < mapWidth; ++dx) {
        Uint8 b = bgTile(dx, dy), f = fgTile(dx, dy), t = 8;
        //
        if (f == 0) {
          if (b >= 1 && b <= 8) t = b-1; else t = 8;
        } else {
          t = f+8;
        }
        if (banks[CONST_BANK_MAP_TILES].spr[t][0]) {
          blitSurface(frame, VIS_X+dx*6+28, VIS_Y+dy*4-6, banks[CONST_BANK_MAP_TILES].spr[t][0]);
        } else {
          blitSurface(frame, VIS_X+dx*6+28, VIS_Y+dy*4-6, banks[CONST_BANK_MAP_TILES].spr[0][0]);
        }
      }
    }
  }
  SDL_SetClipRect(frame, NULL);
  redrawLevel = 0;
}


static void levelDrawSprites (SDL_Surface *frame) {
  if (!inMinimap) {
    int scrX = vmGVars[GVAR_SCR_X], scrY = vmGVars[GVAR_SCR_Y];
    int py = vmGetTVar(0, TVAR_POS_Y)-scrY;
    //
    for (int cc = 0; cc <= 2; ++cc) {
      for (int f = cc==1?1:0; f <= vmLastThread(); ++f) {
        if (vmIsThreadAlive(f) && !vmIsSuspendedThread(f)) {
          int b = vmGetTVar(f, TVAR_SPR_BANK);
          int s = vmGetTVar(f, TVAR_SPR_NUM);
          int d = vmGetTVar(f, TVAR_SPR_DIR);
          //
          if (d >= 0 && d <= 1 && s >= 0 && b >= 0 && b <= 255 && s < banks[b].count && banks[b].spr[s][d]) {
            SDL_Surface *sf = banks[b].spr[s][d];
            int x = vmGetTVar(f, TVAR_POS_X)-scrX;
            int y = vmGetTVar(f, TVAR_POS_Y)-scrY;
            int tx = vmGetTVar(f, TVAR_POS_TX);
            int ty = vmGetTVar(f, TVAR_POS_TY);
            int si = vmGetTVar(f, TVAR_SPR_ITEM);
            //
            if (cc == 0 && b == CONST_BANK_ITEMS) continue;
            if (cc == 1 && b != CONST_BANK_ITEMS) continue;
            if (cc == 2) {
              if (b != CONST_BANK_PROF) {
                if (b != CONST_BANK_ITEMS) continue;
                if (y < py) continue;
              }
            }
            if (b == CONST_BANK_IM_PROF || b == CONST_BANK_PROF) tx -= 5;
            blitSurface(frame, VIS_X+x*20+tx, VIS_Y+y*10+ty-sf->h, sf);
            if (si > 0 && si < banks[CONST_BANK_ITEMS].count) {
              sf = banks[CONST_BANK_ITEMS].spr[si][0];
              blitSurface(frame, VIS_X+x*20+tx, VIS_Y+y*10+ty-sf->h-24, sf);
            }
          }
        }
      }
    }
  } else {
    blitSurface(frame, VIS_X+(vmGetTVar(0, TVAR_POS_X)+0)*6+28, VIS_Y+(vmGetTVar(0, TVAR_POS_Y)-4)*4-6, banks[CONST_BANK_MAP_ITEMS].spr[0][0]);
    for (int f = 1; f < VM_MAX_THREADS; ++f) {
      if (vmIsThreadAlive(f) && !vmIsSuspendedThread(f)) {
        int i = vmGetTVar(f, TVAR_ITEM_ID);
        if (i > 0 && i <= 255) {
          SDL_Surface *sf = banks[CONST_BANK_MAP_ITEMS].spr[i][0];
          int x = vmGetTVar(f, TVAR_POS_X);
          int y = vmGetTVar(f, TVAR_POS_Y);
          //
          blitSurface(frame, VIS_X+(x+0)*6+28, VIS_Y+(y-2)*4-6, sf);
        }
      }
    }
  }
}


static void levelDrawName (SDL_Surface *frame) {
  drawString(frame, levelname, lnamex, 0, 0x76);
}


static void levelDrawNumber (SDL_Surface *frame) {
  char buf[8];
  //
  sprintf(buf, "%d", curLevel+1);
  drawString(frame, buf, 320-strlen(buf)*8, 0, 1);
}


static void levelDrawCode (SDL_Surface *frame) {
  char buf[8];
  //
  sprintf(buf, "%d", curLevel+1);
  drawString(frame, levelcode, 320-strlen(levelcode)*8-strlen(buf)*8, 0, 3);
}


static void levelDrawItemName (SDL_Surface *frame) {
  drawString(frame, itemname, 0, 0, 1);
}


static void levelDrawMisc (SDL_Surface *frame) {
  if (demorecmode < 0) {
    if (SDL_GetTicks()%1000 < 500) drawChar(frame, 'R', 0, 8, 3);
  } else if (demorecmode > 0) {
    if (SDL_GetTicks()%1000 < 500) drawChar(frame, 'D', 0, 8, 3);
  }
}


static void drawSpriteBank (SDL_Surface *frame, SpriteBank *bank) {
  SDL_Rect dst;
  int x = 0, y = 0, maxh = 0;
  //
  SDL_SetClipRect(frame, NULL);
  dst.x = 0;
  dst.y = 0;
  dst.w = frame->w;
  dst.h = frame->h;
  SDL_FillRect(frame, &dst, palette[3]);
  //
  for (int num = 0; num < bank->count; ++num) {
    SDL_Surface *s = bank->spr[num][0];
    //
    if (s != NULL) {
      char buf[16];
      int w = s->w;
      //
      if (gamekeys[4] || gamekeys[5]) {
        // take or use
        sprintf(buf, "%d", num);
        if (w < strlen(buf)*8+2) w = strlen(buf)*8+2;
      }
      if (x+w > 320) {
        x = 0;
        y += maxh;
        maxh = 0;
      }
      if (maxh < s->h+1) maxh = s->h+1;
      blitSurface(frame, x, y, s);
      if (gamekeys[4] || gamekeys[5]) drawString(frame, buf, x, y, 1);
      x += w+1;
    }
  }
}


static void drawPalette (SDL_Surface *frame) {
  SDL_Rect dst;
  //
  SDL_SetClipRect(frame, NULL);
  dst.x = 0;
  dst.y = 0;
  dst.w = frame->w;
  dst.h = frame->h;
  SDL_FillRect(frame, &dst, palette[0]);
  //
  for (int y = 0; y < 16; ++y) {
    for (int x = 0; x < 16; ++x) {
      dst.x = x*20;
      dst.y = y*20;
      dst.w = 18;
      dst.h = 18;
      SDL_FillRect(frame, &dst, palette[y*16+x]);
      if (gamekeys[4] || gamekeys[5]) {
        char buf[16];
        //
        sprintf(buf, "%d", y*16+x);
        drawString(frame, buf, x*20, y*20, 1);
      }
    }
  }
}


static void frmGameDraw (SDL_Surface *frame) {
  SDL_Rect rc;
  //
  if (vmPaused && doSave) {
    if (doSave < 0) {
      if (loadGame() != 0) fatal("can't load game, VM is inconsistent, dying.");
      redrawLevel = 1;
    } else {
      saveGame();
    }
    doSave = 0;
    vmPaused = inMinimap;
  }
  //
  SDL_SetClipRect(frame, NULL);
  //
  if (curLevel >= 0) {
    if (redrawLevel) {
      levelDrawMap(backbuf);
      levelDrawName(backbuf);
      levelDrawNumber(backbuf);
      levelDrawCode(backbuf);
      levelDrawItemName(backbuf);
    }
    blitSurface(frame, 0, 0, backbuf);
    levelDrawMisc(frame);
    //
    rc.x = VIS_X;
    rc.y = VIS_Y;
    rc.w = VIS_WIDTH*20;
    rc.h = VIS_HEIGHT*10;
    SDL_SetClipRect(frame, &rc);
    levelDrawSprites(frame);
  } else {
    rc.x = 0;
    rc.y = 0;
    rc.w = frame->w;
    rc.h = frame->h;
    SDL_FillRect(frame, &rc, palette[0]);
    rc.x = VIS_X;
    rc.y = VIS_Y;
    rc.w = VIS_WIDTH*20;
    rc.h = VIS_HEIGHT*10;
    SDL_SetClipRect(frame, &rc);
  }
  //
  levelDrawSpriteLayers(frame, VIS_X, VIS_Y, VIS_WIDTH*20, VIS_HEIGHT*10);
  SDL_SetClipRect(frame, NULL);
  clearSpriteLayers();
  //
  pmDraw(frame);
  pmClear();
  //
  textDraw(frame);
  textClear();
  //
#if 0
  polymodStr(frame, "TEST", 50, 50, POLYFIX_BASE, 0, 1, 128);
#endif
#if 0
  static int angle = 0;
  static int mult = 1;
  static int multdir = 1;
  //
  if (!fkeys[9]) {
    angle = (angle+1)%360;
    mult += multdir;
    if (mult < 2 || mult > 50) multdir = -multdir;
    //fprintf(stderr, "a=%d\n", angle);
  } else {
    polyDump = fkeys[8];
  }
  polymodStart(320/2, 200/2, (mult+20)*POLYFIX_BASE/50, angle);
  //
#if 0
  polymodAddPoint(-20, -20);
  polymodAddPoint(20, -20);
  polymodAddPoint(20, 20);
  polymodAddPoint(-20, 20);
#endif
#if 0
  polymodAddPoint(-10, -10);
  polymodAddPoint(20, -10);
  polymodAddPoint(-10, 20);
  polymodAddPoint(20, 20);
#endif
#if 1
  polymodAddPoint(36, 0);
  polymodAddPoint(46, 12);
  polymodAddPoint(31, 17);
  polymodAddPoint(33, 33);
  polymodAddPoint(18, 31);
  polymodAddPoint(12, 46);
  polymodAddPoint(0, 36);
  polymodAddPoint(-12, 46);
  polymodAddPoint(-17, 31);
  polymodAddPoint(-33, 33);
  polymodAddPoint(-31, 17);
  polymodAddPoint(-46, 12);
  polymodAddPoint(-36, 0);
  polymodAddPoint(-46, -12);
  polymodAddPoint(-31, -18);
  polymodAddPoint(-33, -33);
  polymodAddPoint(-18, -31);
  polymodAddPoint(-12, -46);
  polymodAddPoint(0, -36);
  polymodAddPoint(12, -46);
  polymodAddPoint(18, -31);
  polymodAddPoint(33, -33);
  polymodAddPoint(31, -18);
  polymodAddPoint(46, -12);
#endif
#if 0
  polymodAddPoint(42, 0);
  polymodAddPoint(40, 10);
  polymodAddPoint(36, 20);
  polymodAddPoint(29, 29);
  polymodAddPoint(21, 36);
  polymodAddPoint(10, 40);
  polymodAddPoint(0, 42);
  polymodAddPoint(-10, 40);
  polymodAddPoint(-20, 36);
  polymodAddPoint(-29, 29);
  polymodAddPoint(-36, 20);
  polymodAddPoint(-40, 10);
  polymodAddPoint(-42, 0);
  polymodAddPoint(-40, -10);
  polymodAddPoint(-36, -21);
  polymodAddPoint(-29, -29);
  polymodAddPoint(-21, -36);
  polymodAddPoint(-10, -40);
  polymodAddPoint(0, -42);
  polymodAddPoint(10, -40);
  polymodAddPoint(21, -36);
  polymodAddPoint(29, -29);
  polymodAddPoint(36, -21);
  polymodAddPoint(40, -10);
#endif
  //
  polymodEnd();
  polymodFill(frame, 3, 100);
#endif
  //
  if (goobers) {
    for (int f = 84; f <= 90; ++f) if (fkeys[f-83]) drawSpriteBank(frame, &banks[f]);
    if (fkeys[8]) drawPalette(frame);
  }
  if (vmGVars[GVAR_MOUSE_HIDDEN] == 0) {
    int cur = vmGVars[GVAR_MOUSE_CURSOR];
    //
    if (cur >= 0 && cur < banks[256].count) {
      blitSurface(frame, vmGVars[GVAR_MOUSE_X], vmGVars[GVAR_MOUSE_Y], banks[256].spr[cur][0]);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
#define ROOM_SCRIPTS_MARK  " room script labels starts here "


static void loadMapScript (ResFile *resfile) {
  VMLabelInfo *l;
  int csz;
  //
  l = vmLabelAddMark(ROOM_SCRIPTS_MARK);
  l->value = vmCodeSize;
  if ((csz = loadCodeFile(resfile, vmCodeSize, 94+curLevel, 1)) > 1) {
    vmCodeSize += csz;
  }
}


static void unloadMapScript (void) {
  VMLabelInfo *l, *ucl;
  //
  while ((l = vmFindMark(ROOM_SCRIPTS_MARK)) != NULL) {
    ucl = vmFindLabel("entry_local_room_script_onunload");
    if (ucl != NULL && ucl->type == LB_CODE) vmExecuteBSR(0, ucl->value, 0);
    vmCodeSize = l->value;
    vmFreeLabelsUntilMark(ROOM_SCRIPTS_MARK);
  }
  //
  ucl = vmFindLabel("entry_goobers_room_onunload");
  if (ucl != NULL && ucl->type == LB_CODE) vmExecuteBSR(0, ucl->value, 0);
  //
  ucl = vmFindLabel("entry_room_onunload");
  if (ucl != NULL && ucl->type == LB_CODE) vmExecuteBSR(0, ucl->value, 0);
}


////////////////////////////////////////////////////////////////////////////////
static int loadLevelInternal (ResFile *resfile, int lidx) {
  message[0] = 0;
  unloadMapScript();
  //
  vmGVars[GVAR_VIS_WIDTH] = VIS_WIDTH;
  vmGVars[GVAR_VIS_HEIGHT] = VIS_HEIGHT;
  levelname[0] = levelcode[0] = itemname[0] = 0;
  //
  if (levelData != NULL) free(levelData); levelData = NULL; levelDataSize = 0;
  if (bmap != NULL) free(bmap); bmap = NULL;
  if (fmap != NULL) free(fmap); fmap = NULL;
  if (xflags != NULL) free(xflags); xflags = NULL;
  //
  curLevel = -1;
  if (lidx < 0 || lidx > 73) return -1;
  if ((levelData = loadResFile(resfile, lidx+9, &levelDataSize)) == NULL) return -1;
  curLevel = lidx;
  loadMapScript(resfile);
  return levelDataSize;
}


////////////////////////////////////////////////////////////////////////////////
static void getVMString (char *dest, int destsize, int argc, int argv[]) {
  if (destsize > 1) {
    int pos = 0, len = destsize-1, lp = 0;
    //
    switch (argc) {
      case 1: fatal("out of args in FRST_SET_LEVEL_NAME");
      case 2: pos = argv[1]; break;
      case 3: pos = argv[1]; len = argv[2]; break;
    }
    memset(dest, 0, destsize);
    if (len < 0) len = destsize-1;
    if (len > destsize-1) len = destsize-1;
    if (pos >= 0) {
      while (len > 0 && pos < vmCodeSize && vmCode[pos]) {
        dest[lp++] = vmCode[pos++];
        --len;
      }
    } else {
      pos = 0-(pos+1);
      while (len > 0 && pos < levelDataSize && levelData[pos]) {
        dest[lp++] = levelData[pos++];
        --len;
      }
    }
  } else if (destsize == 1) {
    dest[0] = 0;
  }
}


static int gameRST (int tid, int opcode, int argc, int argv[], int *argp[]) {
  if (argv[0] == CONST_FRST_MINIMAP) {
    activateMinimap();
  } else if (argv[0] == CONST_FRST_OPEN_LEVEL_FILE) {
    // levelnum
    //if (vmLoadArgs(tid, 2, argv, argp, argc, argv, argp) != 0) fatal("out of args");
    if (argc < 2) fatal("out of args in FRST_LOAD_LEVEL");
    vmGVars[GVAR_RST_RESULT] = loadLevelInternal(&resfile, argv[1]);
    //if (goobers) fprintf(stderr, "FRST_OPEN_LEVEL_FILE(%d): %d\n", argv[1], vmGVars[GVAR_RST_RESULT]);
  } else if (argv[0] == CONST_FRST_GET_LEVEL_DATA_SIZE) {
    vmGVars[GVAR_RST_RESULT] = levelDataSize;
  } else if (argv[0] == CONST_FRST_GET_LEVEL_LOADER) {
    VMLabelInfo *ucl = vmFindLabel("entry_local_room_loader");
    //
    vmGVars[GVAR_RST_RESULT] = (ucl != NULL && ucl->type == LB_CODE) ? ucl->value : -1;
  } else if (argv[0] == CONST_FRST_GET_LEVEL_INITER) {
    VMLabelInfo *ucl = vmFindLabel("entry_local_room_onload");
    //
    vmGVars[GVAR_RST_RESULT] = (ucl != NULL && ucl->type == LB_CODE) ? ucl->value : -1;
  } else if (argv[0] == CONST_FRST_GET_GOOBERS_INITER) {
    VMLabelInfo *ucl = vmFindLabel("entry_goobers_game_init");
    //
    vmGVars[GVAR_RST_RESULT] = (ucl != NULL && ucl->type == LB_CODE) ? ucl->value : -1;
  } else if (argv[0] == CONST_FRST_GET_GOOBERS_ONLOAD) {
    VMLabelInfo *ucl = vmFindLabel("entry_goobers_room_onload");
    //
    vmGVars[GVAR_RST_RESULT] = (ucl != NULL && ucl->type == LB_CODE) ? ucl->value : -1;
  } else if (argv[0] == CONST_FRST_GET_LEVEL_FILE_BYTE) {
    if (argc < 2) fatal("out of args in FRST_GET_LEVEL_FILE_BYTE");
    if (argv[1] < 0 || argv[1] >= levelDataSize) fatal("invalid offset in FRST_GET_LEVEL_FILE_BYTE: %d", argv[1]);
    vmGVars[GVAR_RST_RESULT] = levelData[argv[1]];
  } else if (argv[0] == CONST_FRST_GET_LEVEL_FILE_WORD) {
    if (argc < 2) fatal("out of args in FRST_GET_LEVEL_FILE_WORD");
    if (argv[1] < 0 || argv[1]+1 >= levelDataSize) fatal("invalid offset in FRST_GET_LEVEL_FILE_WORD: %d", argv[1]);
    vmGVars[GVAR_RST_RESULT] = levelData[argv[1]+1];
    vmGVars[GVAR_RST_RESULT] <<= 8;
    vmGVars[GVAR_RST_RESULT] |= levelData[argv[1]];
    if (vmGVars[GVAR_RST_RESULT] >= 32768) vmGVars[GVAR_RST_RESULT] -= 65536;
  } else if (argv[0] == CONST_FRST_SET_LEVEL_SIZE) {
    int w, h;
    //
    if (argc != 3) fatal("out of args in FRST_SET_LEVEL_SIZE");
    w = argv[1];
    h = argv[2];
    if (w < 1 || w > 32700 || h < 1 || h > 32700) fatal("invalid dimensions in FRST_SET_LEVEL_SIZE: %dx%d", w, h);
    if (bmap != NULL) free(bmap); bmap = NULL;
    if (fmap != NULL) free(fmap); fmap = NULL;
    if (xflags != NULL) free(xflags); xflags = NULL;
    fmap = calloc(w*h, 1); if (fmap == NULL) fatal("out of memory in FRST_SET_LEVEL_SIZE");
    bmap = calloc(w*h, 1); if (bmap == NULL) fatal("out of memory in FRST_SET_LEVEL_SIZE");
    xflags = calloc(w, 1); if (xflags == NULL) fatal("out of memory in FRST_SET_LEVEL_SIZE");
    mapWidth = vmGVars[GVAR_MAP_WIDTH] = w;
    mapHeight = vmGVars[GVAR_MAP_HEIGHT] = h;
    oldScrX = 0;
    oldScrY = 0;
    redrawLevel = 1;
  } else if (argv[0] == CONST_FRST_SET_LEVEL_NAME) {
    char buf[32];
    //
    getVMString(levelname, sizeof(levelname), argc, argv);
    lnamex = (320-strlen(levelname)*8)/2;
    sprintf(buf, "%d", curLevel+1);
    while (lnamex+strlen(levelname)*8+strlen(buf)*8+6*8 > 320) lnamex -= 8;
    redrawLevel = 1;
  } else if (argv[0] == CONST_FRST_SET_LEVEL_CODE) {
    getVMString(levelcode, sizeof(levelcode), argc, argv);
    redrawLevel = 1;
  } else if (argv[0] == CONST_FRST_SET_ITEM_NAME) {
    getVMString(itemname, sizeof(itemname), argc, argv);
    redrawLevel = 1;
  } else {
    if (gameRSTCB) return gameRSTCB(tid, opcode, argc, argv, argp);
    fatal("invalid RST: %d", argv[0]);
  }
  return 0; // continue
}


////////////////////////////////////////////////////////////////////////////////
void setMainLoopGame (void) {
  //fprintf(stderr, "setMainLoopGame\n");
  if (frameCB != frmGameDraw) {
    frameCB = frmGameDraw;
    keyCB = frmGameKey;
    mouseCB = frmMouse;
    gameRSTCB = gameRST;
    beforeVMCB = demoCB;
    vmMapGetCB = mapGet;
    vmMapSetCB = mapSet;
    //
    clearSpriteLayers();
    setSeed(time(NULL));
    vmPaused = 0;
    inMinimap = 0;
    doSave = 0;
    redrawLevel = 1;
    levelname[0] = levelcode[0] = itemname[0] = 0;
    //curLevel = -1;
    //demorecmode = 0;
    message[0] = 0;
    memset(fkeys, 0, sizeof(fkeys));
    memset(gamekeys, 0, sizeof(gamekeys));
  }
}

/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mainloop.h"
#include "gameglobals.h"

#include "video.h"
#include "vm.h"


////////////////////////////////////////////////////////////////////////////////
FrameCB frameCB = NULL;
KeyCB keyCB = NULL;
BeforeVMCB beforeVMCB = NULL;
MouseCB mouseCB = NULL;

int vmPaused = 0;


////////////////////////////////////////////////////////////////////////////////
static Uint32 cbFrameTimer (Uint32 interval, void *param) {
  SDL_Event evt;
  //
  evt.type = SDL_USEREVENT;
  /*
  //evt.user.type = SDL_USEREVENT;
  evt.user.code = 0;
  evt.user.data1 = NULL;
  evt.user.data2 = NULL;
  */
  SDL_PushEvent(&evt);
  //fprintf(stderr, "!\n");
  return interval;
}


static void Blit2x (void) {
  Uint32 *scrB = screen->pixels; // 320x200
  Uint32 *scrR = screenReal->pixels; // 640x480
  int needUnlock = (SDL_MUSTLOCK(screenReal)) ? SDL_LockSurface(screenReal) == 0 : 0;
  //
  if (screenReal->h == 480) {
    for (int y = 0; y < 40; ++y) {
      memset(scrR, 0, screenReal->pitch);
      scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch);
    }
  }
  //
  for (int y = 0; y < screen->h; ++y) {
    Uint32 *d = scrR, *d2 = (Uint32 *)((Uint8 *)d+screenReal->pitch), *s = scrB;
    //
    for (int x = 0; x < screen->w; ++x) {
      d[0] = d[1] = *s;
      d2[0] = d2[1] = *s;
      d += 2; d2 += 2;
      ++s;
    }
    scrB = (Uint32 *)((Uint8 *)scrB+screen->pitch);
    scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch*2);
  }
  //
  if (screenReal->h == 480) {
    for (int y = 0; y < 40; ++y) {
      memset(scrR, 0, screenReal->pitch);
      scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch);
    }
  }
  //
  if (needUnlock) SDL_UnlockSurface(screenReal);
}


static void screenFlip (void) {
  if (screenReal != NULL) {
    Blit2x();
    SDL_Flip(screenReal);
  } else {
    SDL_Flip(screen);
  }
}


// -1: quit; 1: new frame; -2: mouse moved
static int processEvents (void) {
  SDL_Event event;
  //
  while (SDL_WaitEvent(&event)) {
    switch (event.type) {
      case SDL_QUIT: // the user want to quit
        return -1;
      case SDL_KEYDOWN:
        if (event.key.keysym.sym == SDLK_RETURN && (event.key.keysym.mod&(KMOD_ALT)) != 0) {
          switchFullScreen();
          screenFlip();
          break;
        }
        // fallthru
      case SDL_KEYUP:
        if (event.key.keysym.sym == SDLK_F12) return -1; // quit
        if (keyCB) keyCB(&event.key);
        break;
      case SDL_MOUSEMOTION: {
        int x = event.motion.x;
        int y = event.motion.y;
        int buttons = event.motion.state;
        //
        if (screenReal != NULL) { x /= 2; y /= 2; }
        if (mouseCB) mouseCB(x, y, buttons);
        break; }
      case SDL_USEREVENT:
        //while (SDL_PollEvent(NULL)) processEvents();
        return 1;
      default: ;
    }
  }
  return 0;
}


static void buildFrame (void) {
  SurfaceLock lock;
  //
  lockSurface(&lock, screen);
  if (frameCB) frameCB(screen);
  unlockSurface(&lock);
  screenFlip();
}


void mainLoop (void) {
  SDL_TimerID frameTimer = SDL_AddTimer(25, cbFrameTimer, NULL);
  int eres = 1;
  //
  while (eres >= 0) {
    if (eres > 0) {
      if (!vmPaused) {
        if (beforeVMCB) beforeVMCB();
        vmExecuteAll(TVAR_IS_MENU_THREAD, vmGVars[GVAR_IN_MENU]);
        if (!vmIsThreadAlive(0)) break; // main thread is dead
      }
      buildFrame();
    }
    eres = processEvents();
  }
  SDL_RemoveTimer(frameTimer);
}

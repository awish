/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* WARNING! HERE BE DRAGONS! */
#include <ctype.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "awishcommon.h"
#include "vm.h"


////////////////////////////////////////////////////////////////////////////////
#define SECRET  (42)


////////////////////////////////////////////////////////////////////////////////
static const char *ltnames[] = {
  "GVAR",
  "TVAR",
  "SVAR",
  "CODE",
  "CONST"
};


////////////////////////////////////////////////////////////////////////////////
#define MAX_TOKEN_LENGTH  (256)

enum {
  TK_EOF = -1,
  TK_ID = 256,
  TK_NUM,
  TK_LABELDEF,

  TK_VM_OPERATOR = 400
};


enum {
  VMX_DRP = 600,
  VMX_DUP,
  VMX_RTN,
  VMX_RETURN,
  VMX_SAJ
};


////////////////////////////////////////////////////////////////////////////////
static int optWarnings = 1;


////////////////////////////////////////////////////////////////////////////////
static uint8_t vmcode[65536];
static int pc = 0;


////////////////////////////////////////////////////////////////////////////////
typedef struct MStrListItem {
  struct MStrListItem *next;
  char *macname;
  char *newname;
} MStrListItem;


typedef struct InputContext {
  struct InputContext *prev;
  FILE *fl;
  char *fname;
  int lineno;
  int ucnt;
  int uca[4];
  int wasEOF;
  char *text; // not NULL: macro argument expansion
  struct MacroDef *macro; // !NULL: we are inside macro expanding
  int tpos; // position in macro or in text
  MStrListItem *macnames;
  MStrListItem *macargs;
} InputContext;


static InputContext *ictx = NULL;
static int token;
static char tstr[MAX_TOKEN_LENGTH+1];
static int tint;
static int incLevel = 0;


////////////////////////////////////////////////////////////////////////////////
typedef struct MacroDef {
  struct MacroDef *next;
  char *name;
  char *text;
  int argc;
  MStrListItem *argnames;
} MacroDef;


static MacroDef *macros = NULL;


static void freeMStrList (MStrListItem *list) {
  while (list != NULL) {
    MStrListItem *c = list;
    //
    list = c->next;
    if (c->newname != NULL) free(c->newname);
    if (c->macname != NULL) free(c->macname);
    free(c);
  }
}


static void freeMacroList (void) {
  while (macros != NULL) {
    MacroDef *mc = macros;
    //
    macros = mc->next;
    freeMStrList(mc->argnames);
    if (mc->name) free(mc->name);
    if (mc->text) free(mc->text);
    free(mc);
  }
}


////////////////////////////////////////////////////////////////////////////////
static __attribute__((__noreturn__)) __attribute__((format(printf, 1, 2))) void fatal (const char *fmt, ...) {
  va_list ap;
  //
  while (ictx != NULL && ictx->fl == NULL) ictx = ictx->prev;
  //
  if (ictx != NULL) {
    fprintf(stderr, "FATAL (line %d, file '%s'): ", ictx->lineno, ictx->fname);
  } else {
    fprintf(stderr, "FATAL: ");
  }
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fprintf(stderr, "\n");
  exit(1);
}


////////////////////////////////////////////////////////////////////////////////
static void addMacroArgDef (MacroDef *mc, const char *name) {
  MStrListItem *p = NULL, *c;
  //
  for (MStrListItem *ml = mc->argnames; ml != NULL; p = ml, ml = ml->next) {
    if (strcmp(ml->macname, name) == 0) fatal("duplicate macro argument name: '%s'", name);
  }
  if ((c = calloc(1, sizeof(MStrListItem))) == NULL) fatal("out of memory");
  if ((c->macname = strdup(name)) == NULL) fatal("out of memory");
  //
  if (p != NULL) {
    p->next = c;
  } else {
    mc->argnames = c;
  }
  ++(mc->argc);
}


static MStrListItem *genUniqueMacName (MStrListItem *list, const char *macname) {
  MStrListItem *res;
  static int count = 0;
  //
  for (res = list; res != NULL; res = res->next) if (strcmp(res->macname, macname) == 0) return list;
  if ((res = calloc(1, sizeof(MStrListItem))) == NULL) fatal("out of memory");
  if ((res->macname = strdup(macname)) == NULL) fatal("out of memory");
  if ((res->newname = calloc(1, 128)) == NULL) fatal("out of memory");
  sprintf(res->newname, ". lmd #%d", count++);
  res->next = list;
  return res;
}


static MStrListItem *addMacroArg (MStrListItem *args, const char *argname, const char *argtext) {
  MStrListItem *res;
  //
  if ((res = calloc(1, sizeof(MStrListItem))) == NULL) fatal("out of memory");
  if ((res->macname = strdup(argname)) == NULL) fatal("out of memory");
  if ((res->newname = strdup(argtext)) == NULL) fatal("out of memory");
  res->next = args;
  return res;
}


////////////////////////////////////////////////////////////////////////////////
static void openFile (const char *fname) {
  InputContext *ic = calloc(1, sizeof(InputContext));
  //
  if (ic == NULL) fatal("out of memory");
  ic->fl = fopen(fname, "r");
  if (ic->fl == NULL) fatal("can't open file: '%s'", fname);
  ic->prev = ictx;
  ic->fname = strdup(fname);
#ifdef _WIN32
  for (char *t = ic->fname; *t; ++t) if (*t == '\\') *t = '/';
#endif
  ic->lineno = 1;
  ictx = ic;
  for (int f = 0; f < incLevel; ++f) fputc(' ', stderr);
  ++incLevel;
  fprintf(stderr, "compiling: %s\n", ic->fname);
}


static void openText (const char *text) {
  InputContext *ic = calloc(1, sizeof(InputContext));
  //
  if (ic == NULL) fatal("out of memory");
  ic->prev = ictx;
  ic->lineno = 1;
  if ((ic->text = strdup(text)) == NULL) fatal("out of memory");
  ictx = ic;
}


static void openMacro (MacroDef *mac, MStrListItem *args) {
  InputContext *ic = calloc(1, sizeof(InputContext));
  //
  if (ic == NULL) fatal("out of memory");
  ic->prev = ictx;
  ic->lineno = 1;
  ic->macro = mac;
  ic->macargs = args;
  ictx = ic;
}


static void closeFile (void) {
  if (ictx != NULL) {
    InputContext *ic = ictx;
    //
    ictx = ic->prev;
    if (ic->fl != NULL) {
      fclose(ic->fl);
      --incLevel;
    }
    if (ic->fname) free(ic->fname);
    if (ic->text != NULL) free(ic->text);
    freeMStrList(ic->macnames);
    freeMStrList(ic->macargs);
    free(ic);
  }
}


static inline int inText (void) {
  return (ictx != NULL && ictx->text != NULL);
}


////////////////////////////////////////////////////////////////////////////////
static int nextToken (void);

static void transformToken (void) {
  // check if this is macroarg
  if (!inText() && (token == TK_LABELDEF || token == TK_ID)) {
    for (InputContext *ic = ictx; ic != NULL; ic = ic->prev) {
      for (MStrListItem *ml = ic->macargs; ml != NULL; ml = ml->next) {
        if (strcmp(tstr, ml->macname) == 0) {
          if (token != TK_ID) fatal("macro argument redefinitions are prohibited");
          //printf("marg [%s]: [%s]\n", ml->macname, ml->newname);
          openText(ml->newname);
          nextToken();
          return;
        }
      }
    }
  }
  // check macro labels
  if (!inText() && tstr[0] == '$' && (token == TK_LABELDEF || token == TK_ID)) {
    for (InputContext *ic = ictx; ic != NULL; ic = ic->prev) {
      for (MStrListItem *ml = ic->macnames; ml != NULL; ml = ml->next) {
        if (strcmp(tstr, ml->macname) == 0) { strcpy(tstr, ml->newname); return; }
      }
    }
  }
  // not found or in text; generate new macro name
  if (!inText() && tstr[0] == '$' && (token == TK_LABELDEF || token == TK_ID)) {
    for (InputContext *ic = ictx; ic != NULL; ic = ic->prev) {
      if (ic->macro != NULL) {
        strcpy(tstr, (ic->macnames = genUniqueMacName(ic->macnames, tstr))->newname);
        break;
      }
    }
  }
}


static void ungetChar (int c) {
  if (ictx != NULL) {
    if (ictx->ucnt >= 4) fatal("too many unread chars");
    ictx->uca[ictx->ucnt++] = c;
  }
}


static int tokenWantFileName = 0;

static int nextChar (void) {
  int c;
  //
  if (ictx == NULL) return EOF;
  if (ictx->ucnt > 0) {
    c = ictx->uca[--ictx->ucnt];
  } else {
    if (ictx->wasEOF) {
      c = EOF;
    } else if (ictx->fl != NULL) {
      c = fgetc(ictx->fl);
      if (c == 0) c = ' ';
    } else if (ictx->text != NULL) {
      c = ictx->text[ictx->tpos++];
      if (c == 0) { --(ictx->tpos); c = EOF; }
    } else {
      c = ictx->macro->text[ictx->tpos++];
      if (c == 0) { --(ictx->tpos); c = EOF; }
    }
    if (c == EOF) {
      if (ictx->wasEOF) {
        closeFile();
        c = (ictx==NULL ? EOF : '\n');
      } else {
        ictx->wasEOF = 1;
        c = ' ';
      }
    } else {
      if (c == '\n') ++(ictx->lineno);
    }
  }
  if (!tokenWantFileName) {
    if (c >= 'A' && c <= 'Z') c += 32; // tolower
  }
  return c;
}


static void skipSpaces (int allowNL) {
  for (;;) {
    int c = nextChar();
    //
    if (c == EOF) return;
    if (c == '\n' && !allowNL) { ungetChar(c); return; }
    if (c == ';') {
      do { c = nextChar(); } while (c != EOF && c != '\n');
      if (!allowNL) { if (c != EOF) ungetChar(c); return; }
      continue;
    }
    if (c == '/') {
      int c1 = nextChar();
      //
      if (c1 == '/') {
        do { c = nextChar(); } while (c != EOF && c != '\n');
        if (!allowNL) { if (c != EOF) ungetChar(c); return; }
        continue;
      }
      if (c1 == '*') {
        int wasNL = 0;
        //
        for (;;) {
          c = nextChar();
          if (c == EOF) break;
          if (c == '\n') wasNL = 1;
          if (c == '*') {
            c = nextChar();
            if (c == '\n') wasNL = 1;
            if (c == '/') break;
            if (c == EOF) fatal("unterminated comment");
          }
        }
        if (!allowNL && wasNL) { ungetChar('\n'); return; }
        continue;
      }
      ungetChar(c1);
      ungetChar(c);
      break;
    }
    if (c > 32) { ungetChar(c); break; }
  }
}


static int digit (int c, int base) {
  if (c == EOF) return -1;
  if (c >= 'a' && c <= 'z') c -= 32;
  if (c > '9' && c < 'A') return -1;
  if (c > '9') c -= 7;
  c -= '0';
  if (c < 0 || c >= base) return -1;
  return c;
}


static void getNumber (int c) {
  int base = 10;
  //
  token = TK_NUM;
  tint = 0;
  if (c == '0') {
    c = nextChar();
    if (c == EOF) return;
    if (!isdigit(c)) {
      switch (c) {
        case 'b': case 'B': base = 2; break;
        case 'o': case 'O': base = 8; break;
        case 'd': case 'D': base = 10; break;
        case 'x': case 'X': base = 16; break;
        default:
          if (isalpha(c)) fatal("invalid number");
          if (c != EOF) ungetChar(c);
          return;
      }
      c = nextChar();
      if (digit(c, base) < 0) fatal("invalid number");
    }
  }
  //
  for (;;) {
    int d = digit(c, base);
    //
    if (d < 0) break;
    tint = (tint*base)+d;
    if (tint > 32767) fatal("number constant too big");
    c = nextChar();
  }
  if (c != EOF && isalpha(c)) fatal("invalid number");
  if (c != EOF) ungetChar(c);
}


static int nextToken (void) {
  int c;
  //
  memset(tstr, 0, sizeof(tstr));
  tint = 0;
  token = TK_EOF;
  skipSpaces(1);
  if (ictx == NULL) return TK_EOF;
  c = nextChar();
  if (c == EOF) return TK_EOF;
  if (c >= 127) fatal("invalid char (%d)", c);
  tstr[0] = c;
  tstr[1] = 0;
  //
  if (isalpha(c) || c == '_' || c == '$' || c == '.' || c == '@' || (tokenWantFileName && (c == '/' || c == '\\'))) {
    // id or label
    int f = 1;
    //
    for (;;) {
      c = nextChar();
      if (isalnum(c) || c == '_' || c == '$' || c == '.' || c == '@' || (tokenWantFileName && (c == '/' || c == '\\'))) {
        if (f >= MAX_TOKEN_LENGTH-1) fatal("identifier too long");
        //if (c >= 'A' && c <= 'Z') c += 32; // tolower
        tstr[f++] = c;
      } else {
        //if (c != ';' && c > 32) fatal("invalid identifier");
        break;
      }
    }
    tstr[f] = 0;
    if (!isalnum(tstr[0]) && !tstr[1]) {
      if (c != EOF) ungetChar(c);
      token = tstr[0];
      return token;
    }
    token = TK_ID;
    // label definition?
    if (!tokenWantFileName) {
      if (c == ':') {
        token = TK_LABELDEF;
      } else {
        if (c != EOF) ungetChar(c);
      }
      // vm mnemonics?
      if (f == 3) {
        for (f = 0; vmOpNames[f]; ++f) {
          if (strcmp(tstr, vmOpNames[f]) == 0) {
            if (token == TK_LABELDEF) fatal("invalid label: '%s'", tstr);
            token = TK_VM_OPERATOR+f;
            break;
          }
        }
        if (token < TK_VM_OPERATOR) {
          if (strcmp(tstr, "drp") == 0) token = VMX_DRP;
          else if (strcmp(tstr, "dup") == 0) token = VMX_DUP;
          else if (strcmp(tstr, "rtn") == 0) token = VMX_RTN;
          else if (strcmp(tstr, "saj") == 0) token = VMX_SAJ;
        }
      } else if (strcmp(tstr, "return") == 0) {
        token = VMX_RETURN;
      }
      if (strcmp(tstr, "@@") == 0) strcpy(tstr, "."); // special label
      transformToken(); // macro transformations
    }
  } else if (c == '-' || c == '+') {
    int neg = (c=='-');
    //
    token = c;
    if ((c = nextChar()) != EOF) {
      if (isdigit(c)) {
        getNumber(c);
        if (neg) tint = -tint;
      } else {
        ungetChar(c);
      }
    }
  } else if (isdigit(c)) {
    // number
    getNumber(c);
  } else {
    // delimiter
    token = c;
  }
  return token;
}


////////////////////////////////////////////////////////////////////////////////
typedef struct LabelRefInfo {
  struct LabelRefInfo *next;
  int pc;
  int size;
} LabelRefInfo;


typedef struct LabelInfo {
  struct LabelInfo *next;
  char *name;
  int type; // LB_XXXX
  int value; // code && <0: not defined yet (forward ref)
  int ext; // extern (<0), public(>0), normal(0)
  LabelRefInfo *fixes; // for undefined code labels
  LabelRefInfo *refs; // for externals
  int used;
} LabelInfo;


static LabelInfo *labels = NULL;
static LabelRefInfo *relrefs = NULL; // info for relocaions
static LabelRefInfo *refFwd = NULL; // @@f references (will be resolved on next @@)
static int prevTmpLabelPC = -1; // previous @@ PC (-1: not defined yet)
static LabelInfo labelTempBack, labelTempFwd;

static int vglast = 0;
static int vtlast = 0;
static int vtloc = VM_VARS_SIZE-1; // local thread (var 126 is used for internal thing)


typedef struct VarFixup {
  struct VarFixup *next;
  int pc;
} VarFixup;


static VarFixup *gvfixes = NULL;
static VarFixup *tvfixes = NULL;

static VarFixup *addVarFixup (VarFixup *list, int pc) {
  VarFixup *res = calloc(1, sizeof(VarFixup));
  //
  if (res == NULL) fatal("out of memory");
  res->next = list;
  res->pc = pc;
  return res;
}


static void addGVarFixup (int pc) {
  gvfixes = addVarFixup(gvfixes, pc);
}


static void addTVarFixup (int pc) {
  tvfixes = addVarFixup(tvfixes, pc);
}


static LabelRefInfo *lrefRemoveDups (LabelRefInfo *list) {
  LabelRefInfo *p = NULL, *c = list;
  //
  list = NULL;
  while (c != NULL) {
    int dup = 0;
    //
    if (list != NULL) {
      for (LabelRefInfo *r = list; r != c; r = r->next) if (r->pc == c->pc && r->size == c->size) { dup = 1; break; }
    }
    //
    if (dup) {
      // remove
      LabelRefInfo *n = c->next;
      //
      if (p != NULL) p->next = n;
      free(c);
      c = n;
    } else {
      // keep
      if (p == NULL) list = c;
      p = c;
      c = c->next;
    }
  }
  return list;
}


// returns new head (created item)
static LabelRefInfo *addLabelRefToList (LabelRefInfo *list, int pc) {
  LabelRefInfo *res;
  //
  res = calloc(1, sizeof(LabelRefInfo));
  if (res == NULL) fatal("out of memory");
  res->next = list;
  res->pc = pc;
  res->size = 2;
  return res;
}


static void freeLabelRefList (LabelRefInfo *list) {
  while (list != NULL) {
    LabelRefInfo *r = list;
    //
    list = r->next;
    free(r);
  }
}


static void addExternRef (LabelInfo *l, int pc, int size) {
  if (l == &labelTempBack || l == &labelTempFwd) return;
  if (l != NULL && l->ext < 0) {
    if (size <= 0) {
      size = (l->type == LB_CODE || l->type == LB_CONST || l->type == LB_SVAR) ? 2 : 1;
    }
    l->refs = addLabelRefToList(l->refs, pc);
    l->refs->size = size;
  }
}


static void addLabelRef (LabelInfo *l, int pc) {
  if (l == &labelTempBack) {
    if (prevTmpLabelPC < 0) fatal("no backward '@@' defined");
    //fprintf(stderr, "backref to '@@'\n");
    vmcode[pc+0] = prevTmpLabelPC&0xff;
    vmcode[pc+1] = (prevTmpLabelPC>>8)&0xff;
    relrefs = addLabelRefToList(relrefs, pc);
    return;
  }
  if (l == &labelTempFwd) {
    //fprintf(stderr, "fwdref to '@@'\n");
    refFwd = addLabelRefToList(refFwd, pc);
    relrefs = addLabelRefToList(relrefs, pc);
    return;
  }
  if (l != NULL) {
    l->used = 1;
    addExternRef(l, pc, -1);
    if (l->type == LB_CODE && l->ext >= 0) {
      // record fixup info for undefined code labels
      if (l->value < 0) l->fixes = addLabelRefToList(l->fixes, pc);
      // record reference info for code labels
      relrefs = addLabelRefToList(relrefs, pc);
    }
  }
}


static void fixupFwdTmpRefs (void) {
  for (LabelRefInfo *fix = refFwd; fix != NULL; fix = fix->next) {
    vmcode[fix->pc+0] = pc&0xff;
    vmcode[fix->pc+1] = (pc>>8)&0xff;
  }
  freeLabelRefList(refFwd);
  refFwd = NULL;
}


static void fixupLabelRefs (LabelInfo *l, int pc) {
  if (l == &labelTempBack || l == &labelTempFwd) return;
  if (l != NULL) {
    l->used = 1;
    l->value = pc;
    for (LabelRefInfo *fix = l->fixes; fix != NULL; fix = fix->next) {
      vmcode[fix->pc+0] = (l->value)&0xff;
      vmcode[fix->pc+1] = ((l->value)>>8)&0xff;
    }
    freeLabelRefList(l->fixes);
    l->fixes = NULL;
  }
}


static void checkLabels (void) {
  if (refFwd != NULL) fatal("unresolved forward references to '@@' found");
  for (LabelInfo *l = labels; l != NULL; l = l->next) {
    if (l->type == LB_CODE && l->ext >= 0 && l->value < 0) fatal("undefined %slabel: '%s'", l->used?"":"not referenced ", l->name);
    if (!l->used) {
      l->used = -1;
      if (optWarnings && l->type != LB_CONST && strcmp(l->name, "retval") != 0 && l->ext == 0) fprintf(stderr, "WARNING: unused %s label '%s'\n", ltnames[l->type], l->name);
    }
  }
}


static void freeLabels (void) {
  checkLabels();
  while (labels) {
    LabelInfo *l = labels;
    //
    labels = l->next;
    freeLabelRefList(l->fixes);
    freeLabelRefList(l->refs);
    free(l->name);
    free(l);
  }
}


static void freeLocalLabels (int onlyVars) {
  LabelInfo *p = NULL, *l = labels;
  //
  if (refFwd != NULL) fatal("unresolved references to '@@' found");
  prevTmpLabelPC = -1;
  while (l != NULL) {
    LabelInfo *n = l->next;
    //
    if ((!onlyVars || l->type != LB_CODE) && l->name[0] == '.') {
      if (l->type == LB_CODE && l->ext >= 0 && l->value < 0) fatal("undefined %slabel: '%s'", l->used?"":"not referenced ", l->name);
      if (!l->used) {
        l->used = -1;
        if (optWarnings && l->type != LB_CONST && strcmp(l->name, "retval") != 0 && l->ext == 0) fprintf(stderr, "WARNING: unused %s label '%s'\n", ltnames[l->type], l->name);
      }
      //if (l->type == LB_CODE && l->value < 0) fatal("undefined label: '%s'", l->name);
      freeLabelRefList(l->fixes);
      freeLabelRefList(l->refs);
      free(l->name);
      free(l);
      if (p != NULL) p->next = n; else labels = n;
    } else {
      p = l;
    }
    l = n;
  }
}


static AWISH_PURE LabelInfo *findLabel (const char *name) {
  if (name && strcmp(name, "@@b") == 0) return &labelTempBack;
  if (name && strcmp(name, "@@f") == 0) return &labelTempFwd;
  if (name && strcmp(name, ".") == 0) {
    //fprintf(stderr, "LOOKUP: '@@'\n");
    return NULL;
  }
  if (name && name[0] == '@') ++name;
  if (!name || !name[0]) return NULL;
  for (LabelInfo *l = labels; l != NULL; l = l->next) if (strcmp(l->name, name) == 0) return l;
  return NULL;
}


static LabelInfo *addLabel (const char *name) {
  LabelInfo *l;
  //
  if (!name || !name[0]) fatal("internal error: empty label name");
  if (strcmp(name, "@@b") == 0 || strcmp(name, "@@f") == 0 || strcmp(name, ".") == 0) fatal("can't define special label: '%s'", name);
  if (findLabel(name)) fatal("duplicate label: '%s'", name);
  l = calloc(1, sizeof(LabelInfo));
  if (l == NULL) fatal("out of memory");
  l->name = strdup(name);
  if (l->name == NULL) fatal("out of memory");
  l->type = LB_CODE;
  l->value = -1;
  l->ext = 0;
  l->fixes = NULL;
  l->refs = NULL;
  l->next = labels;
  labels = l;
  return l;
}


static void newTempLabel (void) {
  fixupFwdTmpRefs();
  prevTmpLabelPC = pc;
  //fprintf(stderr, "new '@@'\n");
}


////////////////////////////////////////////////////////////////////////////////
typedef struct OperandInfo {
  LabelInfo *l; // !=NULL: label
  int var; // [...]
  int value; // if l==NULL
  int vartype; // 0: global; 1: tlocal; 2: stack
  struct OperandInfo *next;
} OperandInfo;


static int stackAdj = 0;


static void setIntOperand (OperandInfo *op, int value) {
  op->l = NULL;
  op->var = 0;
  op->value = value;
  op->vartype = -1;
}


static void setLabelOperand (OperandInfo *op, LabelInfo *l) {
  op->l = l;
  op->var = 0;
  op->value = 0;
  op->vartype = -1;
}


static void getOperand (OperandInfo *op, int wantCode) {
  if (token == TK_ID) {
    LabelInfo *l = findLabel(tstr);
    //
    if (l == NULL) {
      // new code label
      l = addLabel(tstr);
      l->type = LB_CODE;
      l->value = -1;
    } else {
      if (wantCode && l->type != LB_CODE) fatal("code offset expected");
    }
    setLabelOperand(op, l);
    nextToken();
    return;
  }
  //
  if (token == TK_NUM) {
    if (wantCode) fatal("code offset expected");
    setIntOperand(op, tint);
    nextToken();
    return;
  }
  //
  if (token == '[') {
    //if (wantCode) fatal("code offset expected");
    nextToken();
    if (token == TK_ID) {
      LabelInfo *l = findLabel(tstr);
      //
      if (l == NULL || l->type == LB_CODE) fatal("unknown variable: '%s'", tstr);
      setLabelOperand(op, l);
    } else if (token == '@' || token == '.') {
      int loc = token;
      //
      if (nextToken() != TK_NUM) fatal("index expected");
      setIntOperand(op, tint);
      if (loc != '.') {
        // not stack
        if (tint < 0 || tint > 126) fatal("invalid variable index");
        op->vartype = loc=='@' ? 0 : 1;
      } else {
        // stack
        op->vartype = 2;
      }
    } else if (token == TK_NUM) {
      // local
      if (tint < 0 || tint > 126) fatal("invalid variable index");
      setIntOperand(op, tint);
      op->vartype = 1;
    } else {
      fatal("invalid operand");
    }
    op->var = 1;
    if (nextToken() != ']') fatal("']' expected");
    nextToken();
    return;
  }
  //
  fprintf(stderr, "*%d [%s] %d\n", token, tstr, tint);
  fatal("invalid operand");
}


static inline int hasOperand (void) {
  return (token != TK_EOF && token != TK_LABELDEF && token < TK_VM_OPERATOR);
}


////////////////////////////////////////////////////////////////////////////////
static void emitByte (int b) {
  if (b < 0 || b > 255) fatal("internal error");
  if (pc >= 32768) fatal("code too big");
  vmcode[pc++] = b;
}


static void emitOpCode (int opc, int opcount) {
  if (opc < 0 || opc > 63 || opcount < 0 || opcount > 3) fatal("internal error");
  emitByte(opc|(opcount<<6));
}


/*
static void emitInteger (int val) {
  emitByte(255); // special
  emitByte(val&0xff);
  emitByte((val>>8)&0xff);
}
*/


static void emitOperand (OperandInfo *op) {
  if (!op || op->var < 0) return;
  //
  if (op->var > 0) {
    // variable
    if (op->l) {
      // from label
      op->l->used = 1;
      switch (op->l->type) {
        case LB_GVAR:
          emitByte(op->l->value|0x80);
          addLabelRef(op->l, pc-1);
          if (op->l->ext >= 0 && op->l->name[0] != '.') addGVarFixup(pc-1);
          break;
        case LB_TVAR:
          emitByte(op->l->value);
          addLabelRef(op->l, pc-1);
          if (op->l->ext >= 0 && op->l->name[0] != '.') addTVarFixup(pc-1);
          break;
        case LB_SVAR:
          emitByte(127); // special
          if (op->l->value < 0) {
            emitByte((op->l->value-stackAdj)&0xff);
            emitByte(((op->l->value-stackAdj)>>8)&0xff);
          } else {
            emitByte(op->l->value&0xff);
            emitByte((op->l->value>>8)&0xff);
          }
          addLabelRef(op->l, pc-2);
          break;
        default: fatal("internal error");
      }
    } else {
      // from value
      switch (op->vartype) {
        case 0: emitByte(op->value|0x80); break; // global
        case 1: emitByte(op->value); break; // tlocal
        case 2: // stack
          emitByte(127); // special
          if (op->value < 0) {
            emitByte((op->value-stackAdj)&0xff);
            emitByte(((op->value-stackAdj)>>8)&0xff);
          } else {
            emitByte(op->value&0xff);
            emitByte((op->value>>8)&0xff);
          }
          break;
        default: fatal("internal error");
      }
    }
    return;
  }
  // code label
  if (op->var == 0) {
    // immediate
    emitByte(255); // special
    if (op->l) {
      // from label
      op->l->used = 1;
      emitByte(op->l->value&0xff);
      emitByte((op->l->value>>8)&0xff);
      addLabelRef(op->l, pc-2);
    } else {
      // direct
      if (op->value < -32767 || op->value > 32767) fatal("invalid value");
      emitByte(op->value&0xff);
      emitByte((op->value>>8)&0xff);
    }
  }
}


static void emitInstruction (int opc, OperandInfo *op0, OperandInfo *op1, OperandInfo *op2) {
  int ocnt = 0;
  //
  if (op0 && op0->var >= 0) ++ocnt;
  if (ocnt == 1 && op1 && op1->var >= 0) ++ocnt;
  if (ocnt == 2 && op2 && op2->var >= 0) ++ocnt;
  emitOpCode(opc, ocnt);
  if (ocnt > 0) emitOperand(op0);
  if (ocnt > 1) emitOperand(op1);
  if (ocnt > 2) emitOperand(op2);
}


////////////////////////////////////////////////////////////////////////////////
static void doNoOperands (int opcode) {
  emitInstruction(opcode, NULL, NULL, NULL);
}


static void doMath (int opcode) {
  OperandInfo op0, op1, op2;
  //
  op0.var = op1.var = op2.var = -1;
  if (hasOperand()) {
    getOperand(&op0, 0);
    if (token == ',') {
      nextToken();
      getOperand(&op1, 0);
      if (token == ',') {
        nextToken();
        getOperand(&op2, 0);
        if (op2.var != 1) fatal("variable expected as third operand");
      } else {
        if (op0.var != 1) fatal("variable expected as first operand");
      }
    }
  }
  emitInstruction(opcode, &op0, &op1, &op2);
}


static void doJXX (int opcode) {
  OperandInfo op0, op1, op2;
  //
  op0.var = op1.var = op2.var = -1;
  if (hasOperand()) {
    getOperand(&op0, 1);
    if (token == ',') {
      nextToken();
      getOperand(&op1, 0);
      if (token == ',') {
        nextToken();
        getOperand(&op2, 0);
      }
    }
  }
  emitInstruction(opcode, &op0, &op1, &op2);
}


static void doBranch (int opcode) {
  OperandInfo op0;
  //
  op0.var = -1;
  if (hasOperand()) {
    getOperand(&op0, 1);
  }
  emitInstruction(opcode, &op0, NULL, NULL);
}


static void doBSR (int opcode) {
  OperandInfo op0, *op1 = NULL, *op2 = NULL, *olist = NULL, *c = NULL;
  int osadj = stackAdj, ocnt = 0;;
  //
  op0.var = -1;
  if (hasOperand()) {
    getOperand(&op0, (opcode == VM_BSR) ? 1 : 0);
    // collect other operands
    while (token == ',') {
      OperandInfo *o = calloc(sizeof(OperandInfo), 1);
      //
      if (o == NULL) fatal("out of memory");
      nextToken();
      getOperand(o, 0);
      if (olist != NULL) c->next = o; else olist = o;
      c = o;
      o->next = NULL;
      ++ocnt;
    }
    //
    switch (ocnt) {
      case 1:
        op1 = olist;
        olist = NULL;
        break;
      case 2:
        op1 = olist;
        op2 = olist->next;
        olist = NULL;
        break;
    }
    // generate pushes
    while (olist != NULL) {
      OperandInfo *o = olist;
      //
      if (o->next->next == NULL) {
        // last 2 operands
        op1 = olist;
        op2 = olist->next;
        olist = NULL;
        break;
      }
      olist = o->next;
      emitInstruction(VM_PSH, o, NULL, NULL);
      ++stackAdj;
      free(o);
    }
  }
  emitInstruction(opcode, &op0, op1, op2);
  if (op2 != NULL) free(op2);
  if (op1 != NULL) free(op1);
  stackAdj = osadj;
}


static void doMap (int opcode, int lastMBV) {
  OperandInfo op0, op1, op2;
  //
  op0.var = op1.var = op2.var = -1;
  if (hasOperand()) {
    getOperand(&op0, 0);
    if (token == ',') {
      nextToken();
      getOperand(&op1, 0);
      if (token == ',') {
        nextToken();
        getOperand(&op2, 0);
      }
    }
  }
  emitInstruction(opcode, &op0, &op1, &op2);
}


static void doRST (int opcode) {
  doBSR(opcode);
}


static void doSet (int opcode) {
  OperandInfo op0, op1, op2;
  //
  op0.var = op1.var = op2.var = -1;
  getOperand(&op0, 0);
  if (token == ',') {
    nextToken();
    getOperand(&op1, 0);
    if (token == ',') {
      nextToken();
      getOperand(&op2, 0);
    }
  }
  if (op2.var < 0 && op0.var != 1) fatal("first operand should be var");
  emitInstruction(opcode, &op0, &op1, &op2);
}


static void doDrop (int opcode) {
  OperandInfo op0;
  //
  op0.var = -1;
  if (hasOperand()) getOperand(&op0, 0);
  if (op0.var > 0) fatal("number expected");
  if (op0.value < 0) fatal("positive number expected");
  emitInstruction(opcode, &op0, NULL, NULL);
}


static void doPush (int opcode) {
  OperandInfo op0, op1, op2;
  //
  op0.var = op1.var = op2.var = -1;
  getOperand(&op0, 0);
  if (token == ',') {
    nextToken();
    getOperand(&op1, 0);
    if (token == ',') {
      nextToken();
      getOperand(&op2, 0);
    }
  }
  emitInstruction(opcode, &op0, &op1, &op2);
}


static void doPop (int opcode) {
  OperandInfo op0, op1, op2;
  //
  op0.var = op1.var = op2.var = -1;
  if (hasOperand()) {
    getOperand(&op0, 0);
    if (token == ',') {
      nextToken();
      getOperand(&op1, 0);
      if (token == ',') {
        nextToken();
        getOperand(&op2, 0);
      }
    }
  }
  emitInstruction(opcode, &op0, &op1, &op2);
}


static void doSwap (int opcode) {
  OperandInfo op0, op1;
  //
  op0.var = op1.var = -1;
  if (hasOperand()) {
    getOperand(&op0, 0);
    if (token == ',') {
      nextToken();
      getOperand(&op1, 0);
    }
  }
  emitInstruction(opcode, &op0, &op1, NULL);
}


static void doDepth (int opcode) {
  OperandInfo op0;
  //
  op0.var = -1;
  if (hasOperand()) getOperand(&op0, 0);
  emitInstruction(opcode, &op0, NULL, NULL);
}


static void doPickRoll (int opcode) {
  OperandInfo op0, op1;
  //
  op0.var = op1.var = -1;
  if (hasOperand()) {
    getOperand(&op0, 0);
    if (token == ',') {
      nextToken();
      getOperand(&op1, 0);
      if (op1.var != 1) fatal("second argument must be variable");
    }
  }
  emitInstruction(opcode, &op0, &op1, NULL);
}


static void doNew (int opcode) {
  OperandInfo op0, op1;
  //
  op0.var = op1.var = -1;
  if (hasOperand()) {
    getOperand(&op0, 0);
    if (token == ',') {
      nextToken();
      getOperand(&op1, 0);
    }
  }
  emitInstruction(opcode, &op0, &op1, NULL);
}


static void doTId (int opcode) {
  OperandInfo op0;
  //
  op0.var = -1;
  if (hasOperand()) getOperand(&op0, 0);
  emitInstruction(opcode, &op0, NULL, NULL);
}


static void doKillSusRes (int opcode) {
  OperandInfo op0;
  //
  op0.var = -1;
  if (hasOperand()) getOperand(&op0, 0);
  emitInstruction(opcode, &op0, NULL, NULL);
}


static void doSta (int opcode) {
  OperandInfo op0, op1;
  //
  op0.var = op1.var = -1;
  if (hasOperand()) {
    getOperand(&op0, 0);
    if (token == ',') {
      nextToken();
      getOperand(&op1, 0);
      if (op1.var != 1) fatal("variable expected");
    }
  }
  emitInstruction(opcode, &op0, &op1, NULL);
}


static void doGet (int opcode) {
  OperandInfo op0, op1, op2;
  //
  op0.var = op1.var = op2.var = -1;
  getOperand(&op0, 0);
  if (token != ',') fatal("at least two operands expected");
  nextToken();
  getOperand(&op1, 0);
  if (token == ',') {
    nextToken();
    getOperand(&op2, 0);
    if (op2.var != 1) fatal("variable expected as third operand");
  }
  emitInstruction(opcode, &op0, &op1, &op2);
}


static void doRXC (int opcode) {
  OperandInfo op0, op1;
  //
  op0.var = op1.var = -1;
  if (hasOperand()) {
    getOperand(&op0, 0);
    if (token == ',') {
      nextToken();
      getOperand(&op1, 0);
      if (op1.var != 1) fatal("variable expected as second operand");
    }
  }
  emitInstruction(opcode, &op0, &op1, NULL);
}


static void doWXC (int opcode) {
  OperandInfo op0, op1;
  //
  op0.var = op1.var = -1;
  if (hasOperand()) {
    getOperand(&op0, 0);
    if (token == ',') {
      nextToken();
      getOperand(&op1, 0);
    }
  }
  emitInstruction(opcode, &op0, &op1, NULL);
}


static void doRet (int opcode) {
  OperandInfo op0, op1, op2;
  //
  op0.var = op1.var = op2.var = -1;
  if (hasOperand()) {
    getOperand(&op0, 0);
    if (token != ',') fatal("at least two operands expected");
    nextToken();
    getOperand(&op1, 0);
    if (token == ',') {
      nextToken();
      getOperand(&op2, 0);
    }
  }
  emitInstruction(opcode, &op0, &op1, &op2);
}


static void doSAj (int opcode) {
  if (token != TK_NUM) fatal("number expected");
  stackAdj += tint;
  nextToken();
}


////////////////////////////////////////////////////////////////////////////////
static void parseInlcude (void) {
  static char fname[8192], *t;
  //
  tokenWantFileName = 1;
  if (nextToken() != TK_ID) fatal("identifier expected");
  tokenWantFileName = 0;
  strcpy(fname, ictx->fname);
#ifdef _WIN32
  for (t = fname; *t; ++t) if (*t == '\\') *t = '/';
#endif
  t = strrchr(fname, '/');
  if (t != NULL) t[1] = 0; else fname[0] = 0;
  strcat(fname, tstr);
  if (incLevel > 64) fatal("too many includes");
  openFile(fname);
  nextToken();
}


static void parseVarList (int type, int local, int gotext) {
  for (;;) {
    LabelInfo *l;
    //
    if (nextToken() != TK_ID) fatal("identifier expected");
    if (strcmp(tstr, ".") == 0) fatal("invalid label name: '@@'");
    if (tstr[0] == '.' && local <= 0) fatal("invalid variable name: '%s'", tstr);
    if (tstr[0] != '.' && local > 0) fatal("invalid variable name: '%s'", tstr);
    l = findLabel(tstr);
    if (l != NULL) {
      if (gotext) {
        if (l->ext >= 0) fatal("can't declare existing label as extern: '%s'", tstr);
        if (l->type != type) fatal("can't change existing extern label type: '%s'", tstr);
      } else {
        fatal("duplicate variable or label: '%s'", tstr);
      }
    }
    if (l == NULL) {
      l = addLabel(tstr);
      l->type = type;
      if (local > 0 && vtloc <= vtlast) fatal("too many local vars");
      if (gotext) {
        l->value = 0;
      } else {
        l->value = (local>0)?(--vtloc):(type==LB_GVAR?vglast++:vtlast++);
      }
      if (local < 0) l->ext = 1;
    }
    if (gotext) l->ext = -1;
    if (l->value > 126) fatal("too many vars");
    if (nextToken() != ',') break;
  }
}


static void parseUsedVarList (void) {
  for (;;) {
    LabelInfo *l;
    //
    if (nextToken() != TK_ID) fatal("identifier expected");
    if (strcmp(tstr, ".") == 0) fatal("invalid label name: '@@'");
    l = findLabel(tstr);
    if (l == NULL) fatal("unknown label: '%s'", tstr);
    l->used = 1;
    if (nextToken() != ',') break;
  }
}


static void parseLocList (void) {
  for (;;) {
    LabelInfo *l;
    //
    if (nextToken() != TK_ID) fatal("identifier expected");
    if (strcmp(tstr, ".") == 0) fatal("invalid label name: '@@'");
    if (tstr[0] != '.') fatal("local identifier expected instead of '%s'", tstr);
    l = findLabel(tstr);
    if (l != NULL) fatal("can't redefine label as local: '%s'", tstr);
    l = addLabel(tstr);
    l->ext = 0;
    l->type = LB_SVAR;
    l->value = -1;
    if (nextToken() != '=') fatal("'=' expected");
    if (nextToken() != TK_NUM) fatal("number expected");
    l->value = tint;
    if (nextToken() != ',') break;
  }
}


static void parseConst (int ext, int gotext) {
  LabelInfo *l;
  //
  if (nextToken() != TK_ID) fatal("identifier expected");
  if (strcmp(tstr, ".") == 0) fatal("invalid label name: '@@'");
  if (tstr[0] == '.') fatal("invalid constant name: '%s'", tstr);
  l = findLabel(tstr);
  if (l != NULL) {
    if (gotext) {
      if (l->ext >= 0) fatal("can't declare existing label as extern: '%s'", tstr);
      if (l->type != LB_CONST) fatal("can't change existing extern label type: '%s'", tstr);
    } else {
      fatal("constant must be unique: '%s'", tstr);
    }
  }
  if (l == NULL) {
    l = addLabel(tstr);
    l->type = LB_CONST;
    l->value = 0;
  }
  if (!gotext) {
    if (nextToken() == '=') nextToken();
    if (token != TK_NUM) fatal("constant must be numeric");
    l->value = tint;
  }
  if (ext) l->ext = 1;
  if (gotext) l->ext = -1;
  nextToken();
}


static char procname[8192];
static int proclocals = 0;
static int procargs = 0;
static int lastWasReturn = 0;


static void parseProc (int ext) {
  LabelInfo *l, *a = NULL;
  int spt;
  //
  lastWasReturn = 0;
  if (nextToken() != TK_ID) fatal("identifier expected");
  if (strcmp(tstr, ".") == 0) fatal("invalid label name: '@@'");
  if (tstr[0] == '.') fatal("invalid proc name: '%s'", tstr);
  if (procname[0]) fatal("unclosed proc: '%s'", procname);
  strcpy(procname, tstr);
  proclocals = 0;
  procargs = 0;
  freeLocalLabels(0); // vars and code
  //
  l = findLabel(tstr);
  if (l != NULL) {
    if (l->type != LB_CODE || l->value >= 0) fatal("duplicate proc label: '%s'", tstr);
    fixupLabelRefs(l, pc);
  } else {
    l = addLabel(tstr);
    l->type = LB_CODE;
    l->value = pc;
  }
  if (ext) l->ext = 1;
  //
  nextToken();
  //
  while (token == TK_LABELDEF && (strcmp(tstr, "arg") == 0 || strcmp(tstr, "args") == 0)) {
    for (;;) {
      if (nextToken() != TK_ID) fatal("identifier expected");
      if (tstr[0] != '.') fatal("argument name must starts with '.'");
      l = findLabel(tstr);
      if (l != NULL) fatal("duplicate argument: '%s'", l->name);
      l = addLabel(tstr);
      l->type = LB_SVAR;
      l->value = 666;
      ++procargs;
      if (nextToken() != ',') break;
    }
    a = labels; //HACK!
    // fix values
     // -1: return address, -2: last arg
    for (spt = -2, l = a; l->type == LB_SVAR; l = l->next) {
      l->value = spt;
      --spt;
    }
  }
  //
  spt = -1; // first local
  while (token == TK_LABELDEF && (strcmp(tstr, "local") == 0 || strcmp(tstr, "locals") == 0)) {
    for (;;) {
      if (nextToken() != TK_ID) fatal("identifier expected");
      if (tstr[0] != '.') fatal("local variable name must starts with '.'");
      l = findLabel(tstr);
      if (l != NULL) fatal("duplicate local: '%s'", l->name);
      l = addLabel(tstr);
      l->type = LB_SVAR;
      l->value = spt--;
      ++proclocals;
      // fix args
      for (l = a; l != NULL && l->type == LB_SVAR; l = l->next) --(l->value);
      if (nextToken() != ',') break;
    }
  }
  //
  if (proclocals > 0) {
    // allocate stack
    OperandInfo op0;
    //
    setIntOperand(&op0, -proclocals);
    emitInstruction(VM_POP, &op0, NULL, NULL);
  }
}


static void parseEndP (void) {
  if (!procname[0]) fatal("'endp' without 'proc'");
  if (nextToken() != TK_ID) fatal("identifier expected");
  if (strcmp(procname, tstr) != 0) fatal("endp for '%s' in proc '%s'", tstr, procname);
  //if (!lastWasReturn) fatal("no 'return' in proc");
  nextToken();
  procname[0] = 0;
  freeLocalLabels(0);
}


static void doReturn (void) {
  OperandInfo op0, op1, op2;
  //
  if (!procname[0]) fatal("'return' without 'proc'");
  lastWasReturn = 1;
  op0.var = op1.var = op2.var = -1;
  if (hasOperand()) getOperand(&op2, 0); // result
  setIntOperand(&op0, proclocals);
  setIntOperand(&op1, procargs);
  emitInstruction(VM_RET, &op0, &op1, &op2);
}


static void parseLabel (int gotext) {
  LabelInfo *l;
  //
  if (gotext && tstr[0] == '.') fatal("can't declare local extern label: '%s'", tstr);
  if (strcmp(tstr, ".") == 0) {
    newTempLabel();
    nextToken();
    return;
  }
  if (tstr[0] != '.' && tstr[0] != '@') {
    if (!gotext) {
      if (procname[0]) fatal("you can not define non-special global labels inside proc ('%s')", procname);
    }
    freeLocalLabels(0); // vars and code
  }
  if (tstr[0] == '@') {
    char *d = tstr;
    //
    while (*d++) d[-1] = d[0];
    d[-1] = 0;
  }
  l = findLabel(tstr);
  if (l != NULL) {
    if (gotext) {
      if (l->ext >= 0) fatal("can't declare existing label as extern: '%s'", tstr);
      if (l->type != LB_CODE) fatal("can't change existing extern label type: '%s'", tstr);
    }
    if (l->type != LB_CODE || l->value >= 0) fatal("duplicate label: '%s'", tstr);
    fixupLabelRefs(l, pc);
  } else {
    l = addLabel(tstr);
    l->type = LB_CODE;
    l->value = pc;
  }
  if (gotext) l->ext = -1;
  nextToken();
}


static void parseDW (void) {
  for (;;) {
    LabelInfo *l = NULL;
    nextToken();
    if (token == TK_ID) {
      l = findLabel(tstr);
      //
      if (l == NULL) {
        l = addLabel(tstr);
        l->type = LB_CODE;
        l->value = -1;
        //fatal("unknown label: '%s'", tstr);
      }
      tint = l->value;
    } else if (token != TK_NUM) {
      fatal("number expected");
    }
    //if (tint < -128 || tint > 255) fatal("bad value: %d", tint);
    emitByte(tint&0xff);
    emitByte((tint>>8)&0xff);
    if (l != NULL) {
      addLabelRef(l, pc-2);
      addExternRef(l, pc-2, 2);
      if (l->ext >= 0 && l->name[0] != '.') {
        switch (l->type) {
          case LB_GVAR: addGVarFixup(pc-2); break;
          case LB_TVAR: addTVarFixup(pc-2); break;
        }
      }
    }
    if (nextToken() != ',') break;
  }
}


// terminator eaten
static void parseAndPutString (int qch) {
  for (;;) {
    int ch = nextChar();
    //
    //printf("[%c] [%c]\n", ch, qch);
    if (ch == EOF) fatal("unterminated string");
    if (qch == '"') {
      if (ch == qch) break;
      if (ch == '\\') {
        int n;
        //
        ch = nextChar();
        if (ch == EOF) fatal("invalid escape");
        switch (ch) {
          case 'a': emitByte('\a'); break;
          case 'b': emitByte('\b'); break;
          case 'e': emitByte('\x1b'); break;
          case 'f': emitByte('\f'); break;
          case 'n': emitByte('\n'); break;
          case 'r': emitByte('\r'); break;
          case 't': emitByte('\t'); break;
          case 'v': emitByte('\v'); break;
          case '"': case '\'': case '\\': case ' ': emitByte(ch); break;
          case 'x':
            n = digit(nextChar(), 16);
            if (n < 0) fatal("invalid hex escape");
            ch = nextChar();
            if (ch == EOF) fatal("invalid hex escape");
            if (digit(ch, 16) >= 0) {
              n = n*16+digit(ch, 16);
            } else {
              ungetChar(ch);
            }
            emitByte(n);
            break;
          default: fatal("invalid escape: '%c'", ch);
        }
      } else {
        emitByte(ch);
      }
    } else {
      if (ch == qch) {
        ch = nextChar();
        if (ch == EOF) return;
        if (ch == qch) {
          emitByte(ch);
          continue;
        }
        ungetChar(ch);
        break;
      } else {
        emitByte(ch);
      }
    }
  }
}


static void parseDAscii (int zeroend) {
  for (;;) {
    LabelInfo *l;
    //
    nextToken();
    if (token == TK_EOF) break;
    switch (token) {
      case '"': tokenWantFileName = 1; parseAndPutString(token); tokenWantFileName = 0; break;
      case '\'': tokenWantFileName = 1; parseAndPutString(token); tokenWantFileName = 0; break;
      case TK_ID:
        l = findLabel(tstr);
        //
        if (l == NULL) fatal("unknown label: '%s'", tstr);
        if (l->type == LB_CODE) fatal("bad label type: '%s'", l->name);
        l->used = 1;
        addExternRef(l, pc, 1);
        if (l->ext >= 0 && l->name[0] != '.') {
          switch (l->type) {
            case LB_GVAR: addGVarFixup(pc); break;
            case LB_TVAR: addTVarFixup(pc); break;
          }
        }
        tint = l->value;
        // fallthru
      case TK_NUM:
        if (tint < -128 || tint > 255) fatal("bad value: %d", tint);
        emitByte(tint&0xff);
        break;
      default:
        fatal("number expected");
    }
    if (nextToken() != ',') break;
  }
  if (zeroend) emitByte(0);
}


static void parsePublics (void) {
  for (;;) {
    LabelInfo *l;
    //
    nextToken();
    if (token == TK_LABELDEF) {
      parseLabel(0);
      break;
    }
    if (token != TK_ID) fatal("identifier expected");
    if (tstr[0] == '.') fatal("invalid label name: '%s'", tstr);
    l = findLabel(tstr);
    if (l != NULL) {
      l->ext = 1;
    } else {
      l = addLabel(tstr);
      l->ext = 1;
      l->type = LB_CODE;
      l->value = -1;
    }
    if (nextToken() != ',') break;
  }
}


static void parseMacroDef (void) {
  MacroDef *mc;
  int c;
  char *text = NULL;
  int tpos = 0, tsize = 0;
  //
  void addChar (int c) {
    if (tpos+1 > tsize) {
      int newsz = tsize+1024;
      char *nn = realloc(text, newsz);
      //
      if (nn == NULL) fatal("out of memory");
      text = nn;
      tsize = newsz;
    }
    text[tpos++] = c;
  }
  //
  if (nextToken() != TK_ID) fatal("macro name expected");
  if (tstr[0] == '.') fatal("macros can't be local: '%s'", tstr);
  for (mc = macros; mc != NULL; mc = mc->next) if (strcmp(mc->name, tstr) == 0) fatal("macros can't be redefined: '%s'", tstr);
  if ((mc = calloc(1, sizeof(MacroDef))) == NULL) fatal("out of memory");
  mc->next = macros;
  if ((mc->name = strdup(tstr)) == NULL) fatal("out of memory");
  macros = mc;
  // now parse macro args
  c = nextChar();
  ungetChar(c);
  skipSpaces(0); // no newlines allowed here
  c = nextChar();
  if (c != '\n') {
    // has args
    ungetChar(c);
    for (;;) {
      if (nextToken() != TK_ID) fatal("invalid macro argument definition");
      if (tstr[0] == '.') fatal("macro argument name must not start with dot: '%s'", tstr);
      addMacroArgDef(mc, tstr);
      skipSpaces(1);
      c = nextChar();
      if (c != ',') { ungetChar(c); break; }
    }
  }
  // now parse macro text
  for (;;) {
    int c1;
    //
    c = nextChar();
    switch (c) {
      case EOF: fatal("incomplete macro definition: '%s'", mc->name);
      case '\n': // check for macro end
        addChar(c);
        c = nextChar(); if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
        if (c != 'e') { ungetChar(c); break; }
        c = nextChar(); if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
        if (c != 'n') { ungetChar(c); addChar('e'); break; }
        c = nextChar(); if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
        if (c != 'd') { ungetChar(c); addChar('e'); addChar('n'); break; }
        c = nextChar(); if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
        if (c != 'm') { ungetChar(c); addChar('e'); addChar('n'); addChar('d'); break; }
        c = nextChar(); if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
        if (c != 'a') { ungetChar(c); addChar('e'); addChar('n'); addChar('d'); addChar('m'); break; }
        c = nextChar(); if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
        if (c != 'c') { ungetChar(c); addChar('e'); addChar('n'); addChar('d'); addChar('m'); addChar('a'); break; }
        c = nextChar(); if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
        if (c != 'r') { ungetChar(c); addChar('e'); addChar('n'); addChar('d'); addChar('m'); addChar('a'); addChar('c'); break; }
        c = nextChar(); if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
        if (c != 'o') { ungetChar(c); addChar('e'); addChar('n'); addChar('d'); addChar('m'); addChar('a'); addChar('c'); addChar('r'); break; }
        c = nextChar();
        if (c == EOF) goto macro_complete;
        if (!isspace(c) && c != ';' && c != '/') { ungetChar(c); addChar('e'); addChar('n'); addChar('d'); addChar('m'); addChar('a'); addChar('c'); addChar('r'); addChar('o'); break; }
        else {
          skipSpaces(0); // no EOLs
          c = nextChar();
          if (c != '\n') fatal("invalid endmacro: '%s' (%d)", mc->name, c);
          goto macro_complete;
        }
        break;
      case '"': // string
        addChar(c);
        tokenWantFileName = 1;
        for (;;) {
          c = nextChar();
          if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
          addChar(c);
          if (c == '"') break;
          if (c == '\\') {
            c = nextChar();
            if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
            addChar(c);
          }
        }
        tokenWantFileName = 0;
        break;
      case '\'': // string
        addChar(c);
        tokenWantFileName = 1;
        for (;;) {
          c = nextChar();
          if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
          addChar(c);
          if (c == '\'') {
            c = nextChar();
            if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
            if (c != '\'') { ungetChar(c); break; }
            addChar(c);
          }
        }
        tokenWantFileName = 0;
        break;
      case ';': // comment
one_line_comment:
        addChar('\n');
        for (;;) {
          c = nextChar();
          if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
          if (c == '\n') break;
        }
        break;
      case '/':
        c1 = nextChar();
        if (c1 == '/') goto one_line_comment; // comment
        if (c1 == '*') {
          // multiline comment
          for (;;) {
            c = nextChar();
            if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
            if (c == '*') {
              c = nextChar();
              if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
              if (c == '/') break;
            }
          }
        } else if (c1 == EOF) {
          fatal("incomplete macro definition: '%s'", mc->name);
        } else {
          addChar(c);
          addChar(c1);
        }
        break;
      default:
        addChar(c);
        break;
    }
  }
macro_complete:
  addChar('\0');
  mc->text = text;
  nextToken();
}


static void parseMacroInvocation (MacroDef *mc) {
  MStrListItem *args = NULL;
  //
  if (mc->argnames != NULL) {
    nextToken(); // skip macro name
    // collect macro arguments
    for (MStrListItem *a = mc->argnames; a != NULL; a = a->next) {
      int pos, c;
      //
      switch (token) {
        case TK_EOF: fatal("macro argument expected: '%s'", mc->name);
        case TK_LABELDEF: fatal("macro argument can't be label: '%s'", mc->name);
        case TK_NUM: sprintf(tstr, "%d", tint); break;
        case TK_ID: break; // ok
        case '"': // string
          pos = 0;
          tstr[pos++] = token;
          tokenWantFileName = 1;
          for (;;) {
            c = nextChar();
            if (c == EOF) fatal("incomplete macro argument: '%s'", mc->name);
            if (pos >= MAX_TOKEN_LENGTH) fatal("macro argument too long: '%s'", mc->name);
            tstr[pos++] = c;
            if (c == '"') break;
            if (c == '\\') {
              c = nextChar();
              if (c == EOF) fatal("incomplete macro argument: '%s'", mc->name);
              if (pos >= MAX_TOKEN_LENGTH) fatal("macro argument too long: '%s'", mc->name);
              tstr[pos++] = c;
            }
          }
          tokenWantFileName = 0;
          tstr[pos] = 0;
          break;
        case '\'': // string
          pos = 0;
          tstr[pos++] = token;
          tokenWantFileName = 1;
          for (;;) {
            c = nextChar();
            if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
            if (pos >= MAX_TOKEN_LENGTH) fatal("macro argument too long: '%s'", mc->name);
            tstr[pos++] = c;
            if (c == '\'') {
              c = nextChar();
              if (c == EOF) fatal("incomplete macro definition: '%s'", mc->name);
              if (c != '\'') { ungetChar(c); break; }
              if (pos >= MAX_TOKEN_LENGTH) fatal("macro argument too long: '%s'", mc->name);
              tstr[pos++] = c;
            }
          }
          tokenWantFileName = 0;
          tstr[pos] = 0;
          break;
        default:
          if (token >= TK_VM_OPERATOR) break;
          pos = 0;
          tstr[pos++] = token;
          for (;;) {
            c = nextChar();
            if (c == EOF) break;//fatal("incomplete macro definition: '%s'", mc->name);
            if (pos >= MAX_TOKEN_LENGTH) fatal("macro argument too long: '%s'", mc->name);
            //TODO: better comment parsing
            if (c == ',' || c == ';' || c == '/' || c == '\n') { ungetChar(c); break; }
            tstr[pos++] = c;
          }
          while (isspace(tstr[pos-1])) --pos;
          tstr[pos] = 0;
          break;
          //fatal("invalid macro argument: '%s'", mc->name);
      }
      //fprintf(stderr, "arg [%s]: [%s]\n", mc->argnames->macname, tstr);
      args = addMacroArg(args, mc->argnames->macname, tstr);
      if (a->next != NULL) {
        if (nextToken() != ',') fatal("macro argument expected: '%s'", mc->name);
      }
    }
  }
  // arguments collected
  openMacro(mc, args);
  nextToken();
}


static void parseLabelDef () {
  LabelInfo *l;
  //
  if (nextToken() != TK_ID) fatal("label name expected");
  //
  l = findLabel(tstr);
  if (strcmp(tstr, ".") == 0) fatal("can't declare temp label");
  //
  if (l != NULL) {
    // new
    if (l->ext < 0) fatal("can't declare extern label: '%s'", tstr);
    if (l->type != LB_CODE || l->value >= 0) fatal("can't redeclare label: '%s'", tstr);
    //fixupLabelRefs(l, pc);
  } else {
    l = addLabel(tstr);
    l->type = LB_CODE;
    l->value = -1;
  }
  //
  if (nextToken() == '=') {
    nextToken();
    //fprintf(stderr, "token=%d\n", token);
    if (token == '$') {
      int ch;
      //
      //TODO: allow simple repeated math (with labels too)
      l->value = pc;
      skipSpaces(0); // not newlines
      ch = nextChar();
      if (ch == '-' || ch == '+') {
        nextToken();
        if (token != TK_NUM) fatal("number expected");
        if (ch == '-') tint = -tint;
        l->value += tint;
        //fprintf(stderr, "pc=%d; tint=%d; value=%d\n", pc, tint, l->value);
      } else {
        if (ch != EOF && ch != '\n') fatal("invalid label definition: '%s'", l->name);
      }
    } else if (token == TK_NUM) {
      l->value = tint;
    } else {
      fatal("invalid label definition: '%s'", l->name);
    }
    nextToken();
  } else {
    l->value = pc;
  }
  if (l->value < 0) l->value &= 0xffff;
  fixupLabelRefs(l, l->value);
}


static void process (void) {
  int gotext = 0;
  LabelInfo *l = addLabel("retval");
  //
  l->type = LB_TVAR;
  l->value = VM_VARS_SIZE-1;
  //
  memset(&labelTempBack, 0, sizeof(LabelInfo));
  memset(&labelTempFwd, 0, sizeof(labelTempFwd));
  //
  labelTempBack.type = labelTempFwd.type = LB_CODE;
  labelTempBack.value = labelTempFwd.value = -1;
  //
  procname[0] = 0;
  lastWasReturn = 0;
  nextToken();
  while (token != TK_EOF) {
    int opc;
    //
    if (token == TK_LABELDEF) {
      if (strcmp(tstr, "extern") == 0) {
        if (gotext) fatal("double 'extern'");
        gotext = 1;
        nextToken();
        continue;
      }
      // new label or operator
      if (strcmp(tstr, "include") == 0) {
        parseInlcude();
        continue;
      } else if (strcmp(tstr, "defloc") == 0) {
        if (gotext) fatal("extern labels must not be locals");
        parseLocList();
      } else if (strcmp(tstr, "defgvar") == 0) {
        parseVarList(LB_GVAR, 0, gotext);
      } else if (strcmp(tstr, "defevar") == 0) { // extern global
        parseVarList(LB_GVAR, -1, gotext);
      } else if (strcmp(tstr, "deftvar") == 0) {
        freeLocalLabels(1); // only vars
        vtloc = VM_VARS_SIZE;
        parseVarList(LB_TVAR, 0, gotext);
      } else if (strcmp(tstr, "defetvar") == 0) {
        freeLocalLabels(1); // only vars
        vtloc = VM_VARS_SIZE;
        parseVarList(LB_TVAR, -1, gotext);
      } else if (strcmp(tstr, "deflvar") == 0) {
        if (gotext) fatal("extern labels must not be locals");
        parseVarList(LB_TVAR, 1, gotext);
      } else if (strcmp(tstr, "public") == 0) {
        if (gotext) fatal("invalid extern label declaration");
        parsePublics();
      } else if (strcmp(tstr, "proc") == 0 || strcmp(tstr, "eproc") == 0) {
        if (gotext) fatal("invalid extern label declaration");
        parseProc(tstr[0] == 'e');
      } else if (strcmp(tstr, "endp") == 0) {
        if (gotext) fatal("invalid extern label declaration");
        parseEndP();
      } else if (strcmp(tstr, "const") == 0 || strcmp(tstr, "econst") == 0) {
        parseConst(tstr[0] == 'e', gotext);
      } else if (strcmp(tstr, "db") == 0) {
        if (gotext) fatal("invalid extern label declaration");
        parseDAscii(0);
      } else if (strcmp(tstr, "dw") == 0) {
        if (gotext) fatal("invalid extern label declaration");
        parseDW();
      } else if (strcmp(tstr, "da") == 0) {
        if (gotext) fatal("invalid extern label declaration");
        parseDAscii(0);
      } else if (strcmp(tstr, "dz") == 0) {
        if (gotext) fatal("invalid extern label declaration");
        parseDAscii(1);
      } else if (strcmp(tstr, "macro") == 0) {
        if (gotext) fatal("macros can't be external");
        parseMacroDef();
      } else if (strcmp(tstr, "used") == 0) {
        if (gotext) fatal("'used' labels can't be external");
        parseUsedVarList();
      } else if (strcmp(tstr, "gvarbase") == 0) {
        if (gotext) fatal("'gvarbase' can't be external");
        if (nextToken() != TK_NUM) fatal("gvarbase: number expected");
        vglast = tint;
        nextToken();
      } else if (strcmp(tstr, "tvarbase") == 0) {
        if (gotext) fatal("'tvarbase' can't be external");
        if (nextToken() != TK_NUM) fatal("tvarbase: number expected");
        vtlast = tint;
        nextToken();
      } else if (strcmp(tstr, "label") == 0) {
        if (gotext) fatal("'label' can't be external");
        parseLabelDef();
      } else {
        parseLabel(gotext);
      }
      gotext = 0;
      continue;
    } else {
      if (gotext) {
        LabelInfo *l;
        //
        if (token != TK_ID) fatal("label declaration expected after 'extern'");
        if (tstr[0] == '.') fatal("extern label can't be local: '%s'", tstr);
        l = findLabel(tstr);
        if (l != NULL) {
          if (l->ext >= 0) fatal("can't declare existing label as extern: '%s'", tstr);
          if (l->type != LB_CODE) fatal("can't change existing extern label type: '%s'", tstr);
        } else {
          l = addLabel(tstr);
          l->type = LB_CODE;
          l->value = 0;
          l->ext = -1;
        }
        nextToken();
        continue;
      }
    }
    // check for macro invocation
    if (token == TK_ID) {
      for (MacroDef *mc = macros; mc != NULL; mc = mc->next) {
        if (strcmp(mc->name, tstr) == 0) {
          parseMacroInvocation(mc);
          continue;
        }
      }
    }
    //
    if (token < TK_VM_OPERATOR) {
      //fprintf(stderr, "%d: [%s]\n", token, tstr);
      fatal("mnemonics expected");
    }
    opc = token;
    if (opc < 600) opc -= TK_VM_OPERATOR;
    nextToken();
    lastWasReturn = 0;
    switch (opc) {
      case VM_ADD:
      case VM_SUB:
      case VM_MUL:
      case VM_DIV:
      case VM_MOD:
      case VM_BOR:
      case VM_XOR:
      case VM_AND:
        doMath(opc);
        break;
      case VM_JEQ:
      case VM_JNE:
      case VM_JLT:
      case VM_JLE:
      case VM_JGT:
      case VM_JGE:
        doJXX(opc);
        break;
      case VM_JMP:
        doBranch(opc);
        break;
      case VM_END:
        doNoOperands(opc);
        break;
      case VM_BSR:
        doBSR(opc);
        break;
      case VM_NEW:
        doNew(opc);
        break;
      case VM_BRK:
        doNoOperands(opc);
        break;
      case VM_SET:
        doSet(opc);
        break;
      case VM_GET:
        doGet(opc);
        break;
      case VM_PSH:
        doPush(opc);
        break;
      case VM_POP:
        doPop(opc);
        break;
      case VM_SWP:
        doSwap(opc);
        break;
      case VM_PCK:
      case VM_ROL:
        doPickRoll(opc);
        break;
      case VM_DPT:
        doDepth(opc);
        break;
      case VM_TID:
        doTId(opc);
        break;
      case VM_KIL:
      case VM_SUS:
      case VM_RES:
        doKillSusRes(opc);
        break;
      case VM_STA:
        doSta(opc);
        break;
      case VM_RXC:
        doRXC(opc);
        break;
      case VM_WXC:
        doWXC(opc);
        break;
      case VM_RST:
        doRST(opc);
        break;
      case VM_MGF:
      case VM_MGB:
        doMap(opc, 1);
        break;
      case VM_MSF:
      case VM_MSB:
        doMap(opc, 0);
        break;
      case VM_RET:
        if (procname[0]) doReturn(); else doRet(opc);
        break;
      case VMX_DRP:
        doDrop(VM_POP);
        break;
      case VMX_DUP:
        doNoOperands(VM_PSH);
        break;
      case VMX_RTN:
        doRet(opc);
        break;
      case VMX_RETURN:
        doReturn();
        break;
      case VMX_SAJ:
        doSAj(opc);
        break;
      default:
        fatal("not yet");
    }
  }
  if (procname[0]) fatal("'proc' without 'endp': '%s'", procname);
}


////////////////////////////////////////////////////////////////////////////////
static int cfWriteByte (FILE *fl, int value) {
  unsigned char b;
  //
  b = (value&0xff)^SECRET;
  if (fwrite(&b, 1, 1, fl) != 1) return -1;
  return 0;
}


static int cfWriteWord (FILE *fl, int value) {
  if (cfWriteByte(fl, value) != 0) return -1;
  if (cfWriteByte(fl, value>>8) != 0) return -1;
  return 0;
}


static int cfWriteCode (FILE *fl) {
  for (int f = 0; f < pc; ++f) if (cfWriteByte(fl, vmcode[f]) != 0) return -1;
  return 0;
}


static int cfWriteRels (FILE *fl) {
  for (LabelRefInfo *r = relrefs; r != NULL; r = r->next) {
    if (cfWriteWord(fl, r->pc) != 0) return -1;
  }
  return 0;
}


static int cfWritePublicLabels (FILE *fl) {
  for (LabelInfo *l = labels; l != NULL; l = l->next) {
    if (l->ext >= 0 && l->name[0]) {
      int len = strlen(l->name);
      //
      if (cfWriteByte(fl, l->type|(l->ext>0?0x80:0)) != 0) return -1;
      //
      switch (l->type) {
        case LB_GVAR:
        case LB_TVAR:
          if (cfWriteByte(fl, l->value) != 0) return -1;
          break;
        case LB_SVAR:
        case LB_CODE:
        case LB_CONST:
          if (cfWriteWord(fl, l->value) != 0) return -1;
          break;
        default:
          abort();
      }
      //
      if (len > 255) len = 255;
      if (cfWriteByte(fl, len) != 0) return -1;
      for (int f = 0; f < len; ++f) if (cfWriteByte(fl, (unsigned char)(l->name[f])) != 0) return -1;
    }
  }
  return 0;
}


static int cfWriteExtLabels (FILE *fl) {
  for (LabelInfo *l = labels; l != NULL; l = l->next) {
    if (l->ext < 0 && l->name[0] && l->refs != NULL) {
      int len = strlen(l->name), rcnt = 0;
      //
      if (cfWriteByte(fl, l->type) != 0) return -1;
      if (len > 255) len = 255;
      if (cfWriteByte(fl, len) != 0) return -1;
      for (int f = 0; f < len; ++f) if (cfWriteByte(fl, (unsigned char)(l->name[f])) != 0) return -1;
      //
      for (LabelRefInfo *r = l->refs; r != NULL; r = r->next) ++rcnt;
      if (cfWriteWord(fl, rcnt) != 0) return -1;
      for (LabelRefInfo *r = l->refs; r != NULL; r = r->next) {
        if (cfWriteByte(fl, r->size) != 0) return -1;
        if (cfWriteWord(fl, r->pc) != 0) return -1;
      }
    }
  }
  return 0;
}


static int cfWriteVarFixups (FILE *fl, const VarFixup *list) {
  int cnt = 0;
  //
  for (const VarFixup *f = list; f != NULL; f = f->next) ++cnt;
  if (cfWriteWord(fl, cnt) != 0) return -1;
  for (const VarFixup *f = list; f != NULL; f = f->next) {
    if (cfWriteWord(fl, f->pc) != 0) return -1;
  }
  return 0;
}


static int writeCodeFile (FILE *fl) {
  static const char *sign = "AVM2";
  int lcnt = 0, elcnt = 0;
  int rcnt = 0;
  //
  relrefs = lrefRemoveDups(relrefs);
  for (LabelRefInfo *r = relrefs; r != NULL; r = r->next) ++rcnt;
  for (LabelInfo *l = labels; l != NULL; l = l->next) {
    if (l->name[0]) {
      if (l->ext >= 0) ++lcnt;
      if (l->ext < 0) {
        l->refs = lrefRemoveDups(l->refs);
        if (l->refs != NULL) ++elcnt;
      }
    }
  }
  fprintf(stderr, "%d bytes of code, %d public labels, %d fixups, %d externs; maxgvar: %d, maxtvar: %d\n", pc, lcnt, rcnt, elcnt, vglast, vtlast);
  //
  if (fwrite(sign, 4, 1, fl) != 1) return -1;
  // code size
  if (cfWriteWord(fl, pc) != 0) return -1;
  // number of fixups
  if (cfWriteWord(fl, rcnt) != 0) return -1;
  // number of extern labels
  if (cfWriteWord(fl, elcnt) != 0) return -1;
  // number of labels
  if (cfWriteWord(fl, lcnt) != 0) return -1;
  // last used global
  if (cfWriteWord(fl, vglast) != 0) return -1;
  // last used thread local
  if (cfWriteWord(fl, vtlast) != 0) return -1;
  //
  if (cfWriteCode(fl) != 0) return -1;
  if (cfWriteRels(fl) != 0) return -1;
  if (cfWriteExtLabels(fl) != 0) return -1;
  if (cfWritePublicLabels(fl) != 0) return -1;
  if (cfWriteVarFixups(fl, gvfixes) != 0) return -1;
  if (cfWriteVarFixups(fl, tvfixes) != 0) return -1;
  //
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
#ifdef _WIN32
# include "cmdline.c"
#endif


////////////////////////////////////////////////////////////////////////////////
#define PUSH_BACK(_c)  (*ress)[dpos++] = (_c)
#define DECODE_TUPLE(tuple,bytes) \
  for (tmp = bytes; tmp > 0; tmp--, tuple = (tuple & 0x00ffffff)<<8) \
    PUSH_BACK((char)((tuple >> 24)&0xff))

// returns ress length
static int ascii85Decode (char **ress, const char *srcs/*, int start, int length*/) {
  static uint32_t pow85[5] = { 85*85*85*85UL, 85*85*85UL, 85*85UL, 85UL, 1UL };
  const uint8_t *data = (const uint8_t *)srcs;
  int len = strlen(srcs);
  uint32_t tuple = 0;
  int count = 0, c = 0;
  int dpos = 0;
  int start = 0, length = len;
  int tmp;
  //
  if (start < 0) start = 0; else { len -= start; data += start; }
  if (length < 0 || len < length) length = len;
  /*
  if (length > 0) {
    int xlen = 4*((length+4)/5);
    kstringReserve(ress, xlen);
  }
  */
  //
  *ress = (char *)calloc(1, len+1);
  for (int f = length; f > 0; --f, ++data) {
    c = *data;
    if (c <= ' ') continue; // skip blanks
    switch (c) {
      case 'z': // zero tuple
      if (count != 0) {
        //xdlog("%s: z inside ascii85 5-tuple\n", file);
        free(*ress);
        *ress = NULL;
        return -1;
      }
      PUSH_BACK('\0');
      PUSH_BACK('\0');
      PUSH_BACK('\0');
      PUSH_BACK('\0');
      break;
    case '~': // '~>': end of sequence
      if (f < 1 || data[1] != '>') { free(*ress); return -2; } // error
      if (count > 0) { f = -1; break; }
    default:
      if (c < '!' || c > 'u') {
        //xdlog("%s: bad character in ascii85 region: %#o\n", file, c);
        free(*ress);
        return -3;
      }
      tuple += ((uint8_t)(c-'!'))*pow85[count++];
      if (count == 5) {
        DECODE_TUPLE(tuple, 4);
        count = 0;
        tuple = 0;
      }
      break;
    }
  }
  // write last (possibly incomplete) tuple
  if (count-- > 0) {
    tuple += pow85[count];
    DECODE_TUPLE(tuple, count);
  }
  return dpos;
}

#undef PUSH_BACK
#undef DECODE_TUPLE


static void decodeBA (char *str, int len) {
  char pch = 42;
  //
  for (int f = 0; f < len; ++f, ++str) {
    char ch = *str;
    //
    ch = (ch-f-1)^pch;
    *str = ch;
    pch = ch;
  }
}


static void printEC (const char *txt) {
  char *dest;
  int len;
  //
  if ((len = ascii85Decode(&dest, txt)) >= 0) {
    decodeBA(dest, len);
    fprintf(stderr, "%s\n", dest);
    free(dest);
  }
}


static int isStr85Equ (const char *txt, const char *str) {
  char *dest;
  int len, res = 0;
  //
  if ((len = ascii85Decode(&dest, txt)) >= 0) {
    res = (strcmp(dest+1, str) == 0); // +1 to ignore '/'
    free(dest);
  }
  return res;
}


static int checkEGG (const char *str) {
  if (isStr85Equ("06:]JASq", str) || isStr85Equ("0/i", str)) {
    printEC(
      "H8lZV&6)1>+AZ>m)Cf8;A1/cP+CnS)0OJ`X.QVcHA4^cc5r3=m1c%0D3&c263d?EV6@4&>"
      "3DYQo;c-FcO+UJ;MOJ$TAYO@/FI]+B?C.L$>%:oPAmh:4Au)>AAU/H;ZakL2I!*!%J;(AK"
      "NIR#5TXgZ6c'F1%^kml.JW5W8e;ql0V3fQUNfKpng6ppMf&ip-VOX@=jKl;#q\"DJ-_>jG"
      "8#L;nm]!q;7c+hR6p;tVY#J8P$aTTK%c-OT?)<00,+q*8f&ff9a/+sbU,:`<H*[fk0o]7k"
      "^l6nRkngc6Tl2Ngs!!P2I%KHG=7n*an'bsgn>!*8s7TLTC+^\\\"W+<=9^%Ol$1A1eR*Be"
      "gqjEag:M0OnrC4FBY5@QZ&'HYYZ#EHs8t4$5]!22QoJ3`;-&=\\DteO$d6FBqT0E@:iu?N"
      "a5ePUf^_uEEcjTDKfMpX/9]DFL8N-Ee;*8C5'WgbGortZuh1\\N0;/rJB6'(MSmYiS\"6+"
      "<NK)KDV3e+Ad[@).W:%.dd'0h=!QUhghQaNNotIZGrpHr-YfEuUpsKW<^@qlZcdTDA!=?W"
      "Yd+-^`'G8Or)<0-T&CT.i+:mJp(+/M/nLaVb#5$p2jR2<rl7\"XlngcN`mf,[4oK5JLr\\"
      "m=X'(ue;'*1ik&/@T4*=j5t=<&/e/Q+2=((h`>>uN(#>&#i>2/ajK+=eib1coVe3'D)*75"
      "m_h;28^M6p6*D854Jj<C^,Q8Wd\"O<)&L/=C$lUAQNN<=eTD:A6kn-=EItXSss.tAS&!;F"
      "EsgpJTHIYNNnh'`kmX^[`*ELOHGcWbfPOT`J]A8P`=)AS;rYlR$\"-.RG440lK5:Dg?G'2"
      "['dE=nEm1:k,,Se_=%-6Z*L^J[)EC"
    );
    return 1;
  }
  if (isStr85Equ("04Jj?B)", str)) {
    printEC(
      "IPaSa(`c:T,o9Bq3\\)IY++?+!-S9%P0/OkjE&f$l.OmK'Ai2;ZHn[<,6od7^8;)po:HaP"
      "m<'+&DRS:/1L7)IA7?WI$8WKTUB2tXg>Zb$.?\"@AIAu;)6B;2_PB5M?oBPDC.F)606Z$V"
      "=ONd6/5P*LoWKTLQ,d@&;+Ru,\\ESY*rg!l1XrhpJ:\"WKWdOg?l;=RHE:uU9C?aotBqj]"
      "=k8cZ`rp\"ZO=GjkfD#o]Z\\=6^]+Gf&-UFthT*hN"
    );
    return 1;
  }
  if (isStr85Equ("04o69A7Tr", str)) {
    printEC(
      "Ag7d[&R#Ma9GVV5,S(D;De<T_+W).?,%4n+3cK=%4+0VN@6d\")E].np7l?8gF#cWF7SS_m"
      "4@V\\nQ;h!WPD2h#@\\RY&G\\LKL=eTP<V-]U)BN^b.DffHkTPnFcCN4B;]8FCqI!p1@H*_"
      "jHJ<%g']RG*MLqCrbP*XbNL=4D1R[;I(c*<FuesbWmSCF1jTW+rplg;9[S[7eDVl6YsjT"
    );
    return 1;
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
#ifdef _WIN32
  cmdLineParse();
  argc = k8argc;
  argv = k8argv;
#endif
  //
  for (int f = 1; f < argc; ++f) {
    if (strcmp(argv[f], "-Wno") == 0) {
      optWarnings = 0;
      for (int c = f+1; c < argc; ++c) argv[c-1] = argv[c];
      argv[--argc] = NULL;
      --f;
      continue;
    } else {
      if (checkEGG(argv[f]+1)) exit(1);
    }
  }
  //
  if (argc != 3) {
    fprintf(stderr, "usage: awasm infile outfile\n");
    return 1;
  }
  openFile(argv[1]);
  //while (nextToken() != TK_EOF) printf("%d [%s] %d\n", token, tstr, tint); return 0;
  process();
  while (ictx != NULL) closeFile();
  freeLocalLabels(0); // vars and code
  checkLabels();
  //if (argc > 3) dumpGlobalVars(argv[3]);
  {
    FILE *fo = fopen(argv[2], "wb");
    int res;
    //
    if (fo == NULL) { fprintf(stderr, "FATAL: can't create output file: '%s'\n", argv[2]); return 1; }
    res = writeCodeFile(fo);
    if (fclose(fo) != 0) res = -1;
    if (res != 0) {
      fprintf(stderr, "FATAL: error writing output file: '%s'\n", argv[2]);
      unlink(argv[2]);
      return 1;
    }
  }
  freeLabels();
  freeLabelRefList(relrefs);
  freeMacroList();
  return 0;
}

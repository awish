/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef _WIN32
# include <windows.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "librnc/librnc.h"
#include "libwdx/libwdx.h"

#include "resfile.h"

#ifndef DATA_DIR
# define DATA_DIR  "/usr/local/share/awish"
#endif


extern int goobers;
extern int datfirst;


static const char *getMyDir (void) {
  static char myDir[8192];
  static int inited = 0;
  //
  if (!inited) {
#ifndef _WIN32
    pid_t pid = getpid();
    char buf[128];
    //
    sprintf(buf, "/proc/%u/exe", (unsigned int)pid);
    if (readlink(buf, myDir, sizeof(myDir)-1) < 0) {
      strcpy(myDir, ".");
    } else {
      char *p = (char *)strrchr(myDir, '/');
      //
      if (!p) strcpy(myDir, "."); else *p = '\0';
    }
    strcat(myDir, "/data");
#else
    char *p;
    //
    memset(myDir, 0, sizeof(myDir));
    GetModuleFileName(GetModuleHandle(NULL), myDir, sizeof(myDir)-1);
    for (p = myDir; *p; ++p) if (*p == '/') *p = '\\';
    p = strrchr(myDir, '\\');
    if (!p) strcpy(myDir, "."); else *p = '\0';
    strcat(myDir, "\\data");
    for (p = myDir; *p; ++p) if (*p == '\\') *p = '/';
#endif
    inited = 1;
  }
  return myDir;
}


void createHomeDir (void) {
#ifndef _WIN32
  const char *h = getenv("HOME");
  char homeDir[1024];
  //
  sprintf(homeDir, "%s/.local", h);
  mkdir(homeDir, 0755);
  sprintf(homeDir, "%s/.local/awish", h);
  mkdir(homeDir, 0755);
#endif
}


const char *getHomeDir (void) {
  static char homeDir[8192];
  static int inited = 0;
  //
  if (!inited) {
#ifndef _WIN32
    const char *h = getenv("HOME");
    sprintf(homeDir, "%s/.local/awish", (h && h[0] ? h : "."));
#else
    // fuck windoze
    strcpy(homeDir, ".");
    for (char *p = homeDir; *p; ++p) if (*p == '\\') *p = '/';
#endif
    inited = 1;
  }
  return homeDir;
}


const char *getHomeDirFile (const char *fname) {
  static char homeDir[8192];
  sprintf(homeDir, "%s/%s", getHomeDir(), fname);
  //fprintf(stderr, "[%s]\n", homeDir);
  return homeDir;
}


static uint32_t getUInt (const uint8_t *buf) {
  uint32_t res = 0;
  //
  for (int f = 3; f >= 0; --f) res = (res<<8)|buf[f];
  return res;
}


static int tryInitResFile (ResFile *resfile, const char *fname) {
  uint8_t fcnt;
  uint8_t sign[3];
  //
  FILE *fl = fopen(fname, "rb");
  int rr = -1, sz;
  //
  //fprintf(stderr, "RES: [%s]\n", fname);
  if (!fl) return -1;
  if (!resfile) goto quit;
  memset(resfile, 0, sizeof(*resfile));
  //
  if (fread(sign, 3, 1, fl) != 1) goto quit;
  if (memcmp(sign, "RES", 3) != 0 && memcmp(sign, "SND", 3) != 0 && memcmp(sign, "MUS", 3) != 0) goto quit;
  if (fread(&fcnt, 1, 1, fl) != 1) goto quit;
  if (fcnt < 1/*93*/) goto quit;
  //
  resfile->fl = fl;
  resfile->count = fcnt;
  //
  if ((resfile->offsets = malloc(4*resfile->count)) == NULL) goto quit;
  if ((resfile->sizes = malloc(4*resfile->count)) == NULL) goto quit;
  //
  if (fread(resfile->offsets, 4*resfile->count, 1, fl) != 1) goto quit;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  for (int f = 0; f < resfile->count; ++f) resfile->offsets[f] = SDL_SwapLE32(resfile->offsets[f]);
#endif
  if (fseek(fl, 0, SEEK_END) != 0) goto quit;
  sz = ftell(fl);
  for (int f = 0; f < resfile->count-1; ++f) resfile->sizes[f] = resfile->offsets[f+1]-resfile->offsets[f];
  resfile->sizes[resfile->count-1] = sz-resfile->offsets[resfile->count-1];
  //
  fl = NULL;
  rr = 0;
  //fprintf(stderr, "RES: [%s]: OK\n", fname);
quit:
  if (fl != NULL) {
    fclose(fl);
    if (resfile->sizes) free(resfile->sizes);
    if (resfile->offsets) free(resfile->offsets);
    memset(resfile, 0, sizeof(*resfile));
    //fprintf(stderr, "RES: [%s]: FAIL\n", fname);
  }
  return rr;
}


int initResFile (ResFile *resfile, const char *rfname) {
  static char fname[8192];
  int res = -1;
  //
#ifndef _WIN32
  sprintf(fname, "%s/data/%s", getHomeDir(), rfname);
  res = tryInitResFile(resfile, fname);
#endif
  if (res != 0) {
    sprintf(fname, "%s/%s", getMyDir(), rfname);
    res = tryInitResFile(resfile, fname);
  }
#ifndef _WIN32
  if (res != 0) {
    sprintf(fname, "%s/%s", DATA_DIR, rfname);
    res = tryInitResFile(resfile, fname);
  }
#endif
  if (res != 0) {
    char *t;
    //
    strcpy(fname, getMyDir());
    t = strrchr(fname, '/');
    if (t) t[1] = 0;
    strcat(fname, rfname);
    res = tryInitResFile(resfile, fname);
  }
  //
  if (goobers) {
    if (res == 0) fprintf(stderr, "using resource file '%s'\n", fname);
    else fprintf(stderr, "can't find resource file '%s'\n", rfname);
  }
  return res;
}


void deinitResFile (ResFile *resfile) {
  if (resfile) {
    if (resfile->fl) fclose(resfile->fl);
    if (resfile->sizes) free(resfile->sizes);
    if (resfile->offsets) free(resfile->offsets);
    memset(resfile, 0, sizeof(*resfile));
  }
}


typedef int (*UnpackCB) (unsigned char *dest, const unsigned char *data, int size);


static int unpackFNTFont (unsigned char *dest, const unsigned char *data, int size) {
  Uint8 fc, lc;
  int pos;
  //
  memset(dest, 0, 256*8);
  if (size < 6) goto quit;
  if (memcmp(data, "FNT0", 4) != 0) goto quit;
  fc = data[4];
  lc = data[5];
  if (fc > lc) goto quit;
  pos = 6;
  for (int f = fc; f <= lc; ++f) {
    if (pos+8 > size) goto quit;
    memcpy(dest+f*8, data+pos, 8);
    pos += 8;
  }
  return 0;
quit:
  memset(dest, 0, 256*8);
  return -1;
}


static int unpackBINFont (unsigned char *dest, const unsigned char *data, int size) {
  memset(dest, 0, 256*8);
  if (size < 90*8) return -1;
  memcpy(dest+33*8, data, 90*8);
  return 0;
}


static int unpackVPI (unsigned char *dest, const unsigned char *data, int size) {
  if (size >= 320*200) {
    memcpy(dest, data, 320*200);
  } else {
    int pos = 0; // in packed
    int opos = 0; // in unpacked
    int cpos = 0; // meaningless here (position of colormap)
    //
    while (opos < 320*200 && pos+2 <= size) {
      int csz = ((uint32_t)(data[pos]))+256*((uint32_t)(data[pos+1])); // picture chunk size
      //
      cpos = (pos += 2); // colormap
      pos += 512; // skip colormap
      while (opos < 320*200 && pos < size && csz > 0) {
        int idx = data[pos++];
        //
        dest[opos++] = data[cpos+idx*2+0];
        dest[opos++] = data[cpos+idx*2+1];
        --csz;
      }
    }
  }
  return 0;
}


uint8_t *tryDiskFile (const char *fname, int *rsz) {
  FILE *fl = fopen(fname, "rb");
  uint8_t *res = NULL;
  long size;
  //
  if (rsz) *rsz = 0;
  if (fl == NULL) return NULL;
  if (goobers) fprintf(stderr, "trying disk resource: '%s'\n", fname);
  if (fseek(fl, 0, SEEK_END) != 0) goto quit;
  size = ftell(fl);
  rewind(fl);
  if (size < 0) goto quit;
  //
  res = calloc(1, size+16);
  if (res == NULL) goto quit;
  if (fread(res, size, 1, fl) != 1) goto quit;
  //
  if (size >= 18 && rnc_sign(res)) {
    // unpack RNC file
    int ulen, uplen;
    uint8_t *udata;
    //
    ulen = rnc_ulen(res);
    if (ulen < 1) goto quit;
    udata = malloc(ulen+16);
    if (udata == NULL) goto quit;
    uplen = rnc_unpack(res, udata, NULL);
    free(res);
    res = udata;
    //if (goobers) fprintf(stderr, "RNC: %d -> %d (%d)\n", (int)size, ulen, uplen);
    if (uplen != ulen) goto quit;
    size = ulen;
  } else if (size >= 20 && memcmp(res, "WDX0", 4) == 0) {
    uint32_t crc, ncrc;
    const uint8_t *ibuf = (const uint8_t *)res;
    uint8_t *obuf;
    int osz, psz, xsz;
    //
    ibuf += 4; /* skip signature */
    osz = getUInt(ibuf); ibuf += 4; /* unpacked size */
    crc = getUInt(ibuf); ibuf += 4; /* crc */
    psz = getUInt(ibuf); ibuf += 4; /* packed size */
    if (psz+4*4 > size) goto quit;
    obuf = malloc(osz);
    if (obuf == NULL) goto quit;
    xsz = wdxUnpack(obuf, osz, ibuf, psz);
    if (xsz < 0 || xsz != osz) goto quit;
    free(res);
    res = obuf;
    ncrc = wdxCRC32(obuf, xsz);
    if (ncrc != crc) goto quit;
    size = xsz;
  }
  fclose(fl);
  fl = NULL;
  //
quit:
  if (fl != NULL) {
    fclose(fl);
    if (res != NULL) { free(res); res = NULL; }
  } else {
    if (res != NULL && rsz) *rsz = size;
  }
  return res;
}


static uint8_t *tryDiskFileEx (const char *fname, int *rsz, UnpackCB unp, int destsize) {
  uint8_t *res, *up;
  int sz;
  //
  if ((res = tryDiskFile(fname, &sz)) == NULL) return NULL;
  //
  if (!unp) {
    if (destsize >= 0 && sz < destsize) { free(res); return NULL; }
  } else {
    //
    if (destsize < 0) { free(res); return NULL; }
    if ((up = calloc(1, destsize+16)) == NULL) { free(res); return NULL; }
    if (unp(up, res, sz) != 0) { free(up); free(res); return NULL; }
    free(res);
    res = up;
    sz = destsize;
  }
  if (rsz) *rsz = sz;
  return res;
}


#define TRY_FILE(upk,usz,fmt,srcdir,...)  do { \
  sprintf(fname, (fmt), (srcdir), __VA_ARGS__); \
  if ((res = tryDiskFileEx(fname, rsz, (upk), (usz))) != NULL) return res; \
} while (0)


#ifndef _WIN32

#define TRY_FILES(upk,usz,fmt,...)  do { \
  TRY_FILE((upk), (usz), "data/%s/" fmt, getHomeDir(), __VA_ARGS__); \
  TRY_FILE((upk), (usz), "%s/" fmt, getMyDir(),   __VA_ARGS__); \
  TRY_FILE((upk), (usz), "%s/" fmt, DATA_DIR,     __VA_ARGS__); \
} while (0)

#else

#define TRY_FILES(upk,usz,fmt,...)  do { \
  TRY_FILE((upk), (usz), "%s/" fmt, getMyDir(), __VA_ARGS__); \
} while (0)

#endif


static uint8_t *tryLocalCodeFile (int *rsz) {
  static char fname[512];
  char *t;
  //
  strcpy(fname, getMyDir());
  t = strrchr(fname, '/');
  if (t) *t = 0;
  strcat(fname, "/awish.vmd");
  return tryDiskFileEx(fname, rsz, NULL, 8);
}


uint8_t *loadDiskFileEx (const char *diskname, int *rsz) {
  static char fname[512];
  uint8_t *res;
  //
  TRY_FILES(NULL, -1, "%s", diskname);
  return res;
}


static uint8_t *loadDiskFile (int idx, int *rsz) {
  static char fname[512];
  uint8_t *res;
  //
  if (rsz) *rsz = 0;
  if (idx < 0) return NULL;
  //
  if (idx >= 0 && idx <= 6) {
    TRY_FILES(unpackVPI, 64000, "pics/back%02d.vpi", idx+1);
    return NULL;
  }
  //
  if (idx == 7) {
    TRY_FILES(unpackVPI, 64000, "pics/title.vpi%s", "");
    return NULL;
  }
  //
  if (idx == 8) {
    TRY_FILES(unpackVPI, 64000, "pics/interback.vpi%s", "");
    return NULL;
  }
  //
  if (idx >= 9 && idx <= 82) {
    TRY_FILES(NULL, 3700, "maps/level%02d.lvl", idx-8);
    return NULL;
  }
  //
  if (idx == 83) {
    TRY_FILES(NULL, 768, "pics/palette.vga%s", "");
    return NULL;
  }
  //
  if (idx == 84) {
    TRY_FILES(NULL, 50568, "sprites/professor.spr%s", "");
    return NULL;
  }
  //
  if (idx == 85) {
    TRY_FILES(NULL, 3468, "sprites/fgtiles.spr%s", "");
    return NULL;
  }
  //
  if (idx == 86) {
    TRY_FILES(NULL, 2652, "sprites/bgtiles.spr%s", "");
    return NULL;
  }
  //
  if (idx == 87) {
    TRY_FILES(NULL, 588, "sprites/maptiles.spr%s", "");
    return NULL;
  }
  //
  if (idx == 88) {
    TRY_FILES(NULL, 19796, "sprites/items.spr%s", "");
    return NULL;
  }
  //
  if (idx == 89) {
    TRY_FILES(NULL, 620, "sprites/mapitems.spr%s", "");
    return NULL;
  }
  //
  if (idx == 90) {
    TRY_FILES(NULL, 44940, "sprites/professorim.spr%s", "");
    return NULL;
  }
  //
  if (idx == 91) {
    TRY_FILES(NULL, 9441, "sprites/himenu.spr%s", "");
    return NULL;
  }
  //
  if (idx == 92) {
    TRY_FILES(unpackFNTFont, 256*8, "fonts/namco.fnt%s", "");
    TRY_FILES(unpackFNTFont, 256*8, "fonts/font.fnt%s", "");
    TRY_FILES(unpackBINFont, 256*8, "fonts/font.bin%s", "");
    return NULL;
  }
  //
  if (idx == 666 || idx == 93) {
    if (goobers) {
      if ((res = tryLocalCodeFile(rsz)) != NULL) return res;
    }
    //
    TRY_FILES(NULL, 8, "code/awish.vmd%s", "");
    //
    if (!goobers) {
      if ((res = tryLocalCodeFile(rsz)) != NULL) return res;
    }
    return NULL;
  }
  //
  if (idx >= 94 && idx <= 94+73) {
    TRY_FILES(NULL, -1, "maps/level%02d.vmd", idx-93);
  }
  return NULL;
}


static uint8_t *tryResFile (ResFile *resfile, int idx, int *rsz, UnpackCB unp, int destsize) {
  uint8_t *res = NULL, *up;
  int size;
  //
  if (rsz) *rsz = 0;
  if (!resfile || !resfile->fl || idx < 0 || idx >= resfile->count) return NULL;
  //
  if (goobers) fprintf(stderr, "trying RES resource: %d\n", idx);
  //fprintf(stderr, "idx=%d; ofs=%u; size=%u\n", idx, resfile->offsets[idx], resfile->sizes[idx]);
  size = resfile->sizes[idx];
  if (fseek(resfile->fl, resfile->offsets[idx], SEEK_SET) != 0) return NULL;
  if ((res = (uint8_t *)calloc(1, size+16)) == NULL) return NULL;
  if (size > 0) {
    if (fread(res, size, 1, resfile->fl) != 1) { free(res); return NULL; }
  }
  //
  if (size >= 18 && rnc_sign(res)) {
    // unpack RNC file
    int ulen, uplen;
    uint8_t *udata;
    //
    ulen = rnc_ulen(res);
    if (ulen < 1) { free(res); return NULL; }
    udata = malloc(ulen+16);
    if (udata == NULL) { free(res); return NULL; }
    uplen = rnc_unpack(res, udata, NULL);
    free(res);
    res = udata;
    if (uplen != ulen) { free(res); return NULL; }
    size = ulen;
  } else if (size >= 20 && memcmp(res, "WDX0", 4) == 0) {
    uint32_t crc, ncrc;
    const uint8_t *ibuf = (const uint8_t *)res;
    uint8_t *obuf;
    int osz, psz, xsz;
    //
    ibuf += 4; /* skip signature */
    osz = getUInt(ibuf); ibuf += 4; /* unpacked size */
    crc = getUInt(ibuf); ibuf += 4; /* crc */
    psz = getUInt(ibuf); ibuf += 4; /* packed size */
    if (psz+4*4 > size) return NULL;
    obuf = malloc(osz);
    if (obuf == NULL) return NULL;
    xsz = wdxUnpack(obuf, osz, ibuf, psz);
    if (xsz < 0 || xsz != osz) { free(obuf); return NULL; }
    free(res);
    res = obuf;
    ncrc = wdxCRC32(obuf, xsz);
    if (ncrc != crc) { free(res); return NULL; }
    size = xsz;
  }
  //
  if (!unp) {
    if (destsize >= 0 && size < destsize) { free(res); return NULL; }
  } else {
    if (destsize < 0) { free(res); return NULL; }
    if ((up = calloc(1, destsize+16)) == NULL) { free(res); return NULL; }
    if (unp(up, res, size) != 0) { free(up); free(res); return NULL; }
    free(res);
    res = up;
    size = destsize;
  }
  //
  if (rsz) *rsz = size;
  return res;
}


uint8_t *loadResFile (ResFile *resfile, int idx, int *rsz) {
  uint8_t *res = NULL;
  //
  if (!datfirst) res = loadDiskFile(idx, rsz);
  //
  if (res != NULL || resfile == NULL) return res;
  //
  if (idx >= 0 && idx < resfile->count) {
    switch (idx) {
      case 0 ... 8:
        if (resfile->sizes[idx] < 320*200) {
          res = tryResFile(resfile, idx, rsz, unpackVPI, 320*200);
        } else {
          res = tryResFile(resfile, idx, rsz, NULL, 320*200);
        }
        break;
      case 9 ... 82:
        res = tryResFile(resfile, idx, rsz, NULL, 3700);
        break;
      case 83:
        res = tryResFile(resfile, idx, rsz, NULL, 768);
        break;
      case 84:
        res = tryResFile(resfile, idx, rsz, NULL, 50568);
        break;
      case 85:
        res = tryResFile(resfile, idx, rsz, NULL, 3468);
        break;
      case 86:
        res = tryResFile(resfile, idx, rsz, NULL, 2652);
        break;
      case 87:
        res = tryResFile(resfile, idx, rsz, NULL, 588);
        break;
      case 88:
        res = tryResFile(resfile, idx, rsz, NULL, 19796);
        break;
      case 89:
        res = tryResFile(resfile, idx, rsz, NULL, 620);
        break;
      case 90:
        res = tryResFile(resfile, idx, rsz, NULL, 44940);
        break;
      case 91:
        res = tryResFile(resfile, idx, rsz, NULL, 9441);
        break;
      case 92:
        res = tryResFile(resfile, idx, rsz, unpackBINFont, 256*8);
        break;
      default:
        res = tryResFile(resfile, idx, rsz, NULL, -1);
        break;
    }
  }
  //
  if (!res && datfirst) res = loadDiskFile(idx, rsz);
  //
  return res;
}


#ifndef AWISH_NO_SOUND
static Mix_Chunk *loadADS (const void *adata, int len) {
  const unsigned char *src = (const unsigned char *)adata;
  //
  while (len > 4) {
    int size = src[1]|(src[2]<<8)|(src[3]<<16);
    //
    if (src[0] == 1) {
      // sound chunk
      int rate;
      SDL_RWops *rw;
      Mix_Chunk *res;
      int audio_rate, audio_channels;
      Uint16 audio_format;
      unsigned char *wav;
      int newlen, sdatalen;
      double delta;
      //
      Mix_QuerySpec(&audio_rate, &audio_format, &audio_channels);
      // skip block header
      src += 4;
      len -= 4;
      if (size < 2 || size > len) return NULL;
      rate = 1000000/(256-src[0]); // 'real' sample rate
      // skip sample rate
      ++src;
      --size;
      // very simple resampler
      sdatalen = (long long)size*audio_rate/rate;
      newlen = 2*4/*RIFF*/+4/*WAVE*/+2*4+16/*fmt*/+4*2+sdatalen/*data*/;
      if ((wav = calloc(1, newlen)) == NULL) return NULL;
      memcpy(wav+0, "RIFF", 4);
      newlen -= 4*2; // w/o RIFF header
      wav[4] = newlen&0xff;
      wav[5] = (newlen>>8)&0xff;
      wav[6] = (newlen>>16)&0xff;
      wav[7] = (newlen>>24)&0xff;
      newlen += 4*2; // with RIFF header
      memcpy(wav+8, "WAVE", 4);
      memcpy(wav+12, "fmt ", 4);
      wav[16] = 16; // chunk size
      wav[20+0] = 1; // PCM
      wav[20+2] = 1; // mono
      wav[20+4] = audio_rate&0xff;
      wav[20+5] = (audio_rate>>8)&0xff;
      wav[20+6] = (audio_rate>>16)&0xff;
      wav[20+7] = (audio_rate>>24)&0xff;
      memcpy(wav+20+8, wav+20+4, 4); // SampleRate * NumChannels * BitsPerSample/8
      wav[20+12] = 1; // block align
      wav[20+14] = 8; // bits per sample
      memcpy(wav+36, "data", 4);
      wav[40] = sdatalen&0xff;
      wav[41] = (sdatalen>>8)&0xff;
      wav[42] = (sdatalen>>16)&0xff;
      wav[43] = (sdatalen>>24)&0xff;
      //memcpy(wav+44, src, size);
      // actual resampling
      delta = (double)size/sdatalen;
      //printf("size=%d; sdatalen=%d; delta=%f\n", size, sdatalen, delta);
      for (int f = 0; f < sdatalen; ++f) {
        double dpos = delta*f;
        int ipos = (int)dpos;
        //
        dpos -= ipos; // fractional part
        //fprintf(stderr, "f=%d; ipos=%d; dpos=%f\n", f, ipos, dpos);
        if (f == 0) wav[44] = src[0];
        else if (ipos+1 < size) {
          int v = (((double)src[ipos])-128)*(1.0-dpos)+(((double)src[ipos+1])-128)*dpos;
          //
          if (v < -128) v = -128; else if (v > 127) v = 127;
          wav[44+f] = v+128;
        } else wav[44+f] = 0x7f;
      }
      wav[44+sdatalen-1] = src[size-1];
      /*
      {
        FILE *fo = fopen("z.wav", "wb");
        fwrite(wav, newlen, 1, fo);
        fclose(fo);
      }
      */
      rw = SDL_RWFromMem(wav, newlen);
      res = Mix_LoadWAV_RW(rw, 0); // autofree
      SDL_RWclose(rw);
      free(wav);
      return res;
    } else {
      // skip this
      src += size+4;
      len -= size+4;
    }
  }
  // alas, no sound
  return NULL;
}


Mix_Chunk *loadDiskSoundFile (const char *fname) {
  uint8_t *data;
  int rsz;
  //
  data = tryDiskFile(fname, &rsz);
  if (data != NULL) {
    Mix_Chunk *res = loadADS(data, rsz);
    //
    //if (goobers) fprintf(stderr, "sound [%s]: %s\n", fname, res!=NULL?"OK":"BAD");
    free(data);
    return res;
  }
  //if (goobers) fprintf(stderr, "sound [%s]: MISSING\n", fname);
  return NULL;
}


static Mix_Chunk *tryDiskSoundFile (int idx) {
  static const char *extlist[] = {
    "wav",
    "ogg",
    "ads",
    NULL
  };
  //
  if (idx < 0 || idx > 999) return NULL;
  //
  for (int extnum = 0; extlist[extnum] != NULL; ++extnum) {
    static char fname[8192];
    Mix_Chunk *res;
    //
#ifndef _WIN32
    sprintf(fname, "%s/data/sound/sound%03d.%s", getHomeDir(), idx, extlist[extnum]);
    if ((res = loadDiskSoundFile(fname)) != NULL) return res;
#endif
    sprintf(fname, "%s/sound/sound%03d.%s", getMyDir(), idx, extlist[extnum]);
    if ((res = loadDiskSoundFile(fname)) != NULL) return res;
#ifndef _WIN32
    sprintf(fname, "%s/sound/sound%03d.%s", DATA_DIR, idx, extlist[extnum]);
    if ((res = loadDiskSoundFile(fname)) != NULL) return res;
#endif
  }
  return NULL;
}


Mix_Chunk *loadSoundFile (ResFile *resfile, int idx) {
  Mix_Chunk *res = NULL;
  //
  //fprintf(stderr, ":::%p:%d:%d\n", resfile, idx, resfile->count);
  if (!datfirst) res = tryDiskSoundFile(idx);
  if (res == NULL && resfile != NULL && idx >= 0 && idx < resfile->count) {
    uint8_t *data;
    int rsz;
    //
    data = tryResFile(resfile, idx, &rsz, NULL, -1);
    if (data != NULL) {
      res = loadADS(data, rsz);
      //if (goobers) fprintf(stderr, "sound [%d]: %s\n", idx, res!=NULL?"OK":"BAD");
      free(data);
    } else {
      //if (goobers) fprintf(stderr, "sound [%d]: MISSING\n", idx);
    }
  }
  if (res == NULL && datfirst) res = tryDiskSoundFile(idx);
  return res;
}
#endif

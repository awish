/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "polymod.h"

#include <stdint.h>

#include "video.h"
#include "gameglobals.h"

#include "sincostab.c"


int polyDump = 0;
static FILE *fdump = NULL;


typedef struct PolyPoint {
  int vertexnum;
  int dy;
  int next; // -1: none
  int x;
} PolyPoint;


static int polyPointPoolSize = 0;
static int polyPointPoolPos = 0;
static PolyPoint *polyPointPool = NULL;
static int polyScans[200]; // index of the first point of each scan; -1: none
static int firstX, firstY;
static int lastX, lastY;
static int pofsx, pofsy;
static int pscale;
static int pangles;
static int panglec;
static int vertexnum;


void polymodInitialize (void) {
  for (int f = 0; f < 200; ++f) polyScans[f] = -1;
  polyPointPool = NULL;
  polyPointPoolSize = 0;
  polyPointPoolPos = 0;
  vertexnum = 0;
}


void polymodDeinitialize (void) {
  if (polyPointPool != NULL) free(polyPointPool);
  polyPointPool = NULL;
  polyPointPoolSize = 0;
  polyPointPoolPos = 0;
  vertexnum = 0;
}


static inline int allocPolyPoint (int x, int next, int dy) {
  if (polyPointPoolPos+1 > polyPointPoolSize) {
    int newsz = polyPointPoolSize+8192;
    PolyPoint *nn = realloc(polyPointPool, sizeof(PolyPoint)*newsz);
    //
    if (nn == NULL) fatal("out of memory in polymod");
    polyPointPool = nn;
    polyPointPoolSize = newsz;
  }
  polyPointPool[polyPointPoolPos].x = x;
  polyPointPool[polyPointPoolPos].next = next;
  polyPointPool[polyPointPoolPos].dy = dy;
  polyPointPool[polyPointPoolPos].vertexnum = vertexnum;
  return polyPointPoolPos++;
}


static inline void insertPoint (int x, int y, int dy) {
  if (y >= 0 && y < 200) {
    int n;
    //
    if ((n = polyScans[y]) != -1) {
      int p;
      //
      for (p = -1; n != -1 && x > polyPointPool[n].x; p = n, n = polyPointPool[n].next) ;
      if (p == -1) {
        // this is new first point
        polyScans[y] = allocPolyPoint(x, n, dy);
      } else {
        // insert new point after p; n is the next point
        polyPointPool[p].next = allocPolyPoint(x, n, dy);
      }
    } else {
      // first
      polyScans[y] = allocPolyPoint(x, -1, dy);
    }
  }
}


/*
static inline void rotate (int *x, int *y) {
  int64_t tx = (int64_t)(*x)*pscale/POLYFIX_BASE;
  int64_t ty = (int64_t)(*y)*pscale/POLYFIX_BASE;
  *x = (tx*costab[pangle]-ty*sintab[pangle])/POLYFIX_BASE+pofsx;
  *y = (tx*sintab[pangle]+ty*costab[pangle])/POLYFIX_BASE+pofsy;
}
*/
/*
#define rotate(x, y)  do { \
  int64_t tx = (int64_t)(x)*pscale/POLYFIX_BASE; \
  int64_t ty = (int64_t)(y)*pscale/POLYFIX_BASE; \
  (x) = (tx*costab[pangle]-ty*sintab[pangle])/POLYFIX_BASE+pofsx; \
  (y) = (tx*sintab[pangle]+ty*costab[pangle])/POLYFIX_BASE+pofsy; \
} while (0)
*/
#define rotate(x, y)  do { \
  int64_t tx = (int64_t)(x)*pscale/POLYFIX_BASE; \
  int64_t ty = (int64_t)(y)*pscale/POLYFIX_BASE; \
  (x) = (tx*sintab[panglec]-ty*sintab[pangles])/POLYFIX_BASE+pofsx; \
  (y) = (tx*sintab[pangles]+ty*sintab[panglec])/POLYFIX_BASE+pofsy; \
} while (0)


static void lineDDA (int x0, int y0, int x1, int y1) {
  double x, dx;
  //
  /*
  rotate(&x0, &y0);
  rotate(&x1, &y1);
  */
  rotate(x0, y0);
  rotate(x1, y1);
  //
  if (fdump) fprintf(fdump, "%d: (%d,%d)-(%d,%d)\n", vertexnum, x0, y0, x1, y1);
  //
  if (y0 == y1) return; // horizontal line
  //
  //fprintf(stderr, "*(%d,%d)-(%d,%d)\n", x0, y0, x1, y1);
  if ((y0 < 0 && y1 < 0) || (y0 > 199 && y1 > 199)) return; // out-of-screen lines aren't interesting
  //
  if (x0 == x1) {
    // straight vertical line
    if (y0 < y1) {
      for (; y0 < y1; ++y0) insertPoint(x0, y0, 1);
    } else {
      for (--y0; y0 >= y1; --y0) insertPoint(x0, y0, -1);
    }
  } else {
    // draw sloped
    x = x0;
    if (y0 < y1) {
      dx = (double)(x1-x0)/(double)(y1-y0);
      for (; y0 < y1; ++y0) { insertPoint(x+0.5, y0, 1); x += dx; }
    } else {
      dx = (double)(x1-x0)/(double)(y0-y1);
      for (--y0; y0 >= y1; --y0) { insertPoint(x+0.5, y0, -1); x += dx; }
    }
  }
}


static void polymodSetAngles (int angle) {
  angle %= 360; if (angle < 0) angle += 360;
  pangles = angle;
  panglec = (angle+90)%360;
}


void polymodStart (int ofsx, int ofsy, int scale, int angle) {
  for (int f = 0; f < 200; ++f) polyScans[f] = -1;
  polyPointPoolPos = 0;
  pofsx = ofsx;
  pofsy = ofsy;
  pscale = scale;
  polymodSetAngles(angle);
  if (polyDump) {
    fdump = fopen("zdump.txt", "w");
  }
  vertexnum = 0;
}


void polymodAddPoint (int x, int y) {
  if (vertexnum > 0) {
    lineDDA(lastX, lastY, x, y);
  } else {
    firstX = x;
    firstY = y;
  }
  lastX = x;
  lastY = y;
  ++vertexnum;
}


void polymodEnd (void) {
  if (vertexnum >= 3) {
    lineDDA(lastX, lastY, firstX, firstY);
  } else {
    for (int f = 0; f < 200; ++f) polyScans[f] = -1;
  }
}


static void drawLine (SDL_Surface *frame, int x0, int y0, int x1, int y1, Uint8 color, Uint8 alpha) {
  int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
  int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1;
  int err = dx+dy, e2; /* error value e_xy */
  for (;;) {
    //if (x0 == x1 && y0 == y1) break; // break first, so last point is not drawn
    putPixelA2x(frame, x0, y0, color, alpha);
    if (x0 == x1 && y0 == y1) break;
    e2 = 2*err;
    if (e2 >= dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
    if (e2 <= dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
  }
}


void polymodFill (SDL_Surface *frame, Uint8 color, Uint8 alpha) {
  //fprintf(stderr, "vertexnum=%d\n", vertexnum);
  if (vertexnum < 1) return;
  if (fdump) {
    fprintf(fdump, "=========================\n");
    for (int y = 0; y < 200; ++y) {
      int p = polyScans[y], cnt = 0;
      //
      if (p < 0) continue;
      fprintf(fdump, "y: %d\n", y);
      while (p >= 0) {
        fprintf(fdump, " x: %d (%d) [%d]\n", polyPointPool[p].x, polyPointPool[p].dy, polyPointPool[p].vertexnum);
        p = polyPointPool[p].next;
        ++cnt;
      }
      if (cnt%2) fprintf(fdump, " ***\n");
    }
  }
  //
  if (vertexnum == 1) {
    rotate(firstX, firstY);
    putPixelA2x(frame, firstX, firstY, color, alpha);
    return;
  }
  //
  if (vertexnum == 2) {
    rotate(firstX, firstY);
    rotate(lastX, lastY);
    //fprintf(stderr, "(%d,%d)-(%d,%d)\n", firstX, firstY, lastX, lastY);
    drawLine(frame, firstX, firstY, lastX, lastY, color, alpha);
    return;
  }
  //
  for (int y = 0; y < 200; ++y) {
    int p = polyScans[y];
    //
    if (p >= 0) {
      int wind = polyPointPool[p].dy, x = polyPointPool[p].x;
      //
      for (p = polyPointPool[p].next; p != -1; p = polyPointPool[p].next) {
        int ex = polyPointPool[p].x;
        //
        if (wind != 0) {
          while (x++ < ex) putPixelA2x(frame, x, y, color, alpha);
        } else {
          x = ex;
        }
        if (polyPointPool[p].dy < 0) putPixelA2x(frame, x, y, color, alpha);
        wind += polyPointPool[p].dy;
      }
    }
  }
  if (polyDump) {
    if (fdump != NULL) fclose(fdump);
    fdump = NULL;
    polyDump = 0;
  }
}


// returns next ofsx
static int polymodStrokes (const signed char *data, int len, SDL_Surface *frame, int ofsx, int ofsy, int scale, int angle, Uint8 color, Uint8 alpha) {
  int res = data[2];
  int pendown = 0;
  // skip dimensions
  //fprintf(stderr, "len=%d\n", len);
  if (frame == NULL) return res;
  len /= 2;
  data += 3;
  --len;
  while (len-- > 0) {
    if (data[0] == -128 && data[1] == -128) {
      /*
      if (pendown) {
        polymodStart(ofsx, ofsy, scale, angle);
        polymodAddPoint(data[-2], data[-1]);
        polymodEnd();
        polymodFill(frame, color, alpha);
      }
      */
      pendown = 0;
    } else {
      if (pendown) {
        //fprintf(stderr, " %d: (%d,%d)-(%d,%d)\n", len, data[-2], data[-1], data[0], data[1]);
        polymodStart(ofsx, ofsy, scale, angle);
        polymodAddPoint(data[-2], data[-1]);
        polymodAddPoint(data[0], data[1]);
        polymodEnd();
        polymodFill(frame, color, alpha);
      } else {
        pendown = 1;
      }
    }
    data += 2;
  }
  /*
  if (pendown) {
    polymodStart(ofsx, ofsy, scale, angle);
    polymodAddPoint(data[-2], data[-1]);
    polymodEnd();
    polymodFill(frame, color, alpha);
  }
  */
  return res;
}


#include "hfont.c"

int polymodChar (SDL_Surface *frame, int ch, int ofsx, int ofsy, int scale, int angle, Uint8 color, Uint8 alpha) {
  if (ch < ' ' || ch > 127) return 0;
  return polymodStrokes(hfData+hfInfo[ch-32], hfInfo[ch-31]-hfInfo[ch-32], frame, ofsx, ofsy, scale, angle, color, alpha);
}


int polymodStr (SDL_Surface *frame, const char *str, int ofsx, int ofsy, int scale, int angle, Uint8 color, Uint8 alpha) {
  int sx = ofsx;
  //
  //if (frame == NULL) fprintf(stderr, "[%s]\n", str);
  while (*str) {
    int w, x0, y0;
    //
    w = polymodChar(frame, *str++, ofsx, ofsy, scale, angle, color, alpha);
    x0 = w;
    y0 = 0;
    pofsx = pofsy = 0;
    pscale = scale;
    polymodSetAngles(angle);
    rotate(x0, y0);
    //fprintf(stderr, "'%c': w=%d; x0=%d; y0=%d; ofsx=%d\n", str[-1], w, x0, y0, ofsx);
    ofsx += x0;
    ofsy += y0;
    pscale = POLYFIX_BASE;
    if (*str) {
      x0 = 1;
      y0 = 0;
      rotate(x0, y0);
      ofsx += x0;
      ofsy += y0;
    }
  }
  //if (frame == NULL) fprintf(stderr, "w=%d\n", ofsx-sx);
  return ofsx-sx;
}

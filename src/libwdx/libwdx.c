/*
 * original code: WDOSX-Pack v1.07, (c) 1999-2001 by Joergen Ibsen / Jibz
 * for data and executable compression software: http://www.ibsensoftware.com/
 */
#include "libwdx.h"

#include <stdio.h>

#include <setjmp.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>


#ifndef WDX_EXCLUDE_CRC
__attribute__((pure)) uint32_t wdxCRC32 (const void *src, int len) {
  static const unsigned int crctab[16] = {
    0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac, 0x76dc4190,
    0x6b6b51f4, 0x4db26158, 0x5005713c, 0xedb88320, 0xf00f9344,
    0xd6d6a3e8, 0xcb61b38c, 0x9b64c2b0, 0x86d3d2d4, 0xa00ae278,
    0xbdbdf21c
  };
  const uint8_t *buf = (const uint8_t *)src;
  uint32_t crc = 0xffffffffUL;
  //
  if (src == NULL || len < 1) return 0;
  while (len--) {
    crc ^= *buf++;
    crc = crctab[crc&0x0f]^(crc>>4);
    crc = crctab[crc&0x0f]^(crc>>4);
  }
  return crc^0xffffffffUL;
}
#endif


#define WDXUNPACK_LEN2_LIMIT  1920

#define WDXFatalError()  longjmp(errJP, 666)


#ifndef WDX_EXCLUDE_PACKER
__attribute__((const)) int wdxPackBufferSize (int inSize) {
  if (inSize < 1) return 0;
  return inSize+((inSize+7)/8)+2;
}


int wdxPack (void *dest, int destSize, const void *source, int length) {
  typedef struct {
    uint32_t pos;
    uint32_t len;
  } TheMatch;
  /* global variables */
  const uint8_t *inBuffer = (const uint8_t *)source, *sourceOfs, *backPtr;
  uint8_t *outBuffer = (uint8_t *)dest;
  uint32_t nextBackEntry;
  uint8_t *tagByte;
  int bitCount;
  uint32_t *backTable; /* back-table, array */
  //uint32_t lookup[256][256]; /* lookup-table */ /*FIXME: made dynamic*/
  uint32_t *lookup = NULL; /* lookup-table */ /*FIXME: made dynamic*/
  uint32_t lastMatchPos = 0;
  int lastWasMatch = 0;
  jmp_buf errJP;
  uint32_t bytesOut = 0;

  void advanceTagByte (int bit) {
    /* check bitcount and then decrement */
    if (bitCount == 0) {
      if (--destSize < 0) WDXFatalError();
      bitCount = 8;
      tagByte = outBuffer++;
      *tagByte = 0;
      ++bytesOut;
    }
    /* shift in bit */
    *tagByte = ((*tagByte)<<1)|(bit?1:0);
    --bitCount;
  }

  /* output Gamma2-code for val in range [2..?] ... */
  void outputGamma (uint32_t val) {
    uint32_t invertlen = 0, invert = 0;
    //
    /* rotate bits into invert (except last) */
    do {
      invert = (invert<<1)|(val&0x01);
      ++invertlen;
      val = (val>>1)&0x7FFFFFFF;
    } while (val > 1);
    /* output Gamma2-encoded bits */
    for (--invertlen; invertlen > 0; --invertlen) {
      advanceTagByte(invert&0x01);
      advanceTagByte(1);
      invert >>= 1;
    }
    advanceTagByte(invert&0x01);
    advanceTagByte(0);
  }

  void outputLiteral (uint8_t lit) {
    lastWasMatch = 0;
    advanceTagByte(0); /* 0 indicates a literal */
    if (--destSize < 0) WDXFatalError();
    *outBuffer++ = lit; /* output the literal */
    ++bytesOut;
  }

  void outputCodePair (uint32_t pos, uint32_t len, const uint8_t *buffer) {
    /* if we just had a match, don't use lastMatchPos */
    if (lastWasMatch) {
      /* if a short match is too far away, encode it as two literals instead */
      if (pos > WDXUNPACK_LEN2_LIMIT && len == 2) {
        outputLiteral(buffer[0]);
        outputLiteral(buffer[1]);
      } else {
        advanceTagByte(1); /* 1 indicates a match */
        /* a match more than WDXUNPACK_LEN2_LIMIT bytes back will be longer than 2 */
        if (pos > WDXUNPACK_LEN2_LIMIT) --len;
        outputGamma(len); /* output length */
        lastMatchPos = pos--;
        /*assert(pos >= 0);*/
        outputGamma(((pos>>6)&0x3FFFFFFF)+2); /* output high part of position */
        /* output low 6 bits of position */
        advanceTagByte(pos&0x20);
        advanceTagByte(pos&0x10);
        advanceTagByte(pos&0x08);
        advanceTagByte(pos&0x04);
        advanceTagByte(pos&0x02);
        advanceTagByte(pos&0x01);
      }
    } else {
      lastWasMatch = 1;
      /* if a short match is too far away, encode it as two literals instead */
      if (pos > WDXUNPACK_LEN2_LIMIT && len == 2 && pos != lastMatchPos) {
        outputLiteral(buffer[0]);
        outputLiteral(buffer[1]);
      } else {
        advanceTagByte(1); /* 1 indicates a match */
        /* a match more than WDXUNPACK_LEN2_LIMIT bytes back will be longer than 2 */
        if (pos > WDXUNPACK_LEN2_LIMIT && pos != lastMatchPos) --len;
        outputGamma(len); /* output length */
        /* output position */
        if (pos == lastMatchPos) {
          /* a match with position 0 means use last position */
          advanceTagByte(0);
          advanceTagByte(0);
        } else {
          lastMatchPos = pos--;
          /*assert(pos >= 0);*/
          outputGamma(((pos>>6)&0x3FFFFFFF)+3); /* output high part of position */
          /* output low 6 bits of position */
          advanceTagByte(pos&0x20);
          advanceTagByte(pos&0x10);
          advanceTagByte(pos&0x08);
          advanceTagByte(pos&0x04);
          advanceTagByte(pos&0x02);
          advanceTagByte(pos&0x01);
        }
      }
    }
  }

  void findMatch (TheMatch *thematch, const uint8_t *buffer, uint32_t lookback, uint32_t lookforward) {
    uint32_t backPos, matchLen, bestMatchLen, bestMatchPos;
    const uint8_t *ptr;
    uint32_t i0, i1;
    uint32_t idx0, idx1;
    //
    /* temporary variables to avoid indirect addressing into the match */
    bestMatchLen = 0; bestMatchPos = 0;
    /* update lookup- and backtable up to current position */
    while (backPtr < buffer) {
      idx0 = backPtr[0];
      idx1 = backPtr[1];
      backTable[nextBackEntry] = lookup[idx0*256+idx1];
      lookup[idx0*256+idx1] = nextBackEntry;
      ++nextBackEntry;
      ++backPtr;
    }
    /* get position by looking up next two bytes */
    backPos = lookup[buffer[0]*256+buffer[1]];
    if (backPos != 0 && lookforward > 1) {
      ptr = backPos+sourceOfs;
      /* go backwards until before buffer */
      while (ptr >= buffer && backPos != 0) {
        /*backPos := PInt(Integer(backTable)+backPos*4)^;*/
        backPos = backTable[backPos];
        ptr = backPos+sourceOfs;
      }
      /* search through table entries */
      while (backPos != 0 && buffer-ptr <= lookback) {
        matchLen = 2;
        /* if this position has a chance to be better */
        if (*(ptr+bestMatchLen) == *(buffer+bestMatchLen)) {
          /* scan it */
          while (matchLen < lookforward && *(ptr+matchLen) == *(buffer+matchLen)) ++matchLen;
          /* check it */
          i0 = buffer-ptr==lastMatchPos ? 1 : 0;
          i1 = bestMatchPos==lastMatchPos ? 1 : 0;
          if (matchLen+i0 > bestMatchLen+i1) {
            bestMatchLen = matchLen;
            if (bestMatchLen == lookforward) backPos = 0;
            bestMatchPos = buffer-ptr;
          }
        }
        /* move backwards to next position */
        /*backPos := PInt(Integer(backTable)+backPos*4)^;*/
        backPos = backTable[backPos];
        ptr = backPos+sourceOfs;
      }
    }
    /* forget match if too far away */
    if (bestMatchPos > WDXUNPACK_LEN2_LIMIT && bestMatchLen == 2 && bestMatchPos != lastMatchPos) {
      bestMatchLen = 0;
      bestMatchPos = 0;
    }
    /* update the match with best match */
    thematch->len = bestMatchLen;
    thematch->pos = bestMatchPos;
  }

  /* main code */
  TheMatch match, nextmatch, literalmatch, testmatch;
  uint32_t pos, lastpos, literalCount;
  //int i, j;
  uint32_t i0, i1;

  if (length < 1) return 0;
  if (destSize < 2) return -1;
  if (!(backTable = malloc((length+4)*4))) return -2; /* out of memory */
  if (!(lookup = malloc(256*256*4))) { free(backTable); return -2; } /* out of memory */

  if (setjmp(errJP)) {
    /* compressing error */
    free(lookup);
    free(backTable);
    return -1;
  }

  memset(&match, 0, sizeof(match));
  memset(&nextmatch, 0, sizeof(nextmatch));
  memset(&literalmatch, 0, sizeof(literalmatch));
  memset(&testmatch, 0, sizeof(testmatch));
  literalmatch.pos = literalmatch.len = 0;
  sourceOfs = inBuffer-1;
  /* init lookup- and backtable */
  /*for (i = 0; i <= 255; ++i) for (j = 0; j <= 255; ++j) lookup[i*256+j] = 0;*/
  memset(lookup, 0, 256*256*4);
  memset(backTable, 0, (length+4)*4);
  backPtr = inBuffer;
  backTable[0] = 0;
  nextBackEntry = 1;
  lastpos = -1;
  lastMatchPos = -1;
  lastWasMatch = 0;
  literalCount = 0;
  /* the first byte is sent verbatim */
  *outBuffer++ = *inBuffer++;
  --destSize;
  ++bytesOut;
  /* init tag-byte */
  bitCount = 8;
  *(tagByte = outBuffer++) = 0;
  --destSize;
  ++bytesOut;
  /* pack data */
  pos = 1;
  while (pos < length) {
    /* find best match at current position (if not already found) */
    if (pos == lastpos) {
      match.len = nextmatch.len;
      match.pos = nextmatch.pos;
    } else {
      findMatch(&match, inBuffer, pos, length-pos);
    }
    /* if we found a match, find the best match at the next position */
    if (match.len != 0) {
      findMatch(&nextmatch, inBuffer+1, pos+1, length-(pos+1));
      lastpos = pos+1;
    } else {
      nextmatch.len = 0;
    }
    /* decide if we should output a match or a literal */
    i0 = (match.pos==lastMatchPos ? 1 : 0);
    i1 = (nextmatch.pos==lastMatchPos ? 1 : 0);
    if (match.len != 0 && match.len+i0 >= nextmatch.len+i1) {
      /* output any pending literals */
      if (literalCount != 0) {
        if (literalCount == 1) {
          outputLiteral(inBuffer[-1]);
        } else {
          /* check if there is a closer match with the required length */
          findMatch(&testmatch, inBuffer-literalCount, literalmatch.pos, literalCount);
          if (testmatch.len >= literalCount) outputCodePair(testmatch.pos, literalCount, inBuffer-literalCount);
          else outputCodePair(literalmatch.pos, literalCount, inBuffer-literalCount);
        }
        literalCount = 0;
      }
      /* output match */
      outputCodePair(match.pos, match.len, inBuffer);
      inBuffer += match.len;
      pos += match.len;
    } else {
      /* check if we are allready collecting literals */
      if (literalCount != 0) {
        /* if so, continue.. */
        ++literalCount;
        /* have we collected as many as possible? */
        if (literalCount == literalmatch.len) {
          outputCodePair(literalmatch.pos, literalCount, inBuffer-literalCount+1);
          literalCount = 0;
        }
      } else {
        /* if we had a match which was not good enough, then save it.. */
        if (match.len != 0) {
          literalmatch.len = match.len;
          literalmatch.pos = match.pos;
          ++literalCount;
        } else {
          /* if not, we have to output the literal now */
          outputLiteral(inBuffer[0]);
        }
      }
      ++inBuffer;
      ++pos;
    }
  }
  /* output any remaining literal bytes */
  if (literalCount != 0) {
    if (literalCount == 1) outputLiteral(inBuffer[-1]);
    else outputCodePair(literalmatch.pos, literalCount, inBuffer-literalCount);
  }
  /* switch last tagByte into position */
  if (bitCount != 8) *tagByte <<= bitCount;
  //
  free(lookup);
  free(backTable);
  return bytesOut;
}
#endif


#ifndef WDX_EXCLUDE_UNPACKER
int wdxUnpack (void *bufOut, int outSize, const void *bufIn, int inSize) {
  const uint8_t *src = (const uint8_t *)bufIn;
  uint8_t *dest = (uint8_t *)bufOut, *pp;
  uint8_t fbyte = 0;
  int lastMatchPos = 0, lastWasMatch = 0, len, pos, b, bCount = 0, origOutSz = outSize;
  jmp_buf errJP;
  int itsOk = 0;

  int getBit (void) {
    int res;
    //
    if (bCount <= 0) {
      if (inSize < 1) WDXFatalError();
      fbyte = *src++;
      --inSize;
      bCount = 8;
    }
    res = (fbyte&0x80)!=0 ? 1 : 0;
    fbyte = (fbyte&0x7f)<<1;
    --bCount;
    return res;
  }

  int getGamma (void) {
    int res = 1;
    //
    do {
      res = (res<<1)|getBit();
    } while (getBit() == 1);
    return res;
  }

  /* get 6 low bits of position */
  int getLoPos (int pos) {
    for (int f = 0; f < 6; ++f) pos = (pos<<1)|getBit();
    return pos;
  }

  /* main code */
  if (outSize < 1) return 0;
  if (inSize < 1) return -1; /* out of input data */

  if (setjmp(errJP)) {
    /* decompressing error */
    if (!itsOk) return -1;
    return origOutSz-outSize;
  }

  /* the first byte was sent verbatim */
  *dest++ = *src++;
  --inSize;
  --outSize;
  while (outSize > 0) {
    itsOk = 1;
    b = getBit();
    itsOk = 0;
    if (b == 0) {
      /* literal */
      if (inSize < 1) break;
      *dest++ = *src++;
      --inSize;
      --outSize;
      lastWasMatch = 0;
    } else {
      /* match */
      len = getGamma();
      if (lastWasMatch) {
        pos = getGamma()-2;
        pos = getLoPos(pos)+1;
        lastMatchPos = pos;
        if (pos > WDXUNPACK_LEN2_LIMIT) ++len;
      } else {
        lastWasMatch = 1;
        pos = getGamma()-2;
        /* same position as last match? */
        if (pos == 0) pos = lastMatchPos;
        else {
          pos = getLoPos(pos-1)+1;
          lastMatchPos = pos;
          if (pos > WDXUNPACK_LEN2_LIMIT) ++len;
        }
      }
      /* copy match */
      pp = dest-pos;
      if ((void *)pp < (void *)bufOut) return -1; /* shit! */
      for (; len > 0 && outSize > 0; --outSize, --len) *dest++ = *pp++;
    }
  }
  return origOutSz-outSize;
}
#endif

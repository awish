/*
 * original code: WDOSX-Pack v1.07, (c) 1999-2001 by Joergen Ibsen / Jibz
 * for data and executable compression software: http://www.ibsensoftware.com/
 */
#ifndef WDXLIB_H
#define WDXLIB_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>


#ifndef WDX_EXCLUDE_CRC
/* poly: 0xedb88320L: ISO 3309, ITU-T V.42 */
extern uint32_t wdxCRC32 (const void *src, int len) __attribute__((pure));
#endif

#ifndef WDX_EXCLUDE_PACKER
/* compress buffer
 * return -1 or size of the packed data
 * uncompressible data may be expanded by up to 1 bit per byte:
 * i.e. the destination should be length+((length+7)/8)+2 bytes
 */
extern int wdxPack (void *dest, int destSize, const void *source, int length);

/* returs max size of the packed data with the given length */
extern int wdxPackBufferSize (int inSize) __attribute__((const));
#endif

#ifndef WDX_EXCLUDE_UNPACKER
/* decompress buffer
 * return -1 or size of the unpacked data
 */
extern int wdxUnpack (void *bufOut, int outSize, const void *bufIn, int inSize);
#endif


#ifdef __cplusplus
}
#endif
#endif

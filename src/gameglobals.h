/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _GAMEGLOBALS_H_
#define _GAMEGLOBALS_H_

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

#include "awishcommon.h"
#include "resfile.h"
#include "vm.h"

#include "SDL.h"


extern ResFile resfile;
extern ResFile sndfile;

extern SDL_Surface *backs[9]; //0: title; 8: interm


typedef struct {
  int count;
  SDL_Surface *spr[256][2];
} SpriteBank;

extern SpriteBank banks[257];

//extern int mouseX, mouseY, mouseButtons, mouseVisible;

//extern int disableSound;


extern void fatal (const char *fmt, ...) __attribute__((__noreturn__)) __attribute__((format(printf, 1, 2)));

extern void initLabels (void);


extern Uint32 getSeedH (void) AWISH_PURE;
extern Uint32 getSeedL (void) AWISH_PURE;
extern Uint32 getSeed (void) AWISH_PURE;

extern void setSeedHL (Uint32 h, Uint32 l);
extern void setSeedH (Uint32 u);
extern void setSeedL (Uint32 u);
extern void setSeed (Uint32 u);

extern Uint32 randUInt32 (void);


extern int loadCodeFile (ResFile *resfile, int pc, int idx, int asmodule);


extern int loadSound (int idx); // !0: error
extern int unloadSound (int idx); // !0: error (not loaded)
extern int isSoundLoaded (int idx) AWISH_PURE; // bool
extern int playSound (int idx, int chan); // returns channel or -1
extern int stopChannel (int chan); // -1: all
extern int isChannelPlaying (int chan); // bool (-1: how many channels are playing?)

extern void unloadAllSounds (void);

// return # of sprites loaded or <0 on error
extern int loadSpriteBankFromFile (SpriteBank *bank, const char *fname);


extern void clearSpriteLayers (void);
extern void addSpriteToLayer (int x, int y, int layer, int bank, int num, int dir, int inlevel);
extern void levelDrawSpriteLayers (SDL_Surface *frame, int visx, int visy, int visw, int vish);

extern void pmClear (void);
extern void pmStart (int ofsx, int ofsy, int scale, int angle);
extern void pmAddPoint (int x, int y);
extern void pmDone (int color, int alpha);
extern void pmDraw (SDL_Surface *frame);

extern void textClear (void);
extern void textAdd (const char *str, int len, int x, int y, int scale, int angle, int color, int alpha);
extern void textDraw (SDL_Surface *frame);


#include "vm_gamelabels.h"


#endif

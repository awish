/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBRNC_H
#define LIBRNC_H

#ifdef __cplusplus
extern "C" {
#endif


/*
 * Error returns
 */
#define RNC_FILE_IS_NOT_RNC     (-1)
#define RNC_HUF_DECODE_ERROR    (-2)
#define RNC_FILE_SIZE_MISMATCH  (-3)
#define RNC_PACKED_CRC_ERROR    (-4)
#define RNC_UNPACKED_CRC_ERROR  (-5)


/*
 * The compressor needs this define
 */
#define RNC_SIGNATURE  (0x524E4301)  /* "RNC\001" */


extern int rnc_sign (const void *packed) __attribute__((pure)); // is RNC signature valid? (!0: yes)
extern int rnc_plen (const void *packed) __attribute__((pure)); // packed data length
extern int rnc_ulen (const void *packed) __attribute__((pure)); // unpacked data length
extern int rnc_over (const void *packed) __attribute__((pure)); // overlap data for in-place unpacking
extern int rnc_unpack (const void *packed, void *unpacked, int *leeway); // packed data must contain 4 extra zero bytes
extern const char *rnc_error (int errcode) __attribute__((const));
extern int rnc_crc (const void *data, int len);

extern void *rnc_pack (const void *original, int datalen, int *packlen);


#ifdef __cplusplus
}
#endif
#endif

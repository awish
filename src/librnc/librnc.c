/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "librnc.h"


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  unsigned int bitbuf; /* holds between 16 and 32 bits */
  int bitcount;        /* how many bits does bitbuf hold? */
} BitStream;


typedef struct {
  int num;             /* number of nodes in the tree */
  struct {
    unsigned int code;
    int codelen;
    int value;
  } table[32];
} HuffmanTable;


////////////////////////////////////////////////////////////////////////////////
/*
 * Return the big-endian longword at p.
 */
static inline unsigned int blong (const unsigned char *p) {
  unsigned int n;
  //
  n = p[0];
  n = (n<<8)+p[1];
  n = (n<<8)+p[2];
  n = (n<<8)+p[3];
  return n;
}


/*
 * Return the big-endian word at p.
 */
static inline unsigned int bword (const unsigned char *p) {
  unsigned int n;
  //
  n = p[0];
  n = (n<<8)+p[1];
  return n;
}


/*
 * Return the little-endian word at p.
 */
static inline unsigned int lword (const unsigned char *p) {
  unsigned int n;
  //
  n = p[1];
  n = (n<<8)+p[0];
  return n;
}


/*
 * Mirror the bottom n bits of x.
 */
static __attribute__((const)) inline unsigned int mirror (unsigned int x, int n) {
  unsigned int top = 1<<(n-1), bottom = 1;
  //
  while (top > bottom) {
    unsigned int mask = top|bottom;
    unsigned int masked = x&mask;
    //
    if (masked != 0 && masked != mask) x ^= mask;
    top >>= 1;
    bottom <<= 1;
  }
  return x;
}


/*
 * Initialises a bit stream with the first two bytes of the packed
 * data.
 */
static inline void bitread_init (BitStream *bs, const unsigned char **p) {
  bs->bitbuf = lword(*p);
  bs->bitcount = 16;
}


/*
 * Fixes up a bit stream after literals have been read out of the
 * data stream.
 */
static inline void bitread_fix (BitStream *bs, const unsigned char **p) {
  bs->bitcount -= 16;
  bs->bitbuf &= (1<<bs->bitcount)-1; /* remove the top 16 bits */
  bs->bitbuf |= (lword(*p)<<bs->bitcount); /* replace with what's at *p */
  bs->bitcount += 16;
}


/*
 * Returns some bits.
 */
static inline unsigned int bit_peek (BitStream *bs, unsigned int mask) {
  return bs->bitbuf&mask;
}


/*
 * Advances the bit stream.
 */
static inline void bit_advance (BitStream *bs, int n, const unsigned char **p) {
  bs->bitbuf >>= n;
  bs->bitcount -= n;
  if (bs->bitcount < 16) {
    (*p) += 2;
    bs->bitbuf |= (lword(*p)<<bs->bitcount);
    bs->bitcount += 16;
  }
}


/*
 * Reads some bits in one go (ie the above two routines combined).
 */
static inline unsigned int bit_read (BitStream *bs, unsigned int mask, int n, const unsigned char **p) {
  unsigned int result = bit_peek(bs, mask);
  //
  bit_advance(bs, n, p);
  return result;
}


/*
 * Read a Huffman table out of the bit stream and data stream given.
 */
static int read_huftable (HuffmanTable *h, BitStream *bs, const unsigned char **p) {
  int k, num;
  int leaflen[32];
  int leafmax;
  unsigned int codeb; /* big-endian form of code */
  //
  num = bit_read(bs, 0x1f, 5, p);
  if (!num) return 0;
  leafmax = 1;
  for (int i = 0; i < num; ++i) {
    leaflen[i] = bit_read(bs, 0x0F, 4, p);
    if (leafmax < leaflen[i]) leafmax = leaflen[i];
  }
  codeb = 0L;
  k = 0;
  for (int i = 1; i <= leafmax; ++i) {
    for (int j = 0; j < num; ++j) {
      if (leaflen[j] == i) {
        h->table[k].code = mirror(codeb, i);
        h->table[k].codelen = i;
        h->table[k].value = j;
        ++codeb;
        ++k;
      }
    }
    codeb <<= 1;
  }
  h->num = k;
  return 0;
}


/*
 * Read a value out of the bit stream using the given Huffman table.
 */
static unsigned int huf_read (HuffmanTable *h, BitStream *bs, const unsigned char **p) {
  int i;
  unsigned int val;
  //
  for (i = 0; i < h->num; i++) {
    unsigned int mask = (1<<h->table[i].codelen)-1;
    //
    if (bit_peek(bs, mask) == h->table[i].code) break;
  }
  if (i == h->num) return (unsigned int)-1;
  bit_advance(bs, h->table[i].codelen, p);
  val = h->table[i].value;
  if (val >= 2) {
    val = 1<<(val-1);
    val |= bit_read(bs, val-1, h->table[i].value-1, p);
  }
  return val;
}


////////////////////////////////////////////////////////////////////////////////
/*
 * Decompress a packed data block. Returns the unpacked length if
 * successful, or negative error codes if not.
 *
 * If COMPRESSOR is defined, it also returns the leeway number
 * (which gets stored at offset 16 into the compressed-file header)
 * in `*leeway', if `leeway' isn't NULL.
 */
int rnc_unpack (const void *packed, void *unpacked, int *leeway) {
  const unsigned char *input = packed;
  const unsigned char *inputend;
  unsigned char *output = unpacked;
  unsigned char *outputend;
  BitStream bs;
  HuffmanTable raw, dist, len;
  unsigned int ch_count;
  unsigned int ret_len;
  unsigned out_crc;
  int lee = 0;
  //
  if (blong(input) != RNC_SIGNATURE) return RNC_FILE_IS_NOT_RNC;
  ret_len = blong(input+4);
  outputend = output+ret_len;
  inputend = input+18+blong(input+8);
  input += 18; /* skip header */
  // check the packed-data CRC. Also save the unpacked-data CRC for later
  if (rnc_crc(input, inputend-input) != bword(input-4)) return RNC_PACKED_CRC_ERROR;
  out_crc = bword(input-6);
  bitread_init(&bs, &input);
  bit_advance(&bs, 2, &input); /* discard first two bits */
  // process chunks
  while (output < outputend) {
    int this_lee;
    //
    read_huftable(&raw, &bs, &input);
    read_huftable(&dist, &bs, &input);
    read_huftable(&len, &bs, &input);
    ch_count = bit_read(&bs, 0xffff, 16, &input);
    //
    for (;;) {
      int length, posn;
      //
      length = huf_read(&raw, &bs, &input);
      if (length == -1) return RNC_HUF_DECODE_ERROR;
      if (length) {
        while (length--) *output++ = *input++;
        bitread_fix(&bs, &input);
      }
      if (--ch_count <= 0) break;
      posn = huf_read(&dist, &bs, &input);
      if (posn == -1) return RNC_HUF_DECODE_ERROR;
      length = huf_read(&len, &bs, &input);
      if (length == -1) return RNC_HUF_DECODE_ERROR;
      posn += 1;
      length += 2;
      while (length--) {
        *output = output[-posn];
        ++output;
      }
      this_lee = (inputend-input)-(outputend-output);
      if (lee < this_lee) lee = this_lee;
    }
  }
  //
  if (outputend != output) return RNC_FILE_SIZE_MISMATCH;
  if (leeway) *leeway = lee;
  // check the unpacked-data CRC
  if (rnc_crc(outputend-ret_len, ret_len) != out_crc) return RNC_UNPACKED_CRC_ERROR;
  return ret_len;
}


////////////////////////////////////////////////////////////////////////////////
/*
 * Return the uncompressed length of a packed data block, or a
 * negative error code.
 */
__attribute__((pure)) int rnc_ulen (const void *packed) {
  const unsigned char *p = packed;
  //
  if (blong(p) != RNC_SIGNATURE) return RNC_FILE_IS_NOT_RNC;
  return blong(p+4);
}


__attribute__((pure)) int rnc_sign (const void *packed) {
  const unsigned char *p = packed;
  //
  if (blong(p) != RNC_SIGNATURE) return 0;
  return 1;
}


__attribute__((pure)) int rnc_plen (const void *packed) {
  const unsigned char *p = packed;
  //
  if (blong(p) != RNC_SIGNATURE) return RNC_FILE_IS_NOT_RNC;
  return blong(p+8);
}


__attribute__((pure)) int rnc_over (const void *packed) {
  const unsigned char *p = packed;
  //
  if (blong(p) != RNC_SIGNATURE) return RNC_FILE_IS_NOT_RNC;
  return bword(p+16);
}


////////////////////////////////////////////////////////////////////////////////
/*
 * Calculate a CRC, the RNC way.
 */
int rnc_crc (const void *data, int len) {
  static int initialized = 0;
  static unsigned short crctab[256];
  unsigned short val;
  const unsigned char *p = data;
  //
  if (!initialized) {
    for (int i = 0; i < 256; ++i) {
      val = i;
      for (int j = 0; j < 8; ++j) if (val&1) val = (val>>1)^0xA001; else val = (val>>1);
      crctab[i] = val;
    }
  }
  //
  val = 0;
  while (len-- > 0) {
    val ^= *p++;
    val = (val>>8)^crctab[val&0xFF];
  }
  //
  return val;
}


////////////////////////////////////////////////////////////////////////////////
/*
 * Return an error string corresponding to an error return code.
 */
__attribute__((const)) const char *rnc_error (int errcode) {
  static const char *const errors[] = {
    "No error",
    "File is not RNC-1 format",
    "Huffman decode error",
    "File size mismatch",
    "CRC error in packed data",
    "CRC error in unpacked data",
    "Unknown error"
  };
  //
  errcode = -errcode;
  if (errcode < 0) errcode = 0;
  if (errcode > sizeof(errors)/sizeof(*errors)-1) errcode = sizeof(errors)/sizeof(*errors)-1;
  return errors[errcode];
}


////////////////////////////////////////////////////////////////////////////////
// RNC packer
//
#define BLOCKMAX      (8192)
#define WINMAX        (32767)
#define MAXTUPLES     (4096)
/* it's prime */
#define HASHMAX       (509)
#define PACKED_DELTA  (65536)


typedef struct {
  struct {
    unsigned int code;
    int codelen;
  } table[32];
} PackerHuffmanTable;


typedef struct {
  int rawlen;
  int pos;
  int len;
} Tuple;


static unsigned char blk[WINMAX];
static int linkp[WINMAX];
static int hashp[HASHMAX];
static int blkstart, bpos;
static int blklen;

static Tuple tuples[MAXTUPLES];
static int ntuple;

static unsigned char *packed;
static int packedlen;
static int packedpos, bitpos, bitcount, bitbuf;


static inline int hash (const unsigned char *a) {
  return ((a[0]*7+a[1])*7+a[2])%HASHMAX;
}


static __attribute__((const)) inline int pklength (unsigned value) {
  int ret = 0;
  //
  while (value != 0) { value >>= 1; ++ret; }
  return ret;
}


/*
 * Write big-endian int.
 */
static inline void bwrite (unsigned char *p, unsigned int val) {
  p[3] = val;
  val >>= 8;
  p[2] = val;
  val >>= 8;
  p[1] = val;
  val >>= 8;
  p[0] = val;
}


static inline void emit_raw (int n) {
  tuples[ntuple].rawlen += n;
  bpos += n;
}


static inline void emit_pair (int pos, int len) {
  tuples[ntuple].pos = bpos-pos;
  tuples[ntuple].len = len;
  tuples[++ntuple].rawlen = 0;
  bpos += len;
}


static inline void check_size (void) {
  if (packedpos > packedlen-16) {
    packedlen += PACKED_DELTA;
    packed = realloc(packed, packedlen);
    if (packed == NULL) {
      perror("realloc");
      exit(1);
    }
  }
}


static inline void write_literal (unsigned value) {
  packed[packedpos++] = value;
  check_size();
}


static inline void write_bits (unsigned value, int nbits) {
  value &= (1 << nbits)-1;
  bitbuf |= (value << bitcount);
  bitcount += nbits;
  //
  if (bitcount > 16) {
    packed[bitpos] = bitbuf;
    bitbuf >>= 8;
    packed[bitpos+1] = bitbuf;
    bitbuf >>= 8;
    bitcount -= 16;
    bitpos = packedpos;
    packedpos += 2;
    check_size();
  }
}


static void write_hval (PackerHuffmanTable *h, unsigned value) {
  int len = pklength(value);
  //
  write_bits(h->table[len].code, h->table[len].codelen);
  if (len >= 2) write_bits(value, len-1);
}


static void write_huf (PackerHuffmanTable *h) {
  int j = 0;
  //
  for (int i = 0; i < 32; ++i) if (h->table[i].codelen > 0) j = i+1;
  write_bits(j, 5);
  for (int i = 0; i < j; ++i) write_bits(h->table[i].codelen, 4);
}


static void build_huf (PackerHuffmanTable *h, int *freqs) {
  struct hnode {
    int freq, parent, lchild, rchild;
  } pool[64];
  int j, k, m, toobig;
  int maxcodelen;
  unsigned int codeb;
  //
  j = 0; /* j counts nodes in the pool */
  toobig = 1;
  for (int i = 0; i < 32; ++i) {
    if (freqs[i]) {
      pool[j].freq = freqs[i];
      pool[j].parent = -1;
      pool[j].lchild = -1;
      pool[j].rchild = i;
      ++j;
      toobig += freqs[i];
    }
  }
  k = j; /* k counts _free_ nodes in the pool */
  while (k > 1) {
    int min = toobig;
    int nextmin = toobig+1;
    int minpos = 0, nextminpos = 0;
    //
    /* loop through free nodes */
    for (int i = 0; i < j; ++i) {
      if (pool[i].parent == -1) {
        if (min > pool[i].freq) {
          nextmin = min;
          nextminpos = minpos;
          min = pool[i].freq;
          minpos = i;
        } else if (nextmin > pool[i].freq) {
          nextmin = pool[i].freq;
          nextminpos = i;
        }
      }
    }
    pool[j].freq = min + nextmin;
    pool[j].parent = -1;
    pool[j].lchild = minpos;
    pool[j].rchild = nextminpos;
    pool[minpos].parent = j;
    pool[nextminpos].parent = j;
    ++j;
    --k;
  }
  //
  for (int i = 0; i < 32; ++i) h->table[i].codelen = 0;
  //
  maxcodelen = 0;
  /* loop through original nodes */
  for (int i = 0; i < j; ++i) {
    if (pool[i].lchild == -1) {
      m = 0;
      k = i;
      while (pool[k].parent != -1) { ++m; k = pool[k].parent; }
      h->table[pool[i].rchild].codelen = m;
      if (maxcodelen < m) maxcodelen = m;
    }
  }
  //
  codeb = 0;
  for (int i = 1; i <= maxcodelen; ++i) {
    for (int j = 0; j < 32; ++j) {
      if (h->table[j].codelen == i) {
        h->table[j].code = mirror(codeb, i);
        ++codeb;
      }
    }
    codeb <<= 1;
  }
}


static void write_block (void) {
  int lengths[32];
  PackerHuffmanTable raw, dist, len;
  int k;
  //
  for (int i = 0; i < 32; ++i) lengths[i] = 0;
  for (int i = 0; i <= ntuple; ++i) ++(lengths[pklength(tuples[i].rawlen)]);
  build_huf(&raw, lengths);
  write_huf(&raw);
  //
  for (int i = 0; i < 32; ++i) lengths[i] = 0;
  for (int i = 0; i < ntuple; ++i) ++(lengths[pklength(tuples[i].pos-1)]);
  build_huf(&dist, lengths);
  write_huf(&dist);
  //
  for (int i = 0; i < 32; ++i) lengths[i] = 0;
  for (int i = 0; i < ntuple; ++i) ++(lengths[pklength(tuples[i].len-2)]);
  build_huf(&len, lengths);
  write_huf(&len);
  //
  write_bits(ntuple+1, 16);
  //
  k = blkstart;
  for (int i = 0; i<=ntuple; ++i) {
    write_hval (&raw, tuples[i].rawlen);
    for (int j = 0; j<tuples[i].rawlen; ++j) write_literal(blk[k++]);
    if (i == ntuple) break;
    write_hval(&dist, tuples[i].pos-1);
    write_hval(&len, tuples[i].len-2);
    k += tuples[i].len;
  }
}


static void do_block (void) {
  int lazylen = 0, lazypos = 0, lazyraw = 0;
  int hashv, ph, h;
  //
  bpos = 0;
  while (bpos < blkstart) {
    hashv = hash(blk+bpos);
    h = hashp[hashv];
    ph = -1;
    while (h != -1) {
      ph = h;
      h = linkp[h];
    }
    if (ph != -1) linkp[ph] = bpos; else hashp[hashv] = bpos;
    linkp[bpos] = -1;
    ++bpos;
  }
  //
  while (bpos < blklen && ntuple < MAXTUPLES-1) {
    if (blklen-bpos < 3) {
      emit_raw(blklen-bpos);
    } else {
      int len, maxlen, maxlenpos;
      int savebpos;
      //
      hashv = hash(blk+bpos);
      h = hashp[hashv];
      //
      maxlen = 0;
      maxlenpos = ph = -1;
      //
      while (h != -1) {
        unsigned char *p = blk+bpos;
        unsigned char *v = blk+h;
        //
        len = 0;
        while (*p == *v && p < blk+blklen) { ++p; ++v; ++len; }
        if (maxlen < len) {
          maxlen = len;
          maxlenpos = h;
        }
        ph = h;
        h = linkp[h];
      }
      //
      if (ph != -1) linkp[ph] = bpos; else hashp[hashv] = bpos;
      //
      linkp[bpos] = -1;
      savebpos = bpos;
      //
      bpos -= lazyraw;
      if (lazyraw) {
        if (maxlen >= lazylen+2) {
          emit_raw(lazyraw);
          lazyraw = 1;
          lazypos = maxlenpos;
          lazylen = maxlen;
        } else {
          emit_pair(lazypos, lazylen);
          lazyraw = lazypos = lazylen = 0;
        }
      } else if (maxlen >= 3) {
        lazyraw = 1;
        lazypos = maxlenpos;
        lazylen = maxlen;
      } else {
        emit_raw(1);
      }
      bpos += lazyraw;
      //
      while (++savebpos < bpos) {
        hashv = hash(blk+savebpos);
        h = hashp[hashv];
        ph = -1;
        while (h != -1) {
          ph = h;
          h = linkp[h];
        }
        if (ph != -1) linkp[ph] = savebpos; else hashp[hashv] = savebpos;
        linkp[savebpos] = -1;
      }
    }
  }
  //
  if (lazyraw) {
    bpos -= lazyraw;
    emit_raw(lazyraw);
  }
}


void *rnc_pack (const void *original, int datalen, int *packlen) {
  const char *data = original;
  int origlen = datalen;
  //
  packed = malloc(PACKED_DELTA);
  if (packed == NULL) {
    perror("malloc");
    exit(1);
  }
  //
  fprintf(stderr, "%3d%%", (int)(((long long)100)*(origlen-datalen)/origlen)); fflush(stderr);
  //
  packedlen = PACKED_DELTA;
  packedpos = 20;
  bwrite(packed+4, datalen);
  //
  bitpos = 18;
  bitcount = 0;
  bitbuf = 0;
  write_bits (0, 2);
  //
  while (datalen > 0) {
    blklen = datalen > BLOCKMAX ? BLOCKMAX : datalen;
    blkstart = WINMAX-BLOCKMAX;
    if (blkstart > origlen-datalen) blkstart = origlen-datalen;
    memcpy (blk, data-blkstart, blkstart+blklen);
    for (int i = 0; i < HASHMAX; ++i) hashp[i] = -1;
    ntuple = 0;
    tuples[ntuple].rawlen = 0;
    blklen += blkstart;
    do_block();
    data += bpos-blkstart;
    datalen -= bpos-blkstart;
    write_block();
    fprintf(stderr, "\x08\x08\x08\x08%3d%%", (int)(((long long)100)*(origlen-datalen)/origlen)); fflush(stderr);
  }
  //
  if (bitcount > 0) {
    write_bits(0, 17-bitcount); /* force flush */
    packedpos -= 2;             /* write_bits will have moved it on */
  }
  //
  *packlen = packedpos;
  //
  bwrite(packed, RNC_SIGNATURE);
  bwrite(packed+12, rnc_crc(packed+18, packedpos-18));
  bwrite(packed+10, rnc_crc(original, origlen));
  bwrite(packed+8, packedpos-18);
  packed[16] = packed[17] = 0;
  //
  fprintf(stderr, "\x08\x08\x08\x08    \x08\x08\x08\x08");
  //
  return packed;
}


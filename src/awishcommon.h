/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _AWISHCOMMON_H
#define _AWISHCOMMON_H


#define AWISH_PURE         __attribute__((pure))
#define AWISH_CONST        __attribute__((const))
#define AWISH_NORETURN     __attribute__((noreturn))
#define AWISH_PRINTF(m,n)  __attribute__((format(printf,m,n)))


#endif

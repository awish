/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _POLYMOD_H_
#define _POLYMOD_H_

#include "SDL.h"

#include "awishcommon.h"


#define POLYFIX_BASE  (4096)


extern int polyDump;

extern void polymodInitialize (void);
extern void polymodDeinitialize (void);

extern void polymodStart (int ofsx, int ofsy, int scale, int angle);
extern void polymodAddPoint (int x, int y);
extern void polymodEnd (void);

extern void polymodFill (SDL_Surface *frame, Uint8 color, Uint8 alpha);

extern int polymodChar (SDL_Surface *frame, int ch, int ofsx, int ofsy, int scale, int angle, Uint8 color, Uint8 alpha);
extern int polymodStr (SDL_Surface *frame, const char *str, int ofsx, int ofsy, int scale, int angle, Uint8 color, Uint8 alpha);


#endif

/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vm.h"


//#define VM_DEBUG

#define SECRET  (42)


// should be enough for everyone!
#define MAX_INST_COUNT  (1000000)

//extern int goobers;


//extern void fatal (const char *fmt, ...) __attribute__((__noreturn__)) __attribute__((format(printf, 1, 2)));
static __attribute__((__noreturn__)) __attribute__((format(printf, 1, 2))) void fatalis (const char *fmt, ...) {
  va_list ap;
  //
  fprintf(stderr, "AWISH FATAL: ");
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fprintf(stderr, "\n");
  exit(1);
}


#ifndef VM_FATAL_DEFINED
static __attribute__((__noreturn__)) __attribute__((format(printf, 3, 4))) void vmfatal (int tid, int pc, const char *fmt, ...) {
  va_list ap;
  //
  fprintf(stderr, "VM FATAL (thread #%d, pc=%04x): ", tid, (unsigned int)pc);
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fprintf(stderr, "\n");
  exit(1);
}
#endif


const char *vmOpNames[] = {
  "add",
  "sub",
  "mul",
  "div",
  "mod",
  "bor",
  "xor",
  "and",

  "jeq",
  "jne",
  "jlt",
  "jle",
  "jgt",
  "jge",

  "jmp",

  "bsr",

  "brk",
  "end",
  "new",

  "set",
  "get",

  "psh",
  "pop",
  "swp",
  "pck",
  "rol",
  "dpt",

  "tid",
  "kil",
  "sus",
  "res",
  "sta",

  "rxc",
  "wxc",

  "ret",

  "rst",

  "mgf",
  "mgb",
  "msf",
  "msb",

  NULL
};


VMRSTCB vmRSTCB = NULL;
VMMapGetCB vmMapGetCB = NULL;
VMMapSetCB vmMapSetCB = NULL;


typedef struct {
  int pc;
  int suspended;
  int sp;
  int *stack; // internal
  int *tvars; // internal
} VMThread;


int vmDebugTrace = 0;
FILE *vmDebugOutput = NULL;


static VMThread vmThreads[VM_MAX_THREADS];
static int vmExecuted[VM_MAX_THREADS];
static int vmLastThreadId = 0;

unsigned char vmCode[65536];
int vmCodeSize = 0;
int vmGVars[VM_VARS_SIZE];

int vmMaxGVar = 0;
int vmMaxTVar = 0;


static void fixLastThreadId (void) {
  for (; vmLastThreadId > 0; --vmLastThreadId) if (vmThreads[vmLastThreadId].stack) break;
}


static int vmFindFreeThread (void) {
  for (int f = 0; f < VM_MAX_THREADS; ++f) if (vmThreads[f].stack == NULL) return f;
  return -1;
}


static int vmInitThread (VMThread *trd) {
  trd->pc = 0;
  trd->sp = 0;
  trd->suspended = 0;
  trd->stack = calloc(VM_STACK_SIZE, sizeof(int));
  if (trd->stack == NULL) return -1;
  trd->tvars = calloc(VM_VARS_SIZE, sizeof(int));
  if (trd->tvars == NULL) { free(trd->stack); return -1; }
  return 0;
}


static void vmFreeThread (VMThread *trd) {
  if (trd->stack) free(trd->stack);
  if (trd->tvars) free(trd->tvars);
  trd->stack = NULL;
  trd->tvars = NULL;
  trd->pc = 0;
  trd->sp = 0;
  trd->suspended = 0;
}


int vmInitialize (void) {
  vmFreeLabels();
  memset(vmThreads, 0, sizeof(vmThreads));
  // setup main thread
  vmLastThreadId = 0;
  if (vmInitThread(vmThreads)) return -1; // alas
  return 0;
}


void vmDeinitialize (void) {
  for (int f = 0; f < VM_MAX_THREADS; ++f) {
    if (vmThreads[f].stack) free(vmThreads[f].stack);
    if (vmThreads[f].tvars) free(vmThreads[f].tvars);
  }
  memset(vmThreads, 0, sizeof(vmThreads));
  vmLastThreadId = 0;
  vmFreeLabels();
}


static inline int vmGetByte (int tid, int opc, VMThread *trd) {
  if (trd->pc < 0 || trd->pc >= vmCodeSize) vmfatal(tid, opc, "out of code");
  return vmCode[trd->pc++];
}


#define STACK_WANT(n)  do { if (trd->sp < (n)) vmfatal(tid, opc, "stack underflow"); } while (0)
#define STACK_FSPC(n)  do { if (trd->sp+(n) > VM_STACK_SIZE) vmfatal(tid, opc, "stack overflow"); } while (0)

#define STACK_TOP()    (trd->stack[trd->sp-1])
#define STACK_POP()    (trd->stack[--(trd->sp)])
#define STACK_PUSH(n)  trd->stack[(trd->sp)++] = (n)


#define MATH(op) do { \
  switch (argc) { \
    case 0: \
      STACK_WANT(2); \
      trd->stack[trd->sp-2] = (trd->stack[trd->sp-2]) op (trd->stack[trd->sp-1]); \
      --(trd->sp); \
      break; \
    case 1: \
      STACK_WANT(1); \
      trd->stack[trd->sp-1] = (trd->stack[trd->sp-1]) op (argv[0]); \
      break; \
    case 2: if (argp[0] != NULL) *(argp[0]) = (argv[0]) op (argv[1]); break; \
    case 3: if (argp[2] != NULL) *(argp[2]) = (argv[0]) op (argv[1]); break; \
  } \
} while(0)


#define CHECK_DIV_ZERO \
  switch (argc) { \
    case 0: \
      if (trd->sp > 0 && trd->stack[trd->sp-1] == 0) { fprintf(stderr, "VM FATAL: division by zero!\n"); exit(1); } \
      break; \
    case 1: \
      if (argv[0] == 0) { fprintf(stderr, "VM FATAL: division by zero!\n"); exit(1); } \
      break; \
    default: \
      if (argv[1] == 0) { fprintf(stderr, "VM FATAL: division by zero!\n"); exit(1); } \
      break; \
  }


#define JXX(op) do { \
  switch (argc) { \
    case 0: \
      STACK_WANT(3); \
      argv[0] = STACK_POP(); \
    case 1: \
      STACK_WANT(2); \
      argv[2] = STACK_POP(); \
      argv[1] = STACK_POP(); \
      break; \
    case 2: \
      STACK_WANT(1); \
      argv[2] = argv[1]; \
      argv[1] = STACK_POP(); \
      break; \
  } \
  if (argv[1] op argv[2]) trd->pc = argv[0]; \
} while (0)


static inline int isBranch (int opcode) {
  return
    opcode == VM_JMP  ||
    opcode == VM_BSR  ||
    opcode == VM_JEQ  ||
    opcode == VM_JNE  ||
    opcode == VM_JLT  ||
    opcode == VM_JLE  ||
    opcode == VM_JGT  ||
    opcode == VM_JGE;
}


// <0: BRK; >0: END
int vmExecuteOne (int tid) {
  VMThread *trd = &vmThreads[tid];
  int opcode;
  int argc;
  int argv[3]; // argument values
  int *argp[3]; // pointer to vars for each arg
  int opc = trd->pc;
  //
  // decode instruction
  opcode = vmGetByte(tid, opc, trd);
  argc = (opcode>>6)&0x03;
  opcode &= 0x3f;
  argv[0] = argv[1] = argv[2] = 0;
  argp[0] = argp[1] = argp[2] = NULL;
  // read operands
  if (tid > vmLastThreadId) vmLastThreadId = tid;
  for (int f = 0; f < argc; ++f) {
    int vn = vmGetByte(tid, opc, trd);
    //
    if (vn == 255) {
      // immediate
      argv[f] = vmGetByte(tid, opc, trd);
      argv[f] |= vmGetByte(tid, opc, trd)<<8;
      if (argv[f] >= 32768 && (f != 0 || !isBranch(opcode))) argv[f] -= 65536;
    } else {
      if (vn == 127) {
        // stack var; <0: from stack top
        argv[f] = vmGetByte(tid, opc, trd);
        argv[f] |= (vmGetByte(tid, opc, trd)<<8);
        if (argv[f] >= 32768) argv[f] -= 65536;
        if (argv[f] < 0) argv[f] += trd->sp;
        if (argv[f] < 0 || argv[f] >= VM_STACK_SIZE) vmfatal(tid, opc, "invalid stack offset");
        argp[f] = trd->stack+argv[f];
      } else {
        // variable
        argp[f] = vn&0x80 ? vmGVars : trd->tvars;
        argp[f] += vn&0x7f;
      }
      argv[f] = *(argp[f]);
    }
  }
  //
//#ifdef VM_DEBUG
  if (vmDebugTrace) {
    if (vmDebugOutput == NULL) vmDebugOutput = stderr;
    //
    fprintf(vmDebugOutput, "(%04d) %04x: ", tid, (unsigned int)opc);
    if (opcode < sizeof(vmOpNames)/sizeof(char *)-1) fprintf(vmDebugOutput, "%s", vmOpNames[opcode]); else fprintf(vmDebugOutput, "BAD");
    for (int f = 0; f < argc; ++f) {
      fputc(' ', vmDebugOutput);
      if (argp[f]) {
        unsigned int ofs = 0;
        //
        fputc('[', vmDebugOutput);
        if (argp[f] >= trd->tvars && argp[f] < trd->tvars+VM_VARS_SIZE) {
          ofs = (unsigned int)(argp[f]-trd->tvars);
        } else if (argp[f] >= trd->stack && argp[f] < trd->stack+VM_VARS_SIZE) {
          ofs = (unsigned int)(argp[f]-trd->stack);
          fputc('.', vmDebugOutput);
        } else if (argp[f] >= vmGVars && argp[f] < vmGVars+VM_VARS_SIZE) {
          ofs = (unsigned int)(argp[f]-vmGVars);
          fputc('@', vmDebugOutput);
        }
        fprintf(vmDebugOutput, "%u]", ofs);
      }
      if (f == 0 && isBranch(opcode)) {
        fprintf(vmDebugOutput, "(0x%04x)", argv[f]);
      } else {
        fprintf(vmDebugOutput, "(%d)", argv[f]);
      }
    }
    fputc('\n', vmDebugOutput);
    if (vmDebugOutput != stderr && vmDebugOutput != stdout) fflush(vmDebugOutput);
  }
//#endif
  //
  switch (opcode) {
    case VM_ADD: MATH(+); break;
    case VM_SUB: MATH(-); break;
    case VM_MUL: MATH(*); break;
    case VM_DIV:
      CHECK_DIV_ZERO
      MATH(/);
      break;
    case VM_MOD:
      CHECK_DIV_ZERO
      MATH(%);
      break;
    case VM_BOR: MATH(|); break;
    case VM_XOR: MATH(^); break;
    case VM_AND: MATH(&); break;

    case VM_JEQ: JXX(==); break;
    case VM_JNE: JXX(!=); break;
    case VM_JLT: JXX(<); break;
    case VM_JLE: JXX(<=); break;
    case VM_JGT: JXX(>); break;
    case VM_JGE: JXX(>=); break;

    case VM_JMP:
      if (argc == 0) {
        STACK_WANT(1);
        argv[0] = STACK_POP();
      }
      trd->pc = argv[0];
      break;

    case VM_BSR:
      switch (argc) {
        case 0:
          STACK_WANT(1);
          argv[0] = STACK_POP();
          break;
        case 1:
          break;
        case 2:
          STACK_FSPC(1);
          STACK_PUSH(argv[1]);
          break;
        case 3:
          STACK_FSPC(2);
          STACK_PUSH(argv[1]);
          STACK_PUSH(argv[2]);
          break;
      }
      STACK_FSPC(1);
      STACK_PUSH(trd->pc);
      trd->pc = argv[0];
      break;

    case VM_END: return 1;
    case VM_BRK: return -1;

    case VM_NEW: { // new thread
      int xtid = vmFindFreeThread();
      //
      if (argc == 0) {
        STACK_WANT(1);
        argv[0] = STACK_POP();
      }
      if (xtid >= 0) {
        if (vmInitThread(&vmThreads[xtid]) == 0) {
          vmThreads[xtid].pc = argv[0];
        } else {
          xtid = -1;
        }
      }
      if (xtid < 0) vmfatal(tid, opc, "too many threads");
      if (xtid > vmLastThreadId) vmLastThreadId = xtid;
      if (argc > 1) {
        if (argp[1]) *(argp[1]) = xtid;
      } else {
        STACK_FSPC(1);
        STACK_PUSH(xtid);
      }
      break; }

    case VM_SET:
      switch (argc) {
        case 0:
          STACK_WANT(2);
          argv[0] = STACK_POP(); // varid
          argv[1] = STACK_POP(); // value
          if (argv[0] < 0) {
            argv[0] = -argv[0];
            if (argv[0] < VM_VARS_SIZE) vmGVars[argv[0]] = argv[1];
          } else if (argv[0] < VM_VARS_SIZE) {
            trd->tvars[argv[0]] = argv[1];
          }
          break;
        case 1:
          STACK_WANT(1);
          argv[1] = STACK_POP(); // value
          // fallthru
        case 2:
          if (argp[0]) *(argp[0]) = argv[1];
          break;
        case 3: {
          int xtid = argv[2], vid = argv[0];
          //
          if (xtid < 0 || xtid >= VM_MAX_THREADS || vmThreads[xtid].stack == NULL || vid < 0 || vid >= VM_VARS_SIZE) break;
          vmThreads[xtid].tvars[vid] = argv[1];
          break; }
      }
      break;

    case VM_GET: {
      int xtid, vid, pushit;
      //
      switch (argc) {
        case 0:
          STACK_WANT(2);
          vid = STACK_POP();
          xtid = STACK_POP();
          pushit = 1;
          break;
        case 1:
          STACK_WANT(1);
          vid = argv[0];
          xtid = STACK_POP();
          pushit = 1;
          break;
        case 2:
          xtid = argv[0];
          vid = argv[1];
          pushit = 1;
          break;
        default:
          xtid = argv[0];
          vid = argv[1];
          pushit = 0;
          break;
      }
      if (xtid >= 0 && xtid < VM_MAX_THREADS && vmThreads[xtid].stack != NULL && vid >= 0 && vid < VM_VARS_SIZE) {
        argv[0] = vmThreads[xtid].tvars[vid];
      } else {
        argv[0] = 0;
      }
      if (!pushit) {
        if (argp[2]) *(argp[2]) = argv[0];
      } else {
        STACK_FSPC(1);
        STACK_PUSH(argv[0]);
      }
      break;

    case VM_PSH:
      if (argc > 0) {
        STACK_FSPC(argc);
        for (int f = 0; f < argc; ++f) STACK_PUSH(argv[f]);
      } else {
        // dup
        STACK_WANT(1);
        STACK_FSPC(1);
        trd->stack[trd->sp] = trd->stack[trd->sp-1];
        ++(trd->sp);
      }
      break;
    case VM_POP:
      switch (argc) {
        case 0: // drop one
          STACK_WANT(1);
          --(trd->sp);
          break;
        case 1:
          if (argp[0] == NULL && argv[0] < 0) {
            // allocate stack space
            argc = -argv[0];
            STACK_FSPC(argc);
            for (; argc > 0; --argc) STACK_PUSH(0);
            break;
          }
          // fallthru
        default:
          for (int f = 0; f < argc; ++f) {
            if (argp[f]) {
              // pop
              STACK_WANT(1);
              *(argp[f]) = STACK_POP();
            } else if (argv[f] > 0) {
              // drop
              STACK_WANT(argv[f]);
              trd->sp -= argv[f];
            }
          }
          break;
        }
      }
      break;
    case VM_SWP:
      switch (argc) {
        case 0:
          STACK_WANT(2);
          argc = trd->stack[trd->sp-2];
          trd->stack[trd->sp-2] = trd->stack[trd->sp-1];
          trd->stack[trd->sp-1] = argc;
          break;
        case 1:
          STACK_WANT(1);
          argc = trd->stack[trd->sp-1];
          trd->stack[trd->sp-1] = argv[0];
          if (argp[0]) *(argp[0]) = argc;
          break;
        default: // 2, 3
          if (argp[0]) *(argp[0]) = argv[1];
          if (argp[1]) *(argp[1]) = argv[0];
          break;
      }
      break;
    case VM_PCK:
    case VM_ROL:
      if (argc < 1) {
        STACK_WANT(1);
        argv[0] = STACK_POP();
      }
      // get item
      if (argv[0] < 0) argv[0] += trd->sp;
      if (argv[0] < 0 || argv[0] >= trd->sp) vmfatal(tid, opc, "invalid stack index (%d)", argv[0]);
      argv[2] = trd->stack[argv[0]];
      //
      if (opcode == VM_ROL) {
        for (int f = argv[0]+1; f < trd->sp; ++f) trd->stack[f-1] = trd->stack[f];
        --(trd->sp);
      }
      //
      switch (argc) {
        case 0:
        case 1:
          STACK_FSPC(1);
          STACK_PUSH(argv[2]);
          break;
        default:
          if (argp[1]) *(argp[1]) = argv[2];
          break;
      }
      break;
    case VM_DPT:
      if (argc > 0) {
        if (argp[0]) *(argp[0]) = trd->sp;
      } else {
        STACK_FSPC(1);
        argc = trd->sp;
        STACK_PUSH(argc);
      }
      break;

    case VM_TID:
      if (argc > 0) {
        if (argp[0]) *(argp[0]) = tid;
      } else {
        STACK_FSPC(1);
        STACK_PUSH(tid);
      }
      break;

    case VM_KIL:
    case VM_SUS:
    case VM_RES:
      if (argc == 0) {
        STACK_WANT(1);
        argv[0] = STACK_POP();
      }
      if (argv[0] >= 0 && argv[0] < VM_MAX_THREADS && vmThreads[argv[0]].stack != NULL) {
        switch (opcode) {
          case VM_KIL:
            if (argv[0] == tid) return 1;
            vmFreeThread(&vmThreads[argv[0]]);
            fixLastThreadId();
            break;
          case VM_SUS:
            if (argv[0] == tid) return -1;
            vmThreads[argv[0]].suspended = 1;
            break;
          case VM_RES:
            vmThreads[argv[0]].suspended = 0;
            break;
        }
      }
      break;

    case VM_STA:
      if (argc == 0) {
        STACK_WANT(1);
        argv[0] = STACK_POP();
      }
      if (argv[0] >= 0 && argv[0] < VM_MAX_THREADS && vmThreads[argv[0]].stack != NULL) {
        argv[0] = vmThreads[argv[0]].suspended;
      } else {
        argv[0] = -1;
      }
      if (argc > 1) {
        if (argp[1]) *(argp[1]) = argv[0];
      } else {
        STACK_FSPC(1);
        STACK_PUSH(argv[0]);
      }
      break;

    case VM_RXC:
      if (argc == 0) {
        STACK_WANT(1);
        argv[0] = STACK_POP();
      }
      if (argv[0] < 0 || argv[0] >= vmCodeSize) vmfatal(tid, opc, "invalid address in RXC (%d)", argv[0]);
      if (argc < 2) {
        STACK_FSPC(1);
        STACK_PUSH(vmCode[argv[0]]);
      } else if (argp[1]) {
        *(argp[1]) = vmCode[argv[0]];
      }
      break;

    case VM_WXC:
      switch (argc) {
        case 0:
          STACK_WANT(2);
          argv[1] = STACK_POP(); // byte
          argv[0] = STACK_POP(); // address
          break;
        case 1:
          STACK_WANT(1);
          argv[1] = STACK_POP(); // byte
          break;
      }
      if (argv[0] < 0 || argv[0] >= vmCodeSize) vmfatal(tid, opc, "invalid address in WXC (%d)", argv[0]);
      if (argv[1] < -128 || argv[1] > 255) vmfatal(tid, opc, "invalid value in WXC (%d)", argv[1]);
      if (argv[1] < 0) argv[1] &= 0xff;
      vmCode[argv[0]] = argv[1];
      break;

    case VM_RET:
      switch (argc) {
        case 0: // simple
          STACK_WANT(1);
          trd->pc = STACK_POP();
          break;
        case 1:
          vmfatal(tid, opc, "invalid RET");
        case 2: // # of locals to pop, # of arguments to pop
        case 3: // # of locals to pop, # of arguments to pop, retvalue (taken before all pops, pushes after returning)
          if (argv[0] < 0) argv[0] = 0;
          if (argv[1] < 0) argv[1] = 0;
          STACK_WANT(argv[0]+argv[1]+1);
          trd->sp -= argv[0]; // drop locals
          trd->pc = STACK_POP();
          trd->sp -= argv[1]; // drop arguments
          if (argc == 3) {
            // push result
            STACK_FSPC(1);
            STACK_PUSH(argv[2]);
          }
          break;
      }
      break;

    case VM_RST:
      if (vmRSTCB) {
        if (argc == 0) {
          STACK_WANT(1);
          argv[0] = STACK_POP();
          argc = 1;
        }
        vmRSTCB(tid, opcode, argc, argv, argp);
      } else if (argc == 0) {
        STACK_WANT(1);
        --(trd->sp);
      }
      break;

    case VM_MGF:
    case VM_MGB:
      switch (argc) {
        case 0: // nothing
        case 1: // only dest
          STACK_WANT(2);
          argv[1] = STACK_POP(); // y
          argv[0] = STACK_POP(); // x
          break;
      }
      if (vmMapGetCB) argv[0] = vmMapGetCB(tid, opcode==VM_MGF, argv[0], argv[1]); else argv[0] = 0;
      if (argc == 3) {
        if (argp[2]) *(argp[2]) = argv[0];
      } else if (argc == 1) {
        if (argp[0]) *(argp[0]) = argv[0];
      } else {
        STACK_FSPC(1);
        STACK_PUSH(argv[0]);
      }
      break;
    case VM_MSF:
    case VM_MSB:
      switch (argc) {
        case 0:
          STACK_WANT(2);
          argv[2] = STACK_POP(); // tile
          argv[1] = STACK_POP(); // y
          argv[0] = STACK_POP(); // x
          break;
        case 1:
          STACK_WANT(2);
          argv[2] = argv[0]; // tile
          argv[1] = STACK_POP(); // y
          argv[0] = STACK_POP(); // x
          break;
        case 2:
          STACK_WANT(1);
          argv[2] = STACK_POP(); // tile
          break;
      }
      if (vmMapSetCB) vmMapSetCB(tid, opcode==VM_MSF, argv[0], argv[1], argv[2]);
      break;

    default:
      vmfatal(tid, opc, "invalid opcode (%d)", opcode);
  }
  return 0;
}


void vmExecuteAll (int menuCheckVar, int inMenu) {
  int runned;
  //
  memset(vmExecuted, 0, sizeof(vmExecuted));
  do {
    runned = 0;
    for (int t = 0; t < VM_MAX_THREADS; ++t) {
      if (!vmExecuted[t] && vmThreads[t].stack != NULL && !vmThreads[t].suspended) {
        int f;
        //
        // skip non-menu threads if we are in menu
        if (inMenu && menuCheckVar >= 0 && menuCheckVar < VM_VARS_SIZE && vmThreads[t].tvars[menuCheckVar] == 0) continue;
        vmExecuted[t] = 1;
        runned = 1;
        for (f = MAX_INST_COUNT; f >= 0; --f) {
          int res = vmExecuteOne(t);
          //
          if (res < 0) {
            // BRK
            //fprintf(stderr, "!!!\n");
            break;
          }
          if (res > 0) {
            // END
            vmFreeThread(&vmThreads[t]);
            fixLastThreadId();
            break;
          }
        }
        if (f < 0) vmfatal(t, vmThreads[t].pc, "too many instructions in frame");
      }
    }
  } while (runned);
}


// <0: BRK; >0: END; 0: ok
// maxinst<0: any number of instructions
int vmExecuteBSR (int tid, int pc, int maxinst) {
  int opc;
  //
  if (tid < 0 || tid >= VM_MAX_THREADS || vmThreads[tid].stack == NULL || vmThreads[tid].suspended) return 2; // END
  opc = vmThreads[tid].pc;
  vmPush(tid, -666666);
  vmThreads[tid].pc = pc;
  if (maxinst == 0) maxinst = MAX_INST_COUNT;
  for (;;) {
    int res = vmExecuteOne(tid);
    //
    if (!vmThreads[tid].stack) return 1;
    if (vmThreads[tid].suspended) return -2;
    if (res > 0) vmFreeThread(&vmThreads[tid]);
    if (res != 0) return res;
    if (vmThreads[tid].pc == -666666) {
      vmThreads[tid].pc = opc;
      return 0;
    }
    if (maxinst == 1) vmfatal(tid, vmThreads[tid].pc, "too many instructions in vmExecuteBSR");
    if (maxinst > 0) --maxinst;
  }
}


AWISH_PURE int vmIsThreadAlive (int tid) {
  if (tid >= 0 && tid < VM_MAX_THREADS) return vmThreads[tid].stack != NULL;
  return 0;
}


int vmNewThread (int pc) {
  int tid = vmFindFreeThread();
  //
  if (tid >= 0) {
    if (vmInitThread(&vmThreads[tid]) != 0) return -1;
    vmThreads[tid].pc = pc;
    if (tid > vmLastThreadId) vmLastThreadId = tid;
  }
  return tid;
}


int vmKillThread (int tid) {
  if (vmIsThreadAlive(tid)) {
    vmFreeThread(&vmThreads[tid]);
    fixLastThreadId();
    return 1;
  }
  return 0;
}


AWISH_PURE int vmIsSuspendedThread (int tid) {
  if (vmIsThreadAlive(tid)) return vmThreads[tid].suspended;
  return 0;
}


int vmSuspendThread (int tid) {
  if (vmIsThreadAlive(tid)) { vmThreads[tid].suspended = 1; return 0; }
  return -1;
}


int vmResumeThread (int tid) {
  if (vmIsThreadAlive(tid)) { vmThreads[tid].suspended = 0; return 0; }
  return -1;
}


AWISH_PURE int vmGetTVar (int tid, int idx) {
  if (idx >= 0 && idx < VM_VARS_SIZE && vmIsThreadAlive(tid)) return vmThreads[tid].tvars[idx];
  return 0;
}


int vmSetTVar (int tid, int idx, int value) {
  if (idx >= 0 && idx < VM_VARS_SIZE && vmIsThreadAlive(tid)) { vmThreads[tid].tvars[idx] = value; return 0; }
  return -1;
}


AWISH_PURE int vmGetSP (int tid) {
  if (vmIsThreadAlive(tid)) return vmThreads[tid].sp;
  return -1;
}


AWISH_PURE int vmGetPC (int tid) {
  if (vmIsThreadAlive(tid)) return vmThreads[tid].pc;
  return -1;
}


int vmSetPC (int tid, int pc) {
  if (vmIsThreadAlive(tid) && pc >= 0 && pc < vmCodeSize) { vmThreads[tid].pc = pc; return 0; }
  return -1;
}


int vmSetSP (int tid, int value) {
  if (vmIsThreadAlive(tid)) { vmThreads[tid].sp = value; return 0; }
  return -1;
}


AWISH_PURE int vmGetStack (int tid, int idx) {
  if (vmIsThreadAlive(tid)) {
    if (idx < 0) idx += vmThreads[tid].sp;
    if (idx >= 0 && idx < VM_STACK_SIZE) return vmThreads[tid].stack[idx];
  }
  return -1;
}


int vmSetStack (int tid, int idx, int value) {
  if (vmIsThreadAlive(tid)) {
    if (idx < 0) idx += vmThreads[tid].sp;
    if (idx >= 0 && idx < VM_STACK_SIZE) { vmThreads[tid].stack[idx] = value; return 0; }
  }
  return -1;
}


int vmPush (int tid, int value) {
  if (vmIsThreadAlive(tid) && vmThreads[tid].sp < VM_STACK_SIZE) {
    vmThreads[tid].stack[vmThreads[tid].sp++] = value;
    return 0;
  }
  return -1;
}


int vmPop (int tid) {
  if (vmIsThreadAlive(tid) && vmThreads[tid].sp > 0) return vmThreads[tid].stack[--vmThreads[tid].sp];
  return 0;
}


int vmLoadArgs (int tid, int argc, int argv[], int *argp[], int aargc, int aargv[], int *aargp[]) {
  if (!vmIsThreadAlive(tid)) return -1;
  if (argc > 0) {
    int e = argc;
    //
    if (e > aargc) e = aargc;
    for (int f = 0; f < e; ++f) { argv[f] = aargv[f]; argp[f] = aargp[f]; }
    for (int f = e; f < argc; ++f) {
      if (vmThreads[tid].sp <= 0) return -1;
      argp[argc-f] = NULL;
      argv[argc-f] = vmThreads[tid].stack[--vmThreads[tid].sp];
    }
  }
  return 0;
}


AWISH_PURE int vmLastThread (void) {
  return vmLastThreadId;
}


static int readBuf (FILE *fl, void *buf, int len) {
  unsigned char *c = (unsigned char *)buf;
  //
  while (len-- > 0) {
    unsigned char b;
    //
    if (fread(&b, 1, 1, fl) != 1) return -1;
    b ^= SECRET;
    *c++ = b;
  }
  return 0;
}


static int writeBuf (FILE *fl, const void *buf, int len) {
  const unsigned char *c = (const unsigned char *)buf;
  //
  while (len-- > 0) {
    unsigned char b = *c++;
    //
    b ^= SECRET;
    if (fwrite(&b, 1, 1, fl) != 1) return -1;
  }
  return 0;
}


static int readDW (FILE *fl, int *v) {
  int t[4];
  //
  for (int f = 0; f < 4; ++f) {
    unsigned char b;
    //
    if (fread(&b, 1, 1, fl) != 1) return -1;
    t[f] = b^SECRET;
  }
  //
  if (v) {
    *v = 0;
    for (int f = 0; f < 4; ++f) *v |= t[f]<<(8*f);
  }
  return 0;
}


static int writeDW (FILE *fl, int v) {
  for (int f = 0; f < 4; ++f) {
    unsigned char b;
    //
    b = v&0xff;
    b ^= SECRET;
    if (fwrite(&b, 1, 1, fl) != 1) return -1;
    v >>= 8;
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static VMLabelInfo *labels = NULL;


////////////////////////////////////////////////////////////////////////////////
void vmFreeLabels (void) {
  while (labels != NULL) {
    VMLabelInfo *l = labels;
    //
    labels = l->next;
    if (l->name) free(l->name);
    free(l);
  }
}


void vmFreeLabelsUntilMark (const char *name) {
  while (labels != NULL) {
    VMLabelInfo *l = labels;
    int isMark = (l->type == LB_MARK);
    //
    if (!(isMark && ((name == NULL && l->name == NULL) || (l->name != NULL && name != NULL && strcmp(name, l->name) == 0)))) isMark = 0;
    labels = l->next;
    if (l->name) free(l->name);
    free(l);
    if (isMark) break;
  }
}


VMLabelInfo *vmLabelAddMark (const char *name) {
  VMLabelInfo *l = malloc(sizeof(VMLabelInfo));
  //
  if (l == NULL) fatalis("out of memory");
  l->name = NULL;
  if (name != NULL) {
    l->name = strdup(name);
    if (l->name == NULL) fatalis("out of memory");
  }
  l->type = LB_MARK;
  l->value = -1;
  l->next = labels;
  l->pub = 0;
  labels = l;
  return l;
}


VMLabelInfo *vmAddLabel (const char *name, int type, int value, int pub) {
  if (name != NULL && name[0]) {
    VMLabelInfo *l = malloc(sizeof(VMLabelInfo));
    //
    if (l == NULL) fatalis("out of memory");
    if (name != NULL) {
      l->name = strdup(name);
      if (l->name == NULL) fatalis("out of memory");
    } else {
      l->name = NULL;
    }
    l->type = type;
    l->value = value;
    l->pub = pub;
    l->next = labels;
    labels = l;
    return l;
  }
  return NULL;
}


static VMLabelInfo *vmAddLabelToEnd (const char *name, int type, int value, int pub) {
  if (name != NULL && name[0]) {
    VMLabelInfo *l = malloc(sizeof(VMLabelInfo)), *last = NULL;
    //
    if (l == NULL) fatalis("out of memory");
    if (name != NULL) {
      l->name = strdup(name);
      if (l->name == NULL) fatalis("out of memory");
    } else {
      l->name = NULL;
    }
    l->type = type;
    l->value = value;
    l->pub = pub;
    l->next = NULL;
    if (labels == NULL) {
      labels = l;
    } else {
      for (last = labels; last->next != NULL; last = last->next) ;
      last->next = l;
    }
    return l;
  }
  return NULL;
}


AWISH_PURE VMLabelInfo *vmFindLabel (const char *name) {
  if (name != NULL && name[0]) {
    for (VMLabelInfo *l = labels; l != NULL; l = l->next) {
      if (l->name && l->name[0] && strcasecmp(l->name, name) == 0) return l;
    }
  }
  return NULL;
}


AWISH_PURE int vmFindPC (const char *name) {
  VMLabelInfo *l = vmFindLabel(name);
  //
  if (l == NULL && l->type != LB_CODE) fatalis("no code label: '%s'", name);
  return l->value;
}


AWISH_PURE int vmFindVarIndex (const char *name) {
  VMLabelInfo *l = vmFindLabel(name);
  //
  if (l == NULL || (l->type != LB_GVAR && l->type != LB_TVAR && l->type != LB_SVAR)) fatalis("no var label: '%s'", name);
  return l->value;
}


AWISH_PURE int vmFindGVarIndex (const char *name) {
  VMLabelInfo *l = vmFindLabel(name);
  //
  if (l == NULL || l->type != LB_GVAR) fatalis("no global var label: '%s'", name);
  return l->value;
}


AWISH_PURE int vmFindTVarIndex (const char *name) {
  VMLabelInfo *l = vmFindLabel(name);
  //
  if (l == NULL || l->type != LB_TVAR) fatalis("no thread var label: '%s'", name);
  return l->value;
}


AWISH_PURE int vmFindSVarIndex (const char *name) {
  VMLabelInfo *l = vmFindLabel(name);
  //
  if (l == NULL || l->type != LB_SVAR) fatalis("no thread var label: '%s'", name);
  return l->value;
}


AWISH_PURE int vmFindConst (const char *name) {
  VMLabelInfo *l = vmFindLabel(name);
  //
  if (l == NULL || l->type != LB_CONST) fatalis("no const label: '%s'", name);
  return l->value;
}


AWISH_PURE VMLabelInfo *vmFindMark (const char *name) {
  for (VMLabelInfo *l = labels; l != NULL; l = l->next) {
    if (l->type != LB_MARK) continue;
    if (name == NULL && l->name == NULL) return l;
    if (name != NULL && l->name != NULL && strcmp(name, l->name) == 0) return l;
  }
  return NULL;
}


#define XREAD(dest,size,errlabel)  do { \
  if (pos+(size) > rsz) goto errlabel; \
  if ((size) > 0) memcpy((dest), buf+pos, (size)); \
  pos += (size); \
} while (0)


#define XREADB(dest,errlabel)  do { \
  if (pos+1 > rsz) goto errlabel; \
  (dest) = buf[pos]; \
  ++pos; \
} while (0)


#define XREADW(dest,errlabel)  do { \
  if (pos+2 > rsz) goto errlabel; \
  (dest) = buf[pos+1]; \
  (dest) <<= 8; \
  (dest) |= buf[pos+0]; \
  pos += 2; \
} while (0)



extern int vmLoadCodeFileFromDump (const void *data, int datasize, int pc, int gvfirst, int tvfirst, int *gvmax, int *tvmax) {
  int rsz = datasize, pos = 4;
  const uint8_t *dta = (const uint8_t *)data;
  uint8_t *buf = NULL;
  int csize, lcnt, rcnt, elcnt, tlast, glast;
  //
  if (rsz < 4) return -1;
  if (memcmp(data, "AVM2", 4) != 0) goto quitbufonly;
  buf = malloc(rsz);
  if (buf == NULL) return 1;
  //
  for (int f = 4; f < rsz; ++f) buf[f] = dta[f]^SECRET;
  //
  XREADW(csize, quitbufonly);
  XREADW(rcnt, quitbufonly); // fixups
  XREADW(elcnt, quitbufonly); // extern labels
  XREADW(lcnt, quitbufonly); // labels
  XREADW(glast, quitbufonly); // last used global
  XREADW(tlast, quitbufonly); // last used thread local
  if (pc < 0 || pc+csize > 65535) goto quitbufonly;
  //
  /*if (goobers)*/ //fprintf(stderr, "code: %d bytes, %d public labels, %d relocations, %d externs\n", csize, lcnt, rcnt, elcnt);
  //
  XREAD(vmCode+pc, csize, quitbufonly);
  //
  // do relocations
  for (int f = 0; f < rcnt; ++f) {
    int rel, newpc, val;
    //
    XREADW(rel, quitbufonly);
    if (rel < 0 || rel+1 >= csize) goto quitbufonly;
    newpc = pc+rel; // fix here
    // get old value
    val = vmCode[newpc+1];
    val <<= 8;
    val |= vmCode[newpc+0];
    // fix it
    val += pc;
    // set new value
    vmCode[newpc+0] = val&0xff;
    vmCode[newpc+1] = (val>>8)&0xff;
  }
  //
  // load extern labels and perform fixups
  for (int f = 0; f < elcnt; ++f) {
    char name[258];
    int type, namelen;
    int rcnt;
    //
    XREADB(type, quitbufonly);
    XREADB(namelen, quitbufonly);
    XREAD(name, namelen, quitbufonly);
    XREADW(rcnt, quitbufonly);
    //
    if (type < LB_MIN_TYPE || type > LB_MAX_TYPE) goto quitbufonly;
    if (namelen < 1) goto quitbufonly;
    name[namelen] = 0;
    //
    for (int c = 0; c < rcnt; ++c) {
      int xpc, size, val;
      VMLabelInfo *l;
      //
      XREADB(size, quitbufonly);
      XREADW(xpc, quitbufonly);
      //
      if (size != 1 && size != 2) goto quitbufonly;
      if (xpc < 0 || xpc+size > csize) goto quitbufonly;
      //
      xpc += pc;
      l = vmFindLabel(name);
      if (l == NULL) {
        /*if (goobers)*/ fprintf(stderr, "VM: unknown extern: '%s'\n", name);
        goto quitbufonly;
      }
      val = l->value;
      if (l->type == LB_GVAR) val |= 0x80;
      //fprintf(stderr, "%d: [%s]: ofs=%d, size=%d, value=%d (%d)\n", c, l->name, xpc, size, val, vmCode[xpc]);
      if (size == 1 && (val < -128 || val > 255)) {
        /*if (goobers)*/ fprintf(stderr, "VM: extern too big: '%s'\n", name);
        goto quitbufonly;
      }
      vmCode[xpc+0] = val&0xff;
      if (size == 2) vmCode[xpc+1] = (val>>8)&0xff;
    }
  }
  //
  vmLabelAddMark(NULL); // for this code labels
  for (int f = 0; f < lcnt; ++f) {
    unsigned char type, namelen, b;
    char name[258];
    int value;
    //
    XREADB(type, quit);
    //
    switch (type&0x7f) {
      case LB_GVAR:
      case LB_TVAR:
        XREADB(b, quit);
        value = b;
        break;
      case LB_SVAR:
      case LB_CONST:
        XREADW(value, quit);
        if (value >= 32768) value -= 65536;
        break;
      case LB_CODE:
        XREADW(value, quit);
        value += pc; // fixup
        break;
      default:
        fatalis("invalid label type: %d\n", type);
        goto quit;
    }
    //
    XREADB(namelen, quit);
    XREAD(name, namelen, quit);
    name[namelen] = 0;
    //
    if (namelen > 0) vmAddLabel(name, type&0x7f, value, type&0x80?1:0);
    //if (l->type == LB_CODE) printf("%d/%d: [%s] %d : %d\n", f, lcnt, l->name, l->value-pc, l->value);
  }
  //
  {
    int cnt;
    //
    XREADW(cnt, quit);
    while (cnt-- > 0) {
      int fpc;
      //
      XREADW(fpc, quit);
      if (gvfirst > 0) {
        //fprintf(stderr, "GVFIX at 0x%04x: %d --> %d\n", fpc, vmCode[pc+fpc], vmCode[pc+fpc]+gvfirst);
        vmCode[pc+fpc] += gvfirst;
      }
    }
    //
    XREADW(cnt, quit);
    while (cnt-- > 0) {
      int fpc;
      //
      XREADW(fpc, quit);
      if (tvfirst > 0) {
        //fprintf(stderr, "TVFIX at 0x%04x: %d --> %d\n", fpc, vmCode[pc+fpc], vmCode[pc+fpc]+tvfirst);
        vmCode[pc+fpc] += tvfirst;
      }
    }
    //
    if (gvmax) *gvmax = glast+gvfirst+1;
    if (tvmax) *tvmax = tlast+tvfirst+1;
  }
  free(buf);
  return csize;
quit:
  vmFreeLabelsUntilMark(NULL);
quitbufonly:
  if (buf != NULL) free(buf);
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
int vmSaveState (FILE *fl) {
  int lcnt = 0;
  //
  fixLastThreadId();
  //
  if (writeDW(fl, VM_VARS_SIZE) != 0) return -1;
  //if (writeDW(fl, VM_STACK_SIZE) != 0) return -1;
  if (writeDW(fl, vmMaxGVar) != 0) return -1;
  if (writeDW(fl, vmMaxTVar) != 0) return -1;
  if (writeDW(fl, vmLastThreadId) != 0) return -1;
  if (writeDW(fl, vmCodeSize) != 0) return -1;
  if (writeBuf(fl, vmCode, vmCodeSize) != 0) return -1;
  for (int f = 0; f < VM_VARS_SIZE; ++f) if (writeDW(fl, vmGVars[f]) != 0) return -1;
  for (int f = 0; f <= vmLastThreadId; ++f) {
    if (vmThreads[f].stack != NULL) {
      if (writeDW(fl, f) != 0) return -1;
      if (writeDW(fl, vmThreads[f].pc) != 0) return -1;
      if (writeDW(fl, vmThreads[f].suspended) != 0) return -1;
      if (writeDW(fl, vmThreads[f].sp) != 0) return -1;
      for (int c = 0; c < vmThreads[f].sp; ++c) if (writeDW(fl, vmThreads[f].stack[c]) != 0) return -1;
      for (int c = 0; c < VM_VARS_SIZE; ++c) if (writeDW(fl, vmThreads[f].tvars[c]) != 0) return -1;
    } else {
      if (writeDW(fl, -1) != 0) return -1;
    }
  }
  //
  for (VMLabelInfo *l = labels; l != NULL; l = l->next) ++lcnt;
  if (writeDW(fl, lcnt) != 0) return -1;
  for (VMLabelInfo *l = labels; l != NULL; l = l->next) {
    int nlen = l->name != NULL ? strlen(l->name) : -1;
    //
    if (writeDW(fl, l->type) != 0) return -1;
    if (writeDW(fl, nlen) != 0) return -1;
    if (nlen > 0) {
      if (writeBuf(fl, l->name, nlen) != 0) return -1;
    }
    if (writeDW(fl, l->value) != 0) return -1;
    if (writeDW(fl, l->pub) != 0) return -1;
  }
  //
  return 0;
}


int vmLoadState (FILE *fl) {
  int v, lcnt;
  //
  vmDeinitialize();
  if (vmInitialize() != 0) goto fail;
  //
  if (readDW(fl, &v) != 0 || v != VM_VARS_SIZE) goto fail;
  //if (readDW(fl, &ssz) != 0) goto fail;
  if (readDW(fl, &vmMaxGVar) != 0 || vmMaxGVar < 0 || vmMaxGVar > VM_VARS_SIZE) goto fail;
  if (readDW(fl, &vmMaxTVar) != 0 || vmMaxTVar < 0 || vmMaxTVar > VM_VARS_SIZE) goto fail;
  if (readDW(fl, &vmLastThreadId) != 0 || vmLastThreadId < 0 || vmLastThreadId > VM_MAX_THREADS) goto fail;
  if (readDW(fl, &vmCodeSize) != 0 || vmCodeSize < 1 || vmCodeSize > 65536) goto fail;
  if (readBuf(fl, vmCode, vmCodeSize) != 0) goto fail;
  for (int f = 0; f < VM_VARS_SIZE; ++f) if (readDW(fl, &vmGVars[f]) != 0) goto fail;
  for (int f = 0; f <= vmLastThreadId; ++f) {
    int flag;
    //
    if (readDW(fl, &flag) != 0) goto fail;
    if (flag == -1) continue;
    if (flag != f) goto fail;
    if (vmInitThread(&vmThreads[f]) != 0) goto fail;
    if (readDW(fl, &vmThreads[f].pc) != 0) goto fail;
    if (readDW(fl, &vmThreads[f].suspended) != 0) goto fail;
    if (readDW(fl, &vmThreads[f].sp) != 0) goto fail;
    if (!vmThreads[f].suspended) {
      if (vmThreads[f].pc < 0 || vmThreads[f].pc >= vmCodeSize) goto fail;
      if (vmThreads[f].sp < 0 || vmThreads[f].sp > VM_STACK_SIZE) goto fail;
    }
    for (int c = 0; c < vmThreads[f].sp; ++c) if (readDW(fl, &vmThreads[f].stack[c]) != 0) goto fail;
    for (int c = 0; c < VM_VARS_SIZE; ++c) if (readDW(fl, &vmThreads[f].tvars[c]) != 0) goto fail;
  }
  //
  if (readDW(fl, &lcnt) != 0) goto fail;
  for (; lcnt > 0; --lcnt) {
    int type, namelen, value, pub;
    char name[257];
    //
    if (readDW(fl, &type) != 0) goto fail;
    if (readDW(fl, &namelen) != 0) goto fail;
    if (namelen > 255) goto fail;
    if (namelen >= 0) {
      if (readBuf(fl, name, namelen) != 0) goto fail;
      name[namelen] = 0;
    }
    if (readDW(fl, &value) != 0) goto fail;
    if (readDW(fl, &pub) != 0) goto fail;
    vmAddLabelToEnd(namelen>=0 ? name : NULL, type, value, pub);
  }
  return 0;
fail:
  vmDeinitialize();
  return -1;
}

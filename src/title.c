/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "title.h"

#include "resfile.h"
#include "video.h"
#include "mainloop.h"
#include "vm.h"
#include "gameglobals.h"
#include "game.h"


static void scrDrawCrashMask (SDL_Surface *frame, int phase) {
  if (phase <= 0) {
    SDL_Rect dst;
    //
    dst.x = 0;
    dst.y = 0;
    dst.w = frame->w;
    dst.h = frame->h;
    SDL_FillRect(frame, &dst, palette[0]);
  } else if (phase < 5) {
    for (int y = 0; y < 20; ++y) {
      for (int x = 0; x < 16; ++x) {
        blitSurface(frame, x*20, y*10, banks[CONST_BANK_FG_TILES].spr[16-phase][0]);
      }
    }
  }
}


static void frmTitleDraw (SDL_Surface *frame) {
  blitSurface(frame, 0, 0, backs[0]);
  scrDrawCrashMask(frame, vmGVars[GVAR_TITLE_PHASE]);
  //
  levelDrawSpriteLayers(frame, 0, 0, 320, 200);
  clearSpriteLayers();
  //
  pmDraw(frame);
  pmClear();
  //
  textDraw(frame);
  textClear();
  //
  if (vmGVars[GVAR_MOUSE_HIDDEN] == 0) {
    int cur = vmGVars[GVAR_MOUSE_CURSOR];
    //
    if (cur >= 0 && cur < banks[256].count) {
      blitSurface(frame, vmGVars[GVAR_MOUSE_X], vmGVars[GVAR_MOUSE_Y], banks[256].spr[cur][0]);
    }
  }
}


static void frmTitleKey (SDL_KeyboardEvent *key) {
  if (key->type == SDL_KEYDOWN) {
    switch (key->keysym.sym) {
      case SDLK_ESCAPE: vmGVars[GVAR_KEY_ESCAPE] = 1; break;
      case SDLK_F12: vmGVars[GVAR_KEY_QUIT] = 1; break;
      case SDLK_SPACE: case SDLK_RETURN: vmGVars[GVAR_KEY_START] = 1; break;
      default: ;
    }
    //
    switch (key->keysym.sym) {
      case SDLK_LEFT: case SDLK_KP4: vmGVars[GVAR_KEY_LEFT] = (key->type == SDL_KEYDOWN); break;
      case SDLK_RIGHT: case SDLK_KP6: vmGVars[GVAR_KEY_RIGHT] = (key->type == SDL_KEYDOWN); break;
      case SDLK_UP: case SDLK_KP8: vmGVars[GVAR_KEY_UP] = (key->type == SDL_KEYDOWN); break;
      case SDLK_DOWN: case SDLK_KP2: vmGVars[GVAR_KEY_DOWN] = (key->type == SDL_KEYDOWN); break;
      default: ;
    }
  }
}


void setMainLoopTitle (void) {
  frameCB = frmTitleDraw;
  keyCB = frmTitleKey;
  mouseCB = NULL;
  gameRSTCB = NULL;
  beforeVMCB = NULL;
  vmMapGetCB = NULL;
  vmMapSetCB = NULL;
  vmPaused = 0;
  clearSpriteLayers();
}

/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* command line parser */
#include "awishcommon.h"
#include "cmdline.h"

#include <windows.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define MAX_ARGS  (256)

static char *kMyImage = NULL;

int k8argc = 0;
char **k8argv = NULL;


void cmdLineParseStr (const char *cmdline) {
  char ech, *cp;
  int maxLen = strlen(cmdline)+64;
  char *pbuf = malloc(maxLen+4);
  //
  /* skip spaces */
  if (k8argv) free(k8argv);
  k8argv = calloc(MAX_ARGS, sizeof(char *));
  k8argc = 0;
  while (*cmdline == ' ') ++cmdline;
  /* parse args */
  while (*cmdline) {
    /* check first char: quoted? */
    if (*cmdline == '"' || *cmdline == '\'') {
      /* quoted arg */
      ech = *cmdline++;
    } else {
      /* normal arg */
      ech = ' ';
    }
    /* collect chars */
    cp = pbuf;
    while (*cmdline && *cmdline != ech) {
      *cp++ = *cmdline++;
      if (ech != ' ' && cmdline[0] == ech && cmdline[1] == ech) {
        /* two quotes is just a quote */
        *cp++ = *cmdline++;
        ++cmdline;
      }
    }
    *cp = '\0';
    //fprintf(stderr, "[%s]\n", pbuf);
    if (k8argc < MAX_ARGS) k8argv[k8argc++] = strdup(pbuf);
    /* skip quote, if any */
    if (ech != ' ' && cmdline[0] == ech) ++cmdline;
    /* skip spaces */
    while (*cmdline == ' ') ++cmdline;
  }
  free(pbuf);
  //
  if (kMyImage == NULL) {
    kMyImage = calloc(MAX_PATH+1, 1);
    if (kMyImage != NULL) {
      GetModuleFileName(NULL, kMyImage, MAX_PATH);
      if (k8argv[0]) free(k8argv[0]);
      k8argv[0] = strdup(kMyImage);
    }
  }
}


void cmdLineParse (void) {
  return cmdLineParseStr(GetCommandLine());
}

/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _VM_H_
#define _VM_H_

#include "awishcommon.h"


#define VM_STACK_SIZE  (1024)
#define VM_VARS_SIZE   (127)

#define VM_MAX_THREADS  (512)


enum {
  VM_ADD,
  VM_SUB,
  VM_MUL,
  VM_DIV,
  VM_MOD,
  VM_BOR,
  VM_XOR,
  VM_AND,

  VM_JEQ,
  VM_JNE,
  VM_JLT,
  VM_JLE,
  VM_JGT,
  VM_JGE,

  VM_JMP,

  VM_BSR,

  VM_BRK,
  VM_END,
  VM_NEW,

  VM_SET,
  VM_GET,

  VM_PSH,
  VM_POP,
  VM_SWP,
  VM_PCK,
  VM_ROL,
  VM_DPT,

  VM_TID,
  VM_KIL,
  VM_SUS,
  VM_RES,
  VM_STA,

  VM_RXC,
  VM_WXC,

  VM_RET,

  VM_RST,

  VM_MGF,
  VM_MGB,
  VM_MSF,
  VM_MSB,

  VM_LASTOP
};


enum {
  LB_MIN_TYPE = 0,
  LB_GVAR = 0,
  LB_TVAR,
  LB_SVAR,
  LB_CODE,
  LB_CONST,
  LB_MAX_TYPE,
  LB_MARK = -1
};


typedef struct VMLabelInfo {
  struct VMLabelInfo *next;
  char *name;
  int type;
  int value;
  int pub; // public?
} VMLabelInfo;


extern int vmDebugTrace;
extern FILE *vmDebugOutput;

extern const char *vmOpNames[];

extern unsigned char vmCode[65536];
extern int vmCodeSize;
extern int vmGVars[VM_VARS_SIZE];

extern int vmMaxGVar;
extern int vmMaxTVar;


// <0: BRK; >0: END; 0: continue
typedef int (*VMRSTCB) (int tid, int opcode, int argc, int argv[], int *argp[]);

extern VMRSTCB vmRSTCB; // argv[0] is ALWAYS fid


typedef int (*VMMapGetCB) (int tid, int fg, int x, int y);
typedef void (*VMMapSetCB) (int tid, int fg, int x, int y, int tile);

extern VMMapGetCB vmMapGetCB;
extern VMMapSetCB vmMapSetCB;


// initialize VM, init main thread
extern int vmInitialize (void); // glovals, code and codesize must be set
extern void vmDeinitialize (void);

extern void vmExecuteAll (int menuCheckVar, int inMenu);

// <0: BRK; >0: END; 0: ok
// maxinst<0: any number of instructions
extern int vmExecuteBSR (int tid, int pc, int maxinst);

// <0: BRK; >0: END
extern int vmExecuteOne (int tid);

extern int vmIsThreadAlive (int tid) AWISH_PURE;

extern int vmNewThread (int pc); // returns thread id or -1
extern int vmKillThread (int tid);
extern int vmIsSuspendedThread (int tid) AWISH_PURE;
extern int vmSuspendThread (int tid);
extern int vmResumeThread (int tid);

extern int vmGetTVar (int tid, int idx) AWISH_PURE;
extern int vmSetTVar (int tid, int idx, int value);
extern int vmGetPC (int tid) AWISH_PURE;
extern int vmSetPC (int tid, int pc);

extern int vmGetSP (int tid) AWISH_PURE;
extern int vmSetSP (int tid, int value);
extern int vmGetStack (int tid, int idx) AWISH_PURE; // <0: from stack top; >=0: from bottom (0: latest pushed item)
extern int vmSetStack (int tid, int idx, int value);

extern int vmPush (int tid, int value);
extern int vmPop (int tid);

extern int vmLoadArgs (int tid, int argc, int argv[], int *argp[], int aargc, int aargv[], int *aargp[]); // max 3 aargs


extern int vmLastThread (void) AWISH_PURE;


// !0: error
extern int vmSaveState (FILE *fl);

// !0: error; VM is deinitialized on error
extern int vmLoadState (FILE *fl);


// return code size
extern int vmLoadCodeFileFromDump (const void *data, int datasize, int pc, int gvfirst, int tvfirst, int *gvmax, int *tvmax);


extern void vmFreeLabels (void);
extern void vmFreeLabelsUntilMark (const char *name);

extern VMLabelInfo *vmLabelAddMark (const char *name);
extern VMLabelInfo *vmAddLabel (const char *name, int type, int value, int pub);

extern VMLabelInfo *vmFindLabel (const char *name) AWISH_PURE;

extern int vmFindPC (const char *name) AWISH_PURE;
extern int vmFindVarIndex (const char *name) AWISH_PURE;
extern int vmFindGVarIndex (const char *name) AWISH_PURE;
extern int vmFindTVarIndex (const char *name) AWISH_PURE;
extern int vmFindSVarIndex (const char *name) AWISH_PURE;
extern int vmFindConst (const char *name) AWISH_PURE;
extern VMLabelInfo *vmFindMark (const char *name) AWISH_PURE;


#endif
